<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'main_page', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
        '/product_for_developers' => [[['_route' => 'product_for_developers_page', '_controller' => 'App\\Controller\\ProductDevController::product_for_developers'], null, null, null, false, false, null]],
        '/product_for_managers' => [[['_route' => 'product_for_managers_page', '_controller' => 'App\\Controller\\ProductManController::product_for_managers'], null, null, null, false, false, null]],
        '/product_for_translators' => [[['_route' => 'product_for_translators_page', '_controller' => 'App\\Controller\\ProductTranController::product_for_translators'], null, null, null, false, false, null]],
        '/features' => [[['_route' => 'features_page', '_controller' => 'App\\Controller\\FeaturesController::features'], null, null, null, false, false, null]],
        '/secutiry' => [[['_route' => 'security_page', '_controller' => 'App\\Controller\\SecurityController::security'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'login_page', '_controller' => 'App\\Controller\\LoginController::login'], null, null, null, false, false, null]],
        '/signup' => [[['_route' => 'signup_page', '_controller' => 'App\\Controller\\SignupController::signup'], null, null, null, false, false, null]],
        '/team' => [[['_route' => 'team_page', '_controller' => 'App\\Controller\\TeamController::team'], null, null, null, false, false, null]],
        '/collaborative_translation' => [[['_route' => 'collaborative_translation', '_controller' => 'App\\Controller\\CollaborativeTranslationController::collaborative_translation'], null, null, null, false, false, null]],
        '/localization_workflow_management' => [[['_route' => 'localization_workflow_management', '_controller' => 'App\\Controller\\LocalizationWorkflowManagementContoller::localization_workflow_management'], null, null, null, false, false, null]],
        '/process_automation' => [[['_route' => 'process_automation', '_controller' => 'App\\Controller\\ProcessAutomationController::process_automation'], null, null, null, false, false, null]],
        '/quality_assurance' => [[['_route' => 'quality_assurance', '_controller' => 'App\\Controller\\QualityAssuranceController::quality_assurance'], null, null, null, false, false, null]],
        '/in_context_editing' => [[['_route' => 'in_context_editing', '_controller' => 'App\\Controller\\InContextEditingController::in_context_editing'], null, null, null, false, false, null]],
        '/deliver_to_end_users' => [[['_route' => 'deliver_to_end_users', '_controller' => 'App\\Controller\\DeliverToEndUsersController::deliver_to_end_users'], null, null, null, false, false, null]],
        '/support_ticket_and_chat_translation' => [[['_route' => 'support_ticket_and_chat_translation', '_controller' => 'App\\Controller\\SupportTicketAndChatTranslationController::support_ticket_and_chat_translation'], null, null, null, false, false, null]],
        '/professional_translation_services' => [[['_route' => 'professional_translation_services', '_controller' => 'App\\Controller\\ProfessionalTranslationServices::professional_translation_services'], null, null, null, false, false, null]],
        '/case_studies' => [[['_route' => 'case_studies', '_controller' => 'App\\Controller\\CaseStudiesController::case_studies'], null, null, null, false, false, null]],
        '/case_study' => [[['_route' => 'case_study', '_controller' => 'App\\Controller\\CaseStudyController::case_study'], null, null, null, false, false, null]],
        '/pricing' => [[['_route' => 'pricing', '_controller' => 'App\\Controller\\PricingController::pricing'], null, null, null, false, false, null]],
        '/integrations' => [[['_route' => 'integrations', '_controller' => 'App\\Controller\\IntegrationsController::integrations'], null, null, null, false, false, null]],
        '/legal' => [[['_route' => 'legal', '_controller' => 'App\\Controller\\LegalController::legal'], null, null, null, false, false, null]],
        '/careers' => [[['_route' => 'careers_page', '_controller' => 'App\\Controller\\CareersController::careers'], null, null, null, false, false, null]],
        '/integrations/asana_integration' => [[['_route' => 'asana_integration_page', '_controller' => 'App\\Controller\\AsanaIntegrationController::asana_integration'], null, null, null, false, false, null]],
        '/integrations/amazon_integration' => [[['_route' => 'amazon_integration_page', '_controller' => 'App\\Controller\\AmazonIntegrationController::amazon_integration'], null, null, null, false, false, null]],
        '/integrations/bitbucket_integration' => [[['_route' => 'bitbucket_integration_page', '_controller' => 'App\\Controller\\BitbucketIntegrationController::bitbucket_integration'], null, null, null, false, false, null]],
        '/integrations/google_integration' => [[['_route' => 'google_integration_page', '_controller' => 'App\\Controller\\GoogleIntegrationController::google_integration'], null, null, null, false, false, null]],
        '/integrations/github_integration' => [[['_route' => 'github_integration_page', '_controller' => 'App\\Controller\\GithubIntegrationController::github_integration'], null, null, null, false, false, null]],
        '/integrations/gitlab_integration' => [[['_route' => 'gitlab_integration_page', '_controller' => 'App\\Controller\\GitlabIntegrationController::gitlab_integration'], null, null, null, false, false, null]],
        '/integrations/slack_integration' => [[['_route' => 'slack_integration_page', '_controller' => 'App\\Controller\\SlackIntegrationController::slack_integration'], null, null, null, false, false, null]],
        '/integrations/trello_integration' => [[['_route' => 'trello_integration_page', '_controller' => 'App\\Controller\\TrelloIntegrationController::trello_integration'], null, null, null, false, false, null]],
        '/integrations/intercom_integration' => [[['_route' => 'intercom_integration_page', '_controller' => 'App\\Controller\\IntercomIntegrationController::intercom_integration'], null, null, null, false, false, null]],
        '/integrations/zendesk_integration' => [[['_route' => 'zendesk_integration_page', '_controller' => 'App\\Controller\\ZendeskIntegrationController::zendesk_integration'], null, null, null, false, false, null]],
        '/integrations/zendesk_support_integration' => [[['_route' => 'zendesk_support_integration_page', '_controller' => 'App\\Controller\\ZendeskSupportIntegrationController::zendesk_support_integration'], null, null, null, false, false, null]],
        '/integrations/sketch_integration' => [[['_route' => 'sketch_integration_page', '_controller' => 'App\\Controller\\SketchIntegrationController::sketch_integration'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
