<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* integrations.html.twig */
class __TwigTemplate_c43064b0245e2e3398fbe8c3d4e422311684cc33a1d8d665b4cefc5ba668e7bc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "integrations.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "integrations.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "integrations.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <source srcset=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_07.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
            <source srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_07@2x.png"), "html", null, true);
        echo " 2x\">
            <img srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_07.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <h1>";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_promo_title"), "html", null, true);
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_promo_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"two-columns\">
        <aside class=\"sidebar\">
            <div class=\"link-holder\">   
                <a href=\"#\" class=\"more\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_all_link_text"), "html", null, true);
        echo "</a>
            </div>
            <a href=\"#\" class=\"open-categories-menu\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_open_categories_menu"), "html", null, true);
        echo "</a>
            <div class=\"categories-menu\">
                <a href=\"#\" class=\"mb-burger\"><span>menu</span></a>
                <h3>";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_title"), "html", null, true);
        echo "</h3>
                <nav class=\"menu\">
                    <ul class=\"categories-switcher\"> <!-- className needed for switching integration-list -->
                        <li><a href=\"#\" data-category=\"type-1\">";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_1"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-2\">";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_2"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-3\">";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_3"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-4\">";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_4"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-5\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_5"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-6\">";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_6"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-7\">";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_7"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"#\" data-category=\"type-8\">";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_sidebar_menu_item_8"), "html", null, true);
        echo "</a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class=\"column\">
            <ul class=\"integration-list\">
                <li class=\"type-1\">
                    <a href=\"";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("amazon_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_amazon.svg"), "html", null, true);
        echo "\" alt=\"Amazon S3\"></span>
                        <strong>Amazon S3</strong>
                        <span>";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_android.svg"), "html", null, true);
        echo "\" alt=\"Android\"></span>
                        <strong>Android</strong>
                        <span>";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_api.svg"), "html", null, true);
        echo "\" alt=\"API\"></span>
                        <strong>API</strong>
                        <span>";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("asana_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_asana.svg"), "html", null, true);
        echo "\" alt=\"Asana\"></span>
                        <strong>Asana</strong>
                        <span>";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbucket_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_bitbucket.svg"), "html", null, true);
        echo "\" alt=\"Bitbucket\"></span>
                        <strong>Bitbucket</strong>
                        <span>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_cake_php.svg"), "html", null, true);
        echo "\" alt=\"Cake PHP\"></span>
                        <strong>Cake PHP</strong>
                        <span>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_contentful.svg"), "html", null, true);
        echo "\" alt=\"Contentful\"></span>
                        <strong>Contentful</strong>
                        <span>";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_datalog.svg"), "html", null, true);
        echo "\" alt=\"Datadog\"></span>
                        <strong>Datadog</strong>
                        <span>";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_django.svg"), "html", null, true);
        echo "\" alt=\"Django\"></span>
                        <strong>Django</strong>
                        <span>";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_docker.svg"), "html", null, true);
        echo "\" alt=\"Docker\"></span>
                        <strong>Docker</strong>
                        <span>";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_email.svg"), "html", null, true);
        echo "\" alt=\"E-mail\"></span>
                        <strong>E-mail</strong>
                        <span>";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_fastlane.svg"), "html", null, true);
        echo "\" alt=\"Fastlane\"></span>
                        <strong>Fastlane</strong>
                        <span>";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_flowdock.svg"), "html", null, true);
        echo "\" alt=\"Flowdock\"></span>
                        <strong>Flowdock</strong>
                        <span>";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"";
        // line 134
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("google_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_google_storage.svg"), "html", null, true);
        echo "\" alt=\"Google Storage\"></span>
                        <strong>Google Storage</strong>
                        <span>";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-6\">
                    <a href=\"";
        // line 141
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("gitlab_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_gitlab.svg"), "html", null, true);
        echo "\" alt=\"GitLab\"></span>
                        <strong>GitLab</strong>
                        <span>";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_grunt.svg"), "html", null, true);
        echo "\" alt=\"Grunt\"></span>
                        <strong>Grunt</strong>
                        <span>";
        // line 151
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-7\">
                    <a href=\"";
        // line 155
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("intercom_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_intercom.svg"), "html", null, true);
        echo "\" alt=\"Intercom\"></span>
                        <strong>Intercom</strong>
                        <span>";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-8\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_ios.svg"), "html", null, true);
        echo "\" alt=\"iOS\"></span>
                        <strong>iOS</strong>
                        <span>";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1 type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_javascript.svg"), "html", null, true);
        echo "\" alt=\"JavaScript\"></span>
                        <strong>JavaScript</strong>
                        <span>";
        // line 172
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-3 type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_jenkins.svg"), "html", null, true);
        echo "\" alt=\"Jenkins\"></span>
                        <strong>Jenkins</strong>
                        <span>";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-7\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_jira.svg"), "html", null, true);
        echo "\" alt=\"Jira\"></span>
                        <strong>Jira</strong>
                        <span>";
        // line 186
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-8 type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 191
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_lighthouse.svg"), "html", null, true);
        echo "\" alt=\"Lighthouse\"></span>
                        <strong>Lighthouse</strong>
                        <span>";
        // line 193
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_node_js.svg"), "html", null, true);
        echo "\" alt=\"Node.js\"></span>
                        <strong>Node.js</strong>
                        <span>";
        // line 200
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_ruby_on_rails.svg"), "html", null, true);
        echo "\" alt=\"Ruby on Rails\"></span>
                        <strong>Ruby on Rails</strong>
                        <span>";
        // line 207
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-5\">
                    <a href=\"";
        // line 211
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("slack_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\"></span>
                        <strong>Slack</strong>
                        <span>";
        // line 214
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-6\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_timacamp.svg"), "html", null, true);
        echo "\" alt=\"TimeCamp\"></span>
                        <strong>TimeCamp</strong>
                        <span>";
        // line 221
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>

                <li class=\"type-3\">
                    <a href=\"";
        // line 226
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trello_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_trello.svg"), "html", null, true);
        echo "\" alt=\"Trello\"></span>
                        <strong>Trello</strong>
                        <span>";
        // line 229
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"";
        // line 233
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("zendesk_integration_page");
        echo "\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_zendesk.svg"), "html", null, true);
        echo "\" alt=\"Zendesk\"></span>
                        <strong>Zendesk</strong>
                        <span>";
        // line 236
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 244
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 245
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "integrations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  585 => 245,  581 => 244,  577 => 243,  567 => 236,  562 => 234,  558 => 233,  551 => 229,  546 => 227,  542 => 226,  534 => 221,  529 => 219,  521 => 214,  516 => 212,  512 => 211,  505 => 207,  500 => 205,  492 => 200,  487 => 198,  479 => 193,  474 => 191,  466 => 186,  461 => 184,  453 => 179,  448 => 177,  440 => 172,  435 => 170,  427 => 165,  422 => 163,  414 => 158,  409 => 156,  405 => 155,  398 => 151,  393 => 149,  385 => 144,  380 => 142,  376 => 141,  369 => 137,  364 => 135,  360 => 134,  353 => 130,  348 => 128,  340 => 123,  335 => 121,  327 => 116,  322 => 114,  314 => 109,  309 => 107,  301 => 102,  296 => 100,  288 => 95,  283 => 93,  275 => 88,  270 => 86,  262 => 81,  257 => 79,  249 => 74,  244 => 72,  240 => 71,  233 => 67,  228 => 65,  224 => 64,  217 => 60,  212 => 58,  204 => 53,  199 => 51,  191 => 46,  186 => 44,  182 => 43,  171 => 35,  167 => 34,  163 => 33,  159 => 32,  155 => 31,  151 => 30,  147 => 29,  143 => 28,  137 => 25,  131 => 22,  126 => 20,  117 => 14,  112 => 12,  105 => 10,  101 => 9,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'integrations_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <source srcset=\"{{asset('build/promo_07.png')}}\" media=\"(max-width: 768px)\">
            <source srcset=\"{{asset('build/promo_07@2x.png')}} 2x\">
            <img srcset=\"{{asset('build/promo_07.png')}}\" alt=\"{{ 'integrations_promo_title'|trans }}\">
        </picture>
        <h1>{{ 'integrations_promo_title'|trans }}</h1>
        <div class=\"text\">
            <p>{{ 'integrations_promo_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"two-columns\">
        <aside class=\"sidebar\">
            <div class=\"link-holder\">   
                <a href=\"#\" class=\"more\">{{ 'integrations_all_link_text'|trans }}</a>
            </div>
            <a href=\"#\" class=\"open-categories-menu\">{{ 'integrations_open_categories_menu'|trans }}</a>
            <div class=\"categories-menu\">
                <a href=\"#\" class=\"mb-burger\"><span>menu</span></a>
                <h3>{{ 'integrations_sidebar_title'|trans }}</h3>
                <nav class=\"menu\">
                    <ul class=\"categories-switcher\"> <!-- className needed for switching integration-list -->
                        <li><a href=\"#\" data-category=\"type-1\">{{ 'integrations_sidebar_menu_item_1'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-2\">{{ 'integrations_sidebar_menu_item_2'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-3\">{{ 'integrations_sidebar_menu_item_3'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-4\">{{ 'integrations_sidebar_menu_item_4'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-5\">{{ 'integrations_sidebar_menu_item_5'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-6\">{{ 'integrations_sidebar_menu_item_6'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-7\">{{ 'integrations_sidebar_menu_item_7'|trans }}</a></li>
                        <li><a href=\"#\" data-category=\"type-8\">{{ 'integrations_sidebar_menu_item_8'|trans }}</a></li>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class=\"column\">
            <ul class=\"integration-list\">
                <li class=\"type-1\">
                    <a href=\"{{ path('amazon_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_amazon.svg') }}\" alt=\"Amazon S3\"></span>
                        <strong>Amazon S3</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_android.svg') }}\" alt=\"Android\"></span>
                        <strong>Android</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_api.svg') }}\" alt=\"API\"></span>
                        <strong>API</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li>
                    <a href=\"{{ path('asana_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_asana.svg') }}\" alt=\"Asana\"></span>
                        <strong>Asana</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"{{ path('bitbucket_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_bitbucket.svg') }}\" alt=\"Bitbucket\"></span>
                        <strong>Bitbucket</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_cake_php.svg') }}\" alt=\"Cake PHP\"></span>
                        <strong>Cake PHP</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_contentful.svg') }}\" alt=\"Contentful\"></span>
                        <strong>Contentful</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_datalog.svg') }}\" alt=\"Datadog\"></span>
                        <strong>Datadog</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li>
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_django.svg') }}\" alt=\"Django\"></span>
                        <strong>Django</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_docker.svg') }}\" alt=\"Docker\"></span>
                        <strong>Docker</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_email.svg') }}\" alt=\"E-mail\"></span>
                        <strong>E-mail</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_fastlane.svg') }}\" alt=\"Fastlane\"></span>
                        <strong>Fastlane</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_flowdock.svg') }}\" alt=\"Flowdock\"></span>
                        <strong>Flowdock</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"{{ path('google_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_google_storage.svg') }}\" alt=\"Google Storage\"></span>
                        <strong>Google Storage</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-6\">
                    <a href=\"{{ path('gitlab_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_gitlab.svg') }}\" alt=\"GitLab\"></span>
                        <strong>GitLab</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-3\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_grunt.svg') }}\" alt=\"Grunt\"></span>
                        <strong>Grunt</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-7\">
                    <a href=\"{{ path('intercom_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_intercom.svg') }}\" alt=\"Intercom\"></span>
                        <strong>Intercom</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-8\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_ios.svg') }}\" alt=\"iOS\"></span>
                        <strong>iOS</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1 type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_javascript.svg') }}\" alt=\"JavaScript\"></span>
                        <strong>JavaScript</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-3 type-4\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_jenkins.svg') }}\" alt=\"Jenkins\"></span>
                        <strong>Jenkins</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-7\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_jira.svg') }}\" alt=\"Jira\"></span>
                        <strong>Jira</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-8 type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_lighthouse.svg') }}\" alt=\"Lighthouse\"></span>
                        <strong>Lighthouse</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-2\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_node_js.svg') }}\" alt=\"Node.js\"></span>
                        <strong>Node.js</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_ruby_on_rails.svg') }}\" alt=\"Ruby on Rails\"></span>
                        <strong>Ruby on Rails</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-5\">
                    <a href=\"{{ path('slack_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_slack.svg') }}\" alt=\"Slack\"></span>
                        <strong>Slack</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-6\">
                    <a href=\"#\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_timacamp.svg') }}\" alt=\"TimeCamp\"></span>
                        <strong>TimeCamp</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>

                <li class=\"type-3\">
                    <a href=\"{{ path('trello_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_trello.svg') }}\" alt=\"Trello\"></span>
                        <strong>Trello</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
                <li class=\"type-1\">
                    <a href=\"{{ path('zendesk_integration_page') }}\">
                        <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_zendesk.svg') }}\" alt=\"Zendesk\"></span>
                        <strong>Zendesk</strong>
                        <span>{{ 'integrations_list_item_text'|trans }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "integrations.html.twig", "/Users/user/dev/lokalize/lokalise/templates/integrations.html.twig");
    }
}
