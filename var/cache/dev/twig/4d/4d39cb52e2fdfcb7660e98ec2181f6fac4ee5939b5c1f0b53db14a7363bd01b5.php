<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* slack_integration.html.twig */
class __TwigTemplate_f1d9b201fce84ed1daf572dfc0b4302d8f0e363bbd473c600ae0b1bd10335ee7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "slack_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "slack_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "slack_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_connect.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_select.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_choose.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_tools_list_text_3");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_text_block_title");
        echo "</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_3_title");
        echo "</strong>
                <p>";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_fuq_item_1_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_fuq_item_2_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_integration_fuq_item_2_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"managment bg-emptiness\">
        <h2 class=\"align-center\">";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("slack_intergation_managment_title");
        echo "</h2>
        <ul class=\"app-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_asana_02.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">Asana</strong>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_trello.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">Trello</strong>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_jira.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">Jira</strong>
            </li>
        </ul> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "slack_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  275 => 87,  271 => 86,  267 => 85,  258 => 79,  251 => 75,  244 => 71,  238 => 68,  230 => 63,  226 => 62,  220 => 59,  216 => 58,  210 => 55,  202 => 50,  198 => 49,  192 => 46,  188 => 45,  182 => 42,  178 => 41,  172 => 38,  164 => 33,  160 => 32,  156 => 31,  150 => 28,  146 => 27,  142 => 26,  136 => 23,  132 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'slack_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'slack_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'slack_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'slack_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_connect.svg')}}\"></div>
                <strong class=\"title\">{{ 'slack_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'slack_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_select.svg')}}\"></div>
                <strong class=\"title\">{{ 'slack_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'slack_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_choose.svg')}}\"></div>
                <strong class=\"title\">{{ 'slack_intergation_tools_list_title_3'|trans }}</strong>
                <p>{{ 'slack_intergation_tools_list_text_3'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>{{ 'slack_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">{{ 'slack_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'slack_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'slack_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'slack_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'slack_intergation_privacy_list_item_3_title'|trans|raw }}</strong>
                <p>{{ 'slack_intergation_privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'slack_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'slack_integration_fuq_item_1_title'|trans }}</div>
                <div class=\"text\">{{ 'slack_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'slack_integration_fuq_item_2_title'|trans }}</div>
                <div class=\"text\">{{ 'slack_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"managment bg-emptiness\">
        <h2 class=\"align-center\">{{ 'slack_intergation_managment_title'|trans|raw }}</h2>
        <ul class=\"app-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_asana_02.svg')}}\"></div>
                <strong class=\"title\">Asana</strong>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_trello.svg')}}\"></div>
                <strong class=\"title\">Trello</strong>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_jira.svg')}}\"></div>
                <strong class=\"title\">Jira</strong>
            </li>
        </ul> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "slack_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/slack_integration.html.twig");
    }
}
