<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pricing.html.twig */
class __TwigTemplate_1d006c66b73d2db35282dfa63ee34661c2cf9be6b7537f57c1f7afc85bb3eb94 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pricing.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pricing.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "pricing.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_08.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_08@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_08.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <h1>";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_promo_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"price-table\">
        <div class=\"table-nav\">
            <ul>
                <li class=\"active\"><span>";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_1"), "html", null, true);
        echo "</span></li>
                <li><span>";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_2"), "html", null, true);
        echo "</span></li>
                <li><span>";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_3"), "html", null, true);
        echo "</span></li>
                <li><span>";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_4"), "html", null, true);
        echo "</span></li>
            </ul>
        </div> 
        <div class=\"columns\">
            <div class=\"col\">
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_1"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_2"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\"></div>
                 <div class=\"row\"></div>
                <div class=\"row align-top\">
                    <strong class=\"row-title\">";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_3"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\"></div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_1"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <span class=\"people\">1+</span>
                    <strong class=\"title\">";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_1"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_text_1");
        echo "</p>
                </div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_2"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <strong class=\"price\">\$9</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_price_text_1");
        echo "</p>
                </div>
                <div class=\"row align-top\">
                    <strong class=\"row-title\">";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_3"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <ul class=\"info-list\">
                        <li>";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_1_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_2_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_3_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_4_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_5_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_6_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_1_item_7_text"), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_btn_start_text"), "html", null, true);
        echo "</a>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">5+</span>
                    <strong class=\"title\">";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_2"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_text_2"), "html", null, true);
        echo "</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">\$19</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_price_text_2");
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">";
        // line 100
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_title");
        echo "</strong>
                    <ul class=\"info-list\">
                        <li>";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_1_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_2_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_3_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_4_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_5_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_2_item_6_text"), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_btn_start_text"), "html", null, true);
        echo "</a>
                </div>
            </div>
             <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">20+</span>
                    <strong class=\"title\">";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_3"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">";
        // line 121
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_text_3");
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">\$29</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_price_text_3");
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_title"), "html", null, true);
        echo "</strong>
                    <ul class=\"info-list\">
                        <li>";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_1_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_2_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_3_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_4_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_5_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_3_item_6_text"), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_btn_start_text"), "html", null, true);
        echo "</a>
                </div>
            </div>
            <div class=\"col plan-info\">
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">50+</span>
                    <strong class=\"title\">";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_title_4"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">";
        // line 153
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_plan_text_4");
        echo "</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">&nbsp;</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">";
        // line 160
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_price_text_4");
        echo "</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">";
        // line 164
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_title"), "html", null, true);
        echo "</strong>
                    <ul class=\"info-list\">
                        <li>";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_item_1_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_item_2_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_item_3_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_item_4_text"), "html", null, true);
        echo "</li>
                        <li>";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_list_4_item_5_text"), "html", null, true);
        echo "</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">";
        // line 174
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_item_4_btn_start_text"), "html", null, true);
        echo "</a>
                </div>
            </div>
        </div>
        <div class=\"btn-more-holder\">
            <a href=\"#\">";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_btn_more_text"), "html", null, true);
        echo "</a>
        </div>
        <div class=\"columns no-borders\">
            <div class=\"col\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 185
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_4"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 189
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_5"), "html", null, true);
        echo "</strong>
                </div>
            </div>
            <div class=\"col  plan-info\">
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 194
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_4"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <p class=\"counter\">";
        // line 197
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_item_1_counter"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"row\">
                    <strong class=\"row-title\">";
        // line 200
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_row_title_5"), "html", null, true);
        echo "</strong>
                </div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">";
        // line 209
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_item_2_counter"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">";
        // line 219
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_item_3_counter"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">";
        // line 229
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("pricing_table_item_4_counter"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
        </div>
    </section>
    <section class=\"privacy-block\">
        <ul class=\"privacy-list\">   
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 241
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_certificate.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_title"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 242
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 243
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_privaci.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_title"), "html", null, true);
        echo "\"></div>
                 <strong class=\"title\">";
        // line 247
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 248
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_gpr.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_title"), "html", null, true);
        echo "\"></div>
                 <strong class=\"title\">";
        // line 252
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 253
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"security-info\">
        <ul class=\"security-info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 260
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_monitored.png"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_1_text");
        echo "\"></div>
                <p>";
        // line 261
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_1_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_uptime.svg"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_2_text");
        echo "\"></div>
                <p>";
        // line 265
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_2_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_sound.svg"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_3_text");
        echo "\"></div>
                <p>";
        // line 269
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 274
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 277
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_1_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 278
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 281
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_2_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 282
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_2_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 285
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_3_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 286
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_3_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 289
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_4_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 290
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fuq_item_4_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 295
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 296
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 297
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "pricing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  679 => 297,  675 => 296,  671 => 295,  663 => 290,  659 => 289,  653 => 286,  649 => 285,  643 => 282,  639 => 281,  633 => 278,  629 => 277,  623 => 274,  615 => 269,  609 => 268,  603 => 265,  597 => 264,  591 => 261,  585 => 260,  575 => 253,  571 => 252,  565 => 251,  559 => 248,  555 => 247,  549 => 246,  543 => 243,  539 => 242,  533 => 241,  518 => 229,  505 => 219,  492 => 209,  480 => 200,  474 => 197,  468 => 194,  460 => 189,  453 => 185,  444 => 179,  436 => 174,  429 => 170,  425 => 169,  421 => 168,  417 => 167,  413 => 166,  408 => 164,  401 => 160,  391 => 153,  385 => 150,  375 => 143,  368 => 139,  364 => 138,  360 => 137,  356 => 136,  352 => 135,  348 => 134,  343 => 132,  336 => 128,  326 => 121,  320 => 118,  310 => 111,  303 => 107,  299 => 106,  295 => 105,  291 => 104,  287 => 103,  283 => 102,  278 => 100,  271 => 96,  261 => 89,  255 => 86,  245 => 79,  238 => 75,  234 => 74,  230 => 73,  226 => 72,  222 => 71,  218 => 70,  214 => 69,  207 => 65,  201 => 62,  192 => 56,  186 => 53,  180 => 50,  173 => 46,  164 => 40,  156 => 35,  148 => 30,  138 => 23,  134 => 22,  130 => 21,  126 => 20,  117 => 14,  112 => 12,  105 => 10,  101 => 9,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'pricing_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"{{asset('build/promo_08.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_08@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_08.png')}}\" alt=\"{{ 'pricing_promo_title'|trans }}\">
        </picture>
        <h1>{{ 'pricing_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'pricing_promo_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"price-table\">
        <div class=\"table-nav\">
            <ul>
                <li class=\"active\"><span>{{ 'pricing_table_plan_title_1'|trans }}</span></li>
                <li><span>{{ 'pricing_table_plan_title_2'|trans }}</span></li>
                <li><span>{{ 'pricing_table_plan_title_3'|trans }}</span></li>
                <li><span>{{ 'pricing_table_plan_title_4'|trans }}</span></li>
            </ul>
        </div> 
        <div class=\"columns\">
            <div class=\"col\">
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_1'|trans }}</strong>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_2'|trans }}</strong>
                </div>
                <div class=\"row\"></div>
                 <div class=\"row\"></div>
                <div class=\"row align-top\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_3'|trans }}</strong>
                </div>
                <div class=\"row\"></div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_1'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <span class=\"people\">1+</span>
                    <strong class=\"title\">{{ 'pricing_table_plan_title_1'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">{{ 'pricing_table_plan_text_1'|trans|raw }}</p>
                </div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_2'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <strong class=\"price\">\$9</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">{{ 'pricing_table_price_text_1'|trans|raw }}</p>
                </div>
                <div class=\"row align-top\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_3'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <ul class=\"info-list\">
                        <li>{{ 'pricing_table_list_1_item_1_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_2_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_3_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_4_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_5_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_6_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_1_item_7_text'|trans }}</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">{{ 'pricing_table_btn_start_text'|trans }}</a>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">5+</span>
                    <strong class=\"title\">{{ 'pricing_table_plan_title_2'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">{{ 'pricing_table_plan_text_2'|trans }}</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">\$19</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">{{ 'pricing_table_price_text_2'|trans|raw }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">{{ 'pricing_table_list_2_title'|trans|raw }}</strong>
                    <ul class=\"info-list\">
                        <li>{{ 'pricing_table_list_2_item_1_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_2_item_2_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_2_item_3_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_2_item_4_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_2_item_5_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_2_item_6_text'|trans }}</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">{{ 'pricing_table_btn_start_text'|trans }}</a>
                </div>
            </div>
             <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">20+</span>
                    <strong class=\"title\">{{ 'pricing_table_plan_title_3'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">{{ 'pricing_table_plan_text_3'|trans|raw }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">\$29</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">{{ 'pricing_table_price_text_3'|trans|raw }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">{{ 'pricing_table_list_3_title'|trans }}</strong>
                    <ul class=\"info-list\">
                        <li>{{ 'pricing_table_list_3_item_1_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_3_item_2_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_3_item_3_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_3_item_4_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_3_item_5_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_3_item_6_text'|trans }}</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">{{ 'pricing_table_btn_start_text'|trans }}</a>
                </div>
            </div>
            <div class=\"col plan-info\">
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"people\">50+</span>
                    <strong class=\"title\">{{ 'pricing_table_plan_title_4'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <p class=\"plan-info\">{{ 'pricing_table_plan_text_4'|trans|raw }}</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"price\">&nbsp;</strong>
                </div>
                <div class=\"row\">
                    <p class=\"price-info\">{{ 'pricing_table_price_text_4'|trans|raw }}</p>
                </div>
                 <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"info-title\">{{ 'pricing_table_list_4_title'|trans }}</strong>
                    <ul class=\"info-list\">
                        <li>{{ 'pricing_table_list_4_item_1_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_4_item_2_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_4_item_3_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_4_item_4_text'|trans }}</li>
                        <li>{{ 'pricing_table_list_4_item_5_text'|trans }}</li>
                    </ul>
                </div>
                <div class=\"row\">
                    <a href=\"#\" class=\"btn-primary btn-large\">{{ 'pricing_table_item_4_btn_start_text'|trans }}</a>
                </div>
            </div>
        </div>
        <div class=\"btn-more-holder\">
            <a href=\"#\">{{ 'pricing_btn_more_text'|trans }}</a>
        </div>
        <div class=\"columns no-borders\">
            <div class=\"col\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_4'|trans }}</strong>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_5'|trans }}</strong>
                </div>
            </div>
            <div class=\"col  plan-info\">
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_4'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <p class=\"counter\">{{ 'pricing_table_item_1_counter'|trans }}</p>
                </div>
                <div class=\"row\">
                    <strong class=\"row-title\">{{ 'pricing_table_row_title_5'|trans }}</strong>
                </div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">{{ 'pricing_table_item_2_counter'|trans }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">{{ 'pricing_table_item_3_counter'|trans }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
            <div class=\"col plan-info\">
                <div class=\"row\"></div>
                <div class=\"row\">
                    <p class=\"counter\">{{ 'pricing_table_item_4_counter'|trans }}</p>
                </div>
                <div class=\"row\"></div>
                <div class=\"row\">
                    <span class=\"checked\"></span>
                </div>
            </div>
        </div>
    </section>
    <section class=\"privacy-block\">
        <ul class=\"privacy-list\">   
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_certificate.svg')}}\" alt=\"{{ 'privacy_list_item_1_title'|trans }}\"></div>
                <strong class=\"title\">{{ 'privacy_list_item_1_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_privaci.svg')}}\" alt=\"{{ 'privacy_list_item_2_title'|trans }}\"></div>
                 <strong class=\"title\">{{ 'privacy_list_item_2_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_gpr.svg')}}\" alt=\"{{ 'privacy_list_item_3_title'|trans }}\"></div>
                 <strong class=\"title\">{{ 'privacy_list_item_3_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"security-info\">
        <ul class=\"security-info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_monitored.png')}}\" alt=\"{{ 'security_info_list_item_1_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_uptime.svg')}}\" alt=\"{{ 'security_info_list_item_2_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_sound.svg')}}\" alt=\"{{ 'security_info_list_item_3_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'fuq_item_1_title'|trans }}</div>
                <div class=\"text\">{{ 'fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'fuq_item_2_title'|trans }}</div>
                <div class=\"text\">{{ 'fuq_item_2_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'fuq_item_3_title'|trans }}</div>
                <div class=\"text\">{{ 'fuq_item_3_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'fuq_item_4_title'|trans }}</div>
                <div class=\"text\">{{ 'fuq_item_4_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "pricing.html.twig", "/Users/user/dev/lokalize/lokalise/templates/pricing.html.twig");
    }
}
