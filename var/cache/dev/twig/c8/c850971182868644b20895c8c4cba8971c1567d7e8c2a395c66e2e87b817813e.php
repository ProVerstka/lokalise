<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* case_study.html.twig */
class __TwigTemplate_1d61b842ef1a1528fd2a80e0ee056dde0bc4a0f76eb15af3fe4b7611437bbbe0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "case_study.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "case_study.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "case_study.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section class=\"posts\">
        <article class=\"post large-post\">
            <div class=\"text\">
                <header>
                    ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <h2>";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
    </section>
    <section class=\"used-integrations\">
        <h4>";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("used_integrations_title"), "html", null, true);
        echo "</h4>
        <ul class=\"usage-list\">
            <li>
                <span class=\"icon\"><img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_airtable.svg"), "html", null, true);
        echo "\" alt=\"Airtable\"></span>
                Airtable
            </li>
            <li>
                 <span class=\"icon\"><img src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_google_drive.svg"), "html", null, true);
        echo "\" alt=\"Google Drive\"></span>
                Google Drive
            </li>
            <li>
                 <span class=\"icon\"><img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_asana.svg"), "html", null, true);
        echo "\" alt=\"Asana\"></span>
                Asana
            </li>
            <li>
                 <span class=\"icon\"><img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_outlook.svg"), "html", null, true);
        echo "\" alt=\"Microsoft Outlook\"></span>
                Microsoft Outlook
            </li>
        </ul>
    </section>
    <section class=\"try-now small\">
        <h1>";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h1>
        <a href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
        <a href=\"#\" class=\"btn btn-large\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("book_link"), "html", null, true);
        echo "</a>
    </section>
    <section class=\"simple-text-block\">
        <h3>Problem</h3> 
        <p>In a context of strong growth, Lucca faced 3 major localization problems: <br>Day to day management of translations lacked fluidity and generated errors. A simple text update required updating a spreadsheet, creating a Jira ticket and asking a developer to update the text, then checking if the update was successful. The developer had to create a branch with the modification. This process was error-prone, as the spreadsheet was not always up to date, resulting in old translations replacing more recent ones.</p>
        <div class=\"image\">
            <picture>
                <data-src srcset=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_38.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_38@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_38.jpg"), "html", null, true);
        echo "\" alt=\"Problem\">
            </picture>
        </div>
        <p>New modules have to be translated, but the translations came from spreadsheets. External or internal translators were copypasting the translations into the main spreadsheets with risks of errors. In addition, external translators had to be guided all the time.</p>
        <p>Adding a new language was a cumbersome operation, as translations for the whole app were split in 80+ spreadsheets. This resulted in new language requests being postponed until an unknown date.</p>
    </section>
    <section class=\"review bg-emptiness\">
        <blockquote>
            <p>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("blockquote_1_text"), "html", null, true);
        echo "</p>
            <footer>";
        // line 71
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("blockqoute_1_author");
        echo "</footer>
        </blockquote>
    </section>
     <section class=\"simple-text-block\">
        <h3>Improving transparency across the company with channels</h3> 
        <p>The Lucca team felt that they need to establish a continuous and agile localization process. They have begun their search using \"Software Internationalization\" and \"Translation Management System\" keywords. After testing about 16 different TMS, Lucca gave their preference to Lokalise. REST API allowed Lucca to easily integrate Lokalse with their code, while clear and intuitive user interface created a pleasurable experience.</p>
    </section>
    <section class=\"review bg-merino\">
        <blockquote>
            <p>";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("blockquote_2_text"), "html", null, true);
        echo "</p>
        </blockquote>
    </section>
    <section class=\"simple-text-block\">
        <p>Their current workflow consists of three simple steps:</p>
        <p>Developers create new keys and fill the value for the base language. <br>Product Owners check and modify this value.  <br>Translators do their job and translate into the target language. <br>Using a translation management platform allowed Lucca to separate their responsibilities. Now developers don’t have to waste their time on each step of the process. Finally, by using the webhooks, translations are automatically updated on the developer's computer.</p>
    </section>
    <section class=\"features bg-emptiness low-paddings\">
        <h4>";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_choose_title");
        echo "</h4>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_qa.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_1"), "html", null, true);
        echo "</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_api.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_2"), "html", null, true);
        echo "</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_sdk.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_3"), "html", null, true);
        echo "</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_inline_machine.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_4"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_study_features_title_4"), "html", null, true);
        echo "</strong>
            </li>
        </ul>
    </section>
    <section class=\"review bg-quince\">
        <blockquote>
            <p>";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("blockquote_3_text"), "html", null, true);
        echo "</p>
        </blockquote>
    </section>
   
    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "case_study.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 110,  298 => 104,  292 => 103,  286 => 100,  280 => 99,  274 => 96,  268 => 95,  262 => 92,  256 => 91,  250 => 88,  239 => 80,  227 => 71,  223 => 70,  212 => 62,  208 => 61,  204 => 60,  194 => 53,  188 => 52,  184 => 51,  175 => 45,  168 => 41,  161 => 37,  154 => 33,  148 => 30,  137 => 24,  133 => 23,  129 => 22,  121 => 17,  117 => 16,  112 => 14,  108 => 13,  103 => 11,  99 => 10,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'case_studies_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section class=\"posts\">
        <article class=\"post large-post\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <h2>{{ 'case_studies_post_1_title'|trans }}</h2>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_01@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_01.jpg')}}\" alt=\"{{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
    </section>
    <section class=\"used-integrations\">
        <h4>{{ 'used_integrations_title'|trans }}</h4>
        <ul class=\"usage-list\">
            <li>
                <span class=\"icon\"><img src=\"{{ asset('build/icon_airtable.svg') }}\" alt=\"Airtable\"></span>
                Airtable
            </li>
            <li>
                 <span class=\"icon\"><img src=\"{{ asset('build/icon_google_drive.svg') }}\" alt=\"Google Drive\"></span>
                Google Drive
            </li>
            <li>
                 <span class=\"icon\"><img src=\"{{ asset('build/icon_asana.svg') }}\" alt=\"Asana\"></span>
                Asana
            </li>
            <li>
                 <span class=\"icon\"><img src=\"{{ asset('build/icon_outlook.svg') }}\" alt=\"Microsoft Outlook\"></span>
                Microsoft Outlook
            </li>
        </ul>
    </section>
    <section class=\"try-now small\">
        <h1>{{ 'try_title'|trans }}</h1>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
        <a href=\"#\" class=\"btn btn-large\">{{ 'book_link'|trans }}</a>
    </section>
    <section class=\"simple-text-block\">
        <h3>Problem</h3> 
        <p>In a context of strong growth, Lucca faced 3 major localization problems: <br>Day to day management of translations lacked fluidity and generated errors. A simple text update required updating a spreadsheet, creating a Jira ticket and asking a developer to update the text, then checking if the update was successful. The developer had to create a branch with the modification. This process was error-prone, as the spreadsheet was not always up to date, resulting in old translations replacing more recent ones.</p>
        <div class=\"image\">
            <picture>
                <data-src srcset=\"{{asset('build/img_38.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_38@2x.jpg')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_38.jpg')}}\" alt=\"Problem\">
            </picture>
        </div>
        <p>New modules have to be translated, but the translations came from spreadsheets. External or internal translators were copypasting the translations into the main spreadsheets with risks of errors. In addition, external translators had to be guided all the time.</p>
        <p>Adding a new language was a cumbersome operation, as translations for the whole app were split in 80+ spreadsheets. This resulted in new language requests being postponed until an unknown date.</p>
    </section>
    <section class=\"review bg-emptiness\">
        <blockquote>
            <p>{{ 'blockquote_1_text'|trans }}</p>
            <footer>{{ 'blockqoute_1_author'|trans|raw }}</footer>
        </blockquote>
    </section>
     <section class=\"simple-text-block\">
        <h3>Improving transparency across the company with channels</h3> 
        <p>The Lucca team felt that they need to establish a continuous and agile localization process. They have begun their search using \"Software Internationalization\" and \"Translation Management System\" keywords. After testing about 16 different TMS, Lucca gave their preference to Lokalise. REST API allowed Lucca to easily integrate Lokalse with their code, while clear and intuitive user interface created a pleasurable experience.</p>
    </section>
    <section class=\"review bg-merino\">
        <blockquote>
            <p>{{ 'blockquote_2_text'|trans }}</p>
        </blockquote>
    </section>
    <section class=\"simple-text-block\">
        <p>Their current workflow consists of three simple steps:</p>
        <p>Developers create new keys and fill the value for the base language. <br>Product Owners check and modify this value.  <br>Translators do their job and translate into the target language. <br>Using a translation management platform allowed Lucca to separate their responsibilities. Now developers don’t have to waste their time on each step of the process. Finally, by using the webhooks, translations are automatically updated on the developer's computer.</p>
    </section>
    <section class=\"features bg-emptiness low-paddings\">
        <h4>{{ 'case_study_choose_title'|trans|raw }}</h4>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_qa.svg')}}\" alt=\"{{ 'case_study_features_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'case_study_features_title_1'|trans }}</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_api.svg')}}\" alt=\"{{ 'case_study_features_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'case_study_features_title_2'|trans }}</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_sdk.svg')}}\" alt=\"{{ 'case_study_features_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'case_study_features_title_3'|trans }}</strong>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_inline_machine.svg')}}\" alt=\"{{ 'case_study_features_title_4'|trans }}\"></div>
                <strong class=\"title\">{{ 'case_study_features_title_4'|trans }}</strong>
            </li>
        </ul>
    </section>
    <section class=\"review bg-quince\">
        <blockquote>
            <p>{{ 'blockquote_3_text'|trans }}</p>
        </blockquote>
    </section>
   
    
{% endblock %}", "case_study.html.twig", "/Users/user/dev/lokalize/lokalise/templates/case_study.html.twig");
    }
}
