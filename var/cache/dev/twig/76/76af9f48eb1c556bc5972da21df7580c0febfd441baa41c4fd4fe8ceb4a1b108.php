<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product_for_managers.html.twig */
class __TwigTemplate_d34ce1f1eab5557866fee46ccb8c13202045a968ea0aa3325cf678dcda73e7df extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_managers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_managers.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "product_for_managers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_promo_title"), "html", null, true);
        echo "</h1>
        <picture class=\"image\">
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_03.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_03@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_03.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"/signup\" class=\"btn-primary btn-large\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preferred_title"), "html", null, true);
        echo "</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stripe.svg"), "html", null, true);
        echo "\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/yelp.svg"), "html", null, true);
        echo "\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lime.svg"), "html", null, true);
        echo "\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lemonade.svg"), "html", null, true);
        echo "\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h1>";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title");
        echo "</h1>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_dashboard.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translate.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_screenshots.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_text_3"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_tasks.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_4"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_title_4"), "html", null, true);
        echo "</strong>
                <p>";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_features_text_4"), "html", null, true);
        echo "</p>
            </li>
        </ul>
        <a href=\"/features\" class=\"btn-primary btn-large\">";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_btn_text"), "html", null, true);
        echo "</a>
    </section>
    <section class=\"report\">
        <h2>";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_title"), "html", null, true);
        echo "</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_3"), "html", null, true);
        echo "</p>
            </li>
        </ul> 
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section id=\"info\">
        <h2>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_title"), "html", null, true);
        echo "</h2>
        <ul class=\"info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_websites.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_mobile.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_games.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_docs.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_market.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_iot.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 120
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_text"), "html", null, true);
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"estabilish\">
        <h1>";
        // line 126
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_title");
        echo "</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_04.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_04@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_04.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_1_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 137
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_1_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_05.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_05@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_05.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_2_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_2_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_06.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_06@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_06.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>";
        // line 162
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_3_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 163
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_3_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_07.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_07@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_07.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>";
        // line 175
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_4_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 176
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("estabilish_item_4_text");
        echo "</p>
            </div>
        </div>
    </section>
    <section class=\"integrate\">
        <h1>";
        // line 181
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "</h1>
        <p>";
        // line 182
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_text"), "html", null, true);
        echo "</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
            </picture>
        </div>
    </section>
    <section class=\"on-board\">
        <h1>";
        // line 192
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_title");
        echo "</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 202
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 203
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 204
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 212
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 216
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 217
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 218
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 223
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 224
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 225
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product_for_managers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  658 => 225,  654 => 224,  650 => 223,  642 => 218,  638 => 217,  634 => 216,  625 => 212,  621 => 211,  617 => 210,  608 => 204,  604 => 203,  600 => 202,  591 => 198,  587 => 197,  583 => 196,  576 => 192,  566 => 187,  562 => 186,  558 => 185,  552 => 182,  548 => 181,  540 => 176,  536 => 175,  527 => 171,  523 => 170,  519 => 169,  510 => 163,  506 => 162,  497 => 158,  493 => 157,  489 => 156,  480 => 150,  476 => 149,  467 => 145,  463 => 144,  459 => 143,  450 => 137,  446 => 136,  437 => 132,  433 => 131,  429 => 130,  422 => 126,  414 => 121,  410 => 120,  404 => 119,  398 => 116,  394 => 115,  388 => 114,  382 => 111,  378 => 110,  372 => 109,  366 => 106,  362 => 105,  356 => 104,  350 => 101,  346 => 100,  340 => 99,  334 => 96,  330 => 95,  324 => 94,  318 => 91,  310 => 86,  306 => 85,  301 => 83,  293 => 80,  289 => 79,  285 => 78,  276 => 72,  272 => 71,  266 => 68,  262 => 67,  256 => 64,  252 => 63,  246 => 60,  240 => 57,  234 => 54,  230 => 53,  224 => 52,  218 => 49,  214 => 48,  208 => 47,  202 => 44,  198 => 43,  192 => 42,  186 => 39,  182 => 38,  176 => 37,  170 => 34,  163 => 30,  159 => 29,  155 => 28,  151 => 27,  147 => 26,  143 => 25,  138 => 23,  131 => 19,  126 => 17,  122 => 16,  117 => 14,  109 => 11,  105 => 10,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'product_for_managers_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'product_for_managers_promo_title'|trans }}</h1>
        <picture class=\"image\">
            <data-src srcset=\"{{asset('build/promo_03.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_03@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_03.png')}}\" alt=\"{{ 'product_for_managers_promo_title'|trans }}\">
        </picture>
        <div class=\"text\">
            <p>{{ 'product_for_managers_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"/signup\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>{{ 'preferred_title'|trans }}</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"{{asset('build/slack.svg')}}\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/revolut.svg')}}\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/stripe.svg')}}\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/yelp.svg')}}\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lime.svg')}}\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lemonade.svg')}}\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h1>{{ 'product_for_managers_features_title'|trans|raw }}</h1>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_dashboard.svg')}}\" alt=\"{{ 'product_for_managers_features_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_managers_features_title_1'|trans }}</strong>
                <p>{{ 'product_for_managers_features_text_1'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_translate.svg')}}\" alt=\"{{ 'product_for_managers_features_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_managers_features_title_2'|trans }}</strong>
                <p>{{ 'product_for_managers_features_text_2'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_screenshots.svg')}}\" alt=\"{{ 'product_for_managers_features_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_managers_features_title_3'|trans }}</strong>
                <p>{{ 'product_for_managers_features_text_3'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_tasks.svg')}}\" alt=\"{{ 'product_for_managers_features_title_4'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_managers_features_title_4'|trans }}</strong>
                <p>{{ 'product_for_managers_features_text_4'|trans }}</p>
            </li>
        </ul>
        <a href=\"/features\" class=\"btn-primary btn-large\">{{ 'features_btn_text'|trans }}</a>
    </section>
    <section class=\"report\">
        <h2>{{ 'report_title'|trans }}</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">{{ 'report_list_title_1'|trans }}</strong>
                <p>{{ 'report_list_text_1'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_2'|trans }}</strong>
                <p>{{ 'report_list_text_2'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_3'|trans }}</strong>
                <p>{{ 'report_list_text_3'|trans }}</p>
            </li>
        </ul> 
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section id=\"info\">
        <h2>{{ 'info_title'|trans }}</h2>
        <ul class=\"info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_websites.svg')}}\" alt=\"{{ 'info_list_1_title'|trans }}\"></div>
                <strong>{{ 'info_list_1_title'|trans }}</strong>
                <p>{{ 'info_list_1_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_mobile.svg')}}\" alt=\"{{ 'info_list_2_title'|trans }}\"></div>
                <strong>{{ 'info_list_2_title'|trans }}</strong>
                <p>{{ 'info_list_2_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_games.svg')}}\" alt=\"{{ 'info_list_3_title'|trans }}\"></div>
                <strong>{{ 'info_list_3_title'|trans }}</strong>
                <p>{{ 'info_list_3_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_docs.svg')}}\" alt=\"{{ 'info_list_4_title'|trans }}\"></div>
                <strong>{{ 'info_list_4_title'|trans }}</strong>
                <p>{{ 'info_list_4_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_market.svg')}}\" alt=\"{{ 'info_list_5_title'|trans }}\"></div>
                <strong>{{ 'info_list_5_title'|trans }}</strong>
                <p>{{ 'info_list_5_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_iot.svg')}}\" alt=\"{{ 'info_list_6_title'|trans }}\"></div>
                <strong>{{ 'info_list_6_title'|trans }}</strong>
                <p>{{ 'info_list_6_text'|trans }}</p>
            </li>
        </ul>
    </section>
    <section class=\"estabilish\">
        <h1>{{ 'estabilish_title'|trans|raw }}</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_04.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_04@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_04.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>{{ 'estabilish_item_1_title'|trans }}</h4>
                <p>{{ 'estabilish_item_1_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_05.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_05@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_05.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>{{ 'estabilish_item_2_title'|trans }}</h4>
                <p>{{ 'estabilish_item_2_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_06.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_06@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_06.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>{{ 'estabilish_item_3_title'|trans }}</h4>
                <p>{{ 'estabilish_item_3_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_07.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_07@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_07.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h4>{{ 'estabilish_item_4_title'|trans }}</h4>
                <p>{{ 'estabilish_item_4_text'|trans|raw }}</p>
            </div>
        </div>
    </section>
    <section class=\"integrate\">
        <h1>{{ 'integrate_title'|trans }}</h1>
        <p>{{ 'integrate_text'|trans }}</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"{{asset('build/img_01.png')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_01@2x.png')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_01.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
            </picture>
        </div>
    </section>
    <section class=\"on-board\">
        <h1>{{ 'on_board_title'|trans|raw }}</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_02.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_02@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_02.png')}}\" alt=\"{{ 'product_for_managers_on_board_item_1_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'product_for_managers_on_board_item_1_title'|trans }}</h3>
                <p>{{ 'product_for_managers_on_board_item_1_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_03.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_03@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_03.png')}}\" alt=\"{{ 'product_for_managers_on_board_item_2_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'product_for_managers_on_board_item_2_title'|trans }}</h3>
                <p>{{ 'product_for_managers_on_board_item_2_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "product_for_managers.html.twig", "/Users/user/dev/lokalize/lokalise/templates/product_for_managers.html.twig");
    }
}
