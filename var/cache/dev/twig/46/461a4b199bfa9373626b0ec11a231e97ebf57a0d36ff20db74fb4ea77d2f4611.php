<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* google_integration.html.twig */
class __TwigTemplate_634e337e0b59f9b2fd71300c004cdcf523f4cd9310c66dafb9351fc924bde660 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "google_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "google_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "google_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_connect.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_trigger.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_tools_list_text_2");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_text_block_title");
        echo "</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_1_title");
        echo "</div>
                <div class=\"text\">";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_2_title");
        echo "</div>
                <div class=\"text\">";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_2_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_3_title");
        echo "</div>
                <div class=\"text\">";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_fuq_item_3_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("google_integration_connect_title");
        echo "</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"";
        // line 66
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("google_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_amazon.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Amazon S3</strong>
                    <span>";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_android.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Android</strong>
                    <span>";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_api.svg"), "html", null, true);
        echo "\"></span>
                    <strong>API</strong>
                    <span>";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("asana_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_asana.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Asana</strong>
                    <span>";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_bitbucket.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Bitbucket</strong>
                    <span>";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_cake_php.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Cake PHP</strong>
                    <span>";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 112
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "google_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 112,  320 => 111,  316 => 110,  307 => 104,  302 => 102,  294 => 97,  289 => 95,  281 => 90,  276 => 88,  272 => 87,  265 => 83,  260 => 81,  252 => 76,  247 => 74,  239 => 69,  234 => 67,  230 => 66,  224 => 63,  216 => 58,  212 => 57,  206 => 54,  202 => 53,  196 => 50,  192 => 49,  186 => 46,  178 => 41,  174 => 40,  168 => 37,  164 => 36,  158 => 33,  150 => 28,  146 => 27,  142 => 26,  136 => 23,  132 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'google_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'google_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'google_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'google_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_connect.svg')}}\"></div>
                <strong class=\"title\">{{ 'google_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'google_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_trigger.svg')}}\"></div>
                <strong class=\"title\">{{ 'google_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'google_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>{{ 'google_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">{{ 'google_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'google_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'google_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'google_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'google_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'google_integration_fuq_item_1_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'google_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'google_integration_fuq_item_2_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'google_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'google_integration_fuq_item_3_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'google_integration_fuq_item_3_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">{{ 'google_integration_connect_title'|trans|raw }}</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"{{ path('google_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_amazon.svg') }}\"></span>
                    <strong>Amazon S3</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_android.svg') }}\"></span>
                    <strong>Android</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_api.svg') }}\"></span>
                    <strong>API</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('asana_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_asana.svg') }}\"></span>
                    <strong>Asana</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_bitbucket.svg') }}\"></span>
                    <strong>Bitbucket</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_cake_php.svg') }}\"></span>
                    <strong>Cake PHP</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "google_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/google_integration.html.twig");
    }
}
