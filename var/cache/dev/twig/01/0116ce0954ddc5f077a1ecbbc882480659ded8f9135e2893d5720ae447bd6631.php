<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* amazon_integration.html.twig */
class __TwigTemplate_1989e8dabb1da360bc1e8be7d9ec460336a97a5ed67541a068e8799058758751 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "amazon_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "amazon_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "amazon_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_connect.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_trigger.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_tools_list_text_2");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_text_block_title");
        echo "</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_1_title");
        echo "</div>
                <div class=\"text\">";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 53
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_2_title");
        echo "</div>
                <div class=\"text\">";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_2_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_3_title");
        echo "</div>
                <div class=\"text\">";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_fuq_item_3_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("amazon_integration_connect_title");
        echo "</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"";
        // line 66
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("amazon_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_amazon.svg"), "html", null, true);
        echo "\" alt=\"Amazon S3\"></span>
                    <strong>Amazon S3</strong>
                    <span>";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_android.svg"), "html", null, true);
        echo "\" alt=\"Android\"></span>
                    <strong>Android</strong>
                    <span>";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_api.svg"), "html", null, true);
        echo "\" alt=\"API\"></span>
                    <strong>API</strong>
                    <span>";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("asana_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_asana.svg"), "html", null, true);
        echo "\" alt=\"Asana\"></span>
                    <strong>Asana</strong>
                    <span>";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 94
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbucket_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_bitbucket.svg"), "html", null, true);
        echo "\" alt=\"Bitbucket\"></span>
                    <strong>Bitbucket</strong>
                    <span>";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_cake_php.svg"), "html", null, true);
        echo "\" alt=\"Cake PHP\"></span>
                    <strong>Cake PHP</strong>
                    <span>";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 112
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "amazon_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  331 => 112,  327 => 111,  323 => 110,  314 => 104,  309 => 102,  301 => 97,  296 => 95,  292 => 94,  285 => 90,  280 => 88,  276 => 87,  269 => 83,  264 => 81,  256 => 76,  251 => 74,  243 => 69,  238 => 67,  234 => 66,  228 => 63,  220 => 58,  216 => 57,  210 => 54,  206 => 53,  200 => 50,  196 => 49,  190 => 46,  182 => 41,  178 => 40,  172 => 37,  168 => 36,  162 => 33,  154 => 28,  150 => 27,  144 => 26,  138 => 23,  134 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'amazon_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'amazon_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'amazon_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'amazon_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_connect.svg')}}\" alt=\"{{ 'amazon_intergation_tools_list_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'amazon_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'amazon_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_trigger.svg')}}\" alt=\"{{ 'amazon_intergation_tools_list_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'amazon_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'amazon_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>{{ 'amazon_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">{{ 'amazon_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'amazon_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'amazon_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'amazon_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'amazon_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'amazon_integration_fuq_item_1_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'amazon_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'amazon_integration_fuq_item_2_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'amazon_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'amazon_integration_fuq_item_3_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'amazon_integration_fuq_item_3_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">{{ 'amazon_integration_connect_title'|trans|raw }}</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"{{ path('amazon_integration_page') }}\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_amazon.svg') }}\" alt=\"Amazon S3\"></span>
                    <strong>Amazon S3</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_android.svg') }}\" alt=\"Android\"></span>
                    <strong>Android</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_api.svg') }}\" alt=\"API\"></span>
                    <strong>API</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('asana_integration_page') }}\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_asana.svg') }}\" alt=\"Asana\"></span>
                    <strong>Asana</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('bitbucket_integration_page') }}\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_bitbucket.svg') }}\" alt=\"Bitbucket\"></span>
                    <strong>Bitbucket</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"/\" data-src=\"{{ asset('build/icon_int_cake_php.svg') }}\" alt=\"Cake PHP\"></span>
                    <strong>Cake PHP</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "amazon_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/amazon_integration.html.twig");
    }
}
