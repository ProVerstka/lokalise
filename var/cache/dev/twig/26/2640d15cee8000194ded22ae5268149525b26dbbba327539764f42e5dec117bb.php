<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product_for_developers.html.twig */
class __TwigTemplate_6d4a7137b5e090e6078c920950ae6fddf59f377f51e233b54b21287f6a4382c1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_developers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_developers.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "product_for_developers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_promo_title"), "html", null, true);
        echo "</h1>
        <picture class=\"image\">
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_02.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_02@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_02.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"/signup\" class=\"btn-primary btn-large\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preferred_title"), "html", null, true);
        echo "</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stripe.svg"), "html", null, true);
        echo "\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/yelp.svg"), "html", null, true);
        echo "\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lime.svg"), "html", null, true);
        echo "\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lemonade.svg"), "html", null, true);
        echo "\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h2 class=\"heading-h1\">";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_choose_title"), "html", null, true);
        echo "</h2>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_api_tool.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_brenching.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_sdk.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_text_3"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_platform.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_4"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_title_4"), "html", null, true);
        echo "</strong>
                <p>";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_developers_features_text_4"), "html", null, true);
        echo "</p>
            </li>
        </ul>
        <a href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("features_page");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_btn_text"), "html", null, true);
        echo "</a>
    </section>
    <section class=\"report\">
        <h2>";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_title"), "html", null, true);
        echo "</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_3"), "html", null, true);
        echo "</p>
            </li>
        </ul> 
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"import\">
        <h2>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_title"), "html", null, true);
        echo "</h2>
        <div class=\"tabset\">
            <div class=\"tabcontrol\">
                <div class=\"holder swiper-wrapper\">
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_apple.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_1"), "html", null, true);
        echo "\"></div>
                        <span class=\"title\">";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_1"), "html", null, true);
        echo "</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_android.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_2"), "html", null, true);
        echo "\"></div>
                        <span class=\"title\">";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_2"), "html", null, true);
        echo "</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_web.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_3"), "html", null, true);
        echo "\"></div>
                        <span class=\"title\">";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_3"), "html", null, true);
        echo "</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_other.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_4"), "html", null, true);
        echo "\"></div>
                        <span class=\"title\">";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("import_tab_title_4"), "html", null, true);
        echo "</span>
                    </div>
                </div>
            </div> 
            <div class=\"tabs\">
                <div class=\"tab active\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li>.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li class=\"checked\">.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li class=\"checked\">.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li >.strings</li>
                        <li>.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li class=\"checked\">.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li class=\"checked\">.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li class=\"checked\">.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class=\"speed-up\">
        <h2>";
        // line 316
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_title"), "html", null, true);
        echo "</h2>
        <ul class=\"speed-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 319
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_upload.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 320
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 321
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 324
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_invite.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 325
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 326
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 329
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_pull.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 330
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 331
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_3");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"integrate\">
        <h2 class=\"heading-h1\">";
        // line 336
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 337
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_text"), "html", null, true);
        echo "</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"";
        // line 340
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 341
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 342
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
            </picture>
        </div>
    </section>
    <section class=\"on-board\">
        <h2 class=\"heading-h1\">";
        // line 347
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_title");
        echo "</h2>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 351
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 352
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 353
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_1_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 357
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_1_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 358
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_1_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 359
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 365
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 366
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 367
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_2_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 371
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_2_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 372
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_item_2_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 373
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 378
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 379
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 380
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product_for_developers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  746 => 380,  742 => 379,  738 => 378,  730 => 373,  726 => 372,  722 => 371,  713 => 367,  709 => 366,  705 => 365,  696 => 359,  692 => 358,  688 => 357,  679 => 353,  675 => 352,  671 => 351,  664 => 347,  654 => 342,  650 => 341,  646 => 340,  640 => 337,  636 => 336,  628 => 331,  624 => 330,  618 => 329,  612 => 326,  608 => 325,  602 => 324,  596 => 321,  592 => 320,  586 => 319,  580 => 316,  370 => 109,  364 => 108,  358 => 105,  352 => 104,  346 => 101,  340 => 100,  334 => 97,  328 => 96,  320 => 91,  312 => 86,  308 => 85,  303 => 83,  295 => 80,  291 => 79,  287 => 78,  278 => 72,  274 => 71,  268 => 68,  264 => 67,  258 => 64,  254 => 63,  248 => 60,  240 => 57,  234 => 54,  230 => 53,  224 => 52,  218 => 49,  214 => 48,  208 => 47,  202 => 44,  198 => 43,  192 => 42,  186 => 39,  182 => 38,  176 => 37,  170 => 34,  163 => 30,  159 => 29,  155 => 28,  151 => 27,  147 => 26,  143 => 25,  138 => 23,  131 => 19,  126 => 17,  122 => 16,  117 => 14,  109 => 11,  105 => 10,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'product_for_developers_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'product_for_developers_promo_title'|trans }}</h1>
        <picture class=\"image\">
            <data-src srcset=\"{{asset('build/promo_02.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_02@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_02.png')}}\" alt=\"{{ 'product_for_developers_promo_title'|trans }}\">
        </picture>
        <div class=\"text\">
            <p>{{ 'product_for_developers_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"/signup\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>{{ 'preferred_title'|trans }}</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"{{asset('build/slack.svg')}}\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/revolut.svg')}}\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/stripe.svg')}}\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/yelp.svg')}}\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lime.svg')}}\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lemonade.svg')}}\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h2 class=\"heading-h1\">{{ 'product_for_developers_choose_title'|trans }}</h2>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_api_tool.svg')}}\" alt=\"{{ 'product_for_developers_features_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_developers_features_title_1'|trans }}</strong>
                <p>{{ 'product_for_developers_features_text_1'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_brenching.svg')}}\" alt=\"{{ 'product_for_developers_features_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_developers_features_title_2'|trans }}</strong>
                <p>{{ 'product_for_developers_features_text_2'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_sdk.svg')}}\" alt=\"{{ 'product_for_developers_features_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_developers_features_title_3'|trans }}</strong>
                <p>{{ 'product_for_developers_features_text_3'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_platform.svg')}}\" alt=\"{{ 'product_for_developers_features_title_4'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_developers_features_title_4'|trans }}</strong>
                <p>{{ 'product_for_developers_features_text_4'|trans }}</p>
            </li>
        </ul>
        <a href=\"{{ path('features_page') }}\" class=\"btn-primary btn-large\">{{ 'features_btn_text'|trans }}</a>
    </section>
    <section class=\"report\">
        <h2>{{ 'report_title'|trans }}</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">{{ 'report_list_title_1'|trans }}</strong>
                <p>{{ 'report_list_text_1'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_2'|trans }}</strong>
                <p>{{ 'report_list_text_2'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_3'|trans }}</strong>
                <p>{{ 'report_list_text_3'|trans }}</p>
            </li>
        </ul> 
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"import\">
        <h2>{{ 'import_title'|trans }}</h2>
        <div class=\"tabset\">
            <div class=\"tabcontrol\">
                <div class=\"holder swiper-wrapper\">
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_apple.svg')}}\" alt=\"{{ 'import_tab_title_1'|trans }}\"></div>
                        <span class=\"title\">{{ 'import_tab_title_1'|trans }}</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_android.svg')}}\" alt=\"{{ 'import_tab_title_2'|trans }}\"></div>
                        <span class=\"title\">{{ 'import_tab_title_2'|trans }}</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_web.svg')}}\" alt=\"{{ 'import_tab_title_3'|trans }}\"></div>
                        <span class=\"title\">{{ 'import_tab_title_3'|trans }}</span>
                    </div>
                    <div class=\"item swiper-slide\">
                        <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_other.svg')}}\" alt=\"{{ 'import_tab_title_4'|trans }}\"></div>
                        <span class=\"title\">{{ 'import_tab_title_4'|trans }}</span>
                    </div>
                </div>
            </div> 
            <div class=\"tabs\">
                <div class=\"tab active\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                        <li class=\"checked\">.strings</li>
                        <li class=\"checked\">.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li>.json</li>
                        <li>.php</li>
                        <li>.xlf</li>
                        <li>.po</li>
                        <li>.pot</li>
                        <li>.js</li>
                        <li>.xliff</li>
                        <li>.yml</li>
                        <li>.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li>.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li class=\"checked\">.strings</li>
                        <li>.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li class=\"checked\">.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li class=\"checked\">.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li>.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
                <div class=\"tab\">
                    <ul class=\"import-list\">
                        <li >.strings</li>
                        <li>.stringsdict</li>
                        <li class=\"checked\">.xliff</li>
                        <li class=\"checked\">.plist</li>
                        <li>.xml</li>
                        <li>.properties</li>
                        <li class=\"checked\">.json</li>
                        <li>.php</li>
                        <li class=\"checked\">.xlf</li>
                        <li>.po</li>
                        <li class=\"checked\">.pot</li>
                        <li>.js</li>
                        <li class=\"checked\">.xliff</li>
                        <li>.yml</li>
                        <li class=\"checked\">.yaml</li>
                        <li>.ini</li>
                        <li>.csv</li>
                        <li class=\"checked\">.xls</li>
                        <li>.xlsx</li>
                        <li>.resx</li>
                        <li>.stf</li>
                        <li>.ts</li>
                        <li>.i18n</li>
                        <li class=\"checked\">.html</li>
                        <li>.htm</li>
                        <li>.docx</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class=\"speed-up\">
        <h2>{{ 'speed_up_title'|trans }}</h2>
        <ul class=\"speed-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_upload.svg')}}\" alt=\"{{ 'speed_up_list_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_1'|trans }}</strong>
                <p>{{ 'speed_up_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_invite.svg')}}\" alt=\"{{ 'speed_up_list_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_2'|trans }}</strong>
                <p>{{ 'speed_up_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_pull.svg')}}\" alt=\"{{ 'speed_up_list_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_3'|trans }}</strong>
                <p>{{ 'speed_up_list_text_3'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"integrate\">
        <h2 class=\"heading-h1\">{{ 'integrate_title'|trans }}</h2>
        <p>{{ 'integrate_text'|trans }}</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"{{asset('build/img_01.png')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_01@2x.png')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_01.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
            </picture>
        </div>
    </section>
    <section class=\"on-board\">
        <h2 class=\"heading-h1\">{{ 'on_board_title'|trans|raw }}</h2>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_02.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_02@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_02.png')}}\" alt=\"{{ 'on_board_item_1_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'on_board_item_1_title'|trans }}</h3>
                <p>{{ 'on_board_item_1_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_03.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_03@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_03.png')}}\" alt=\"{{ 'on_board_item_2_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'on_board_item_2_title'|trans }}</h3>
                <p>{{ 'on_board_item_2_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "product_for_developers.html.twig", "/Users/user/dev/lokalize/lokalise/templates/product_for_developers.html.twig");
    }
}
