<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* careers.html.twig */
class __TwigTemplate_1cfdde9ae75e7c968947d6fc188b253c4d117a254ee1670db5c267172edcf810 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "careers.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "careers.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "careers.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_09.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_09@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_09.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <h1>";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_btn"), "html", null, true);
        echo "</a></li>
            </ul>
        </div> 
    </section>
    <section class=\"full-width-image\">
        <picture>
            <data-src srcset=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_39.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_39@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_39.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_promo_title"), "html", null, true);
        echo "\">
        </picture>
    </section>
    <section class=\"text-block\">
        <h2 class=\"align-center\">";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_1_title");
        echo "</h2>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_1_text_1");
        echo "</p>
            </div>
            <div class=\"col\">
                <p>";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_1_text_2");
        echo "</p>
            </div>  
        </div>
    </section>
    <section class=\"values bg-emptiness\">
        <picture class=\"image\">
            <data-src srcset=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_40.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_40@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_40.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_values_title"), "html", null, true);
        echo "\">
        </picture>
        <div class=\"text\">
            <h2>";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_values_title"), "html", null, true);
        echo "</h2>
            <p>";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_values_text");
        echo "</p>
        </div>
    </section> 
    <section class=\"text-block\">
        <h2 class=\"align-center\">";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_title");
        echo "</h2>
        <p class=\"align-center\">";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_text_1");
        echo "</p>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_text_2");
        echo "</p>
            </div>
            <div class=\"col\">
                <p>";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_text_3");
        echo "</p>
            </div>  
        </div>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_text_4");
        echo "</p>
            </div>
            <div class=\"col\">
                <p>";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_text_block_2_text_5");
        echo "</p>
            </div>  
        </div>
    </section>
    <section class=\"careers-list\">
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_1_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_1_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_2_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_2_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_3_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_3_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_4_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_4_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_5_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_5_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 112
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 117
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_6_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_6_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_7_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_7_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_8_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_8_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_9_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_item_9_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">";
        // line 148
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("careers_list_btn_apply"), "html", null, true);
        echo "</a>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 154
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 155
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "careers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  406 => 155,  402 => 154,  398 => 153,  390 => 148,  384 => 145,  380 => 144,  372 => 139,  366 => 136,  362 => 135,  354 => 130,  348 => 127,  344 => 126,  336 => 121,  330 => 118,  326 => 117,  318 => 112,  312 => 109,  308 => 108,  300 => 103,  294 => 100,  290 => 99,  282 => 94,  276 => 91,  272 => 90,  264 => 85,  258 => 82,  254 => 81,  246 => 76,  240 => 73,  236 => 72,  226 => 65,  220 => 62,  212 => 57,  206 => 54,  200 => 51,  196 => 50,  189 => 46,  185 => 45,  177 => 42,  173 => 41,  169 => 40,  160 => 34,  154 => 31,  148 => 28,  139 => 24,  135 => 23,  131 => 22,  122 => 16,  117 => 14,  112 => 12,  105 => 10,  101 => 9,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'careers_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"{{asset('build/promo_09.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_09@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_09.png')}}\" alt=\"{{ 'careers_promo_title'|trans }}\">
        </picture>
        <h1>{{ 'careers_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'careers_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'careers_promo_btn'|trans }}</a></li>
            </ul>
        </div> 
    </section>
    <section class=\"full-width-image\">
        <picture>
            <data-src srcset=\"{{asset('build/img_39.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/img_39@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/img_39.jpg')}}\" alt=\"{{ 'careers_promo_title'|trans }}\">
        </picture>
    </section>
    <section class=\"text-block\">
        <h2 class=\"align-center\">{{ 'careers_text_block_1_title'|trans|raw }}</h2>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>{{ 'careers_text_block_1_text_1'|trans|raw }}</p>
            </div>
            <div class=\"col\">
                <p>{{ 'careers_text_block_1_text_2'|trans|raw }}</p>
            </div>  
        </div>
    </section>
    <section class=\"values bg-emptiness\">
        <picture class=\"image\">
            <data-src srcset=\"{{asset('build/img_40.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/img_40@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/img_40.png')}}\" alt=\"{{ 'careers_values_title'|trans }}\">
        </picture>
        <div class=\"text\">
            <h2>{{ 'careers_values_title'|trans }}</h2>
            <p>{{ 'careers_values_text'|trans|raw }}</p>
        </div>
    </section> 
    <section class=\"text-block\">
        <h2 class=\"align-center\">{{ 'careers_text_block_2_title'|trans|raw }}</h2>
        <p class=\"align-center\">{{ 'careers_text_block_2_text_1'|trans|raw }}</p>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>{{ 'careers_text_block_2_text_2'|trans|raw }}</p>
            </div>
            <div class=\"col\">
                <p>{{ 'careers_text_block_2_text_3'|trans|raw }}</p>
            </div>  
        </div>
        <div class=\"text-columns\">
            <div class=\"col\">
                <p>{{ 'careers_text_block_2_text_4'|trans|raw }}</p>
            </div>
            <div class=\"col\">
                <p>{{ 'careers_text_block_2_text_5'|trans|raw }}</p>
            </div>  
        </div>
    </section>
    <section class=\"careers-list\">
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_1_title'|trans }}</strong>
                <p>{{ 'careers_list_item_1_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_2_title'|trans }}</strong>
                <p>{{ 'careers_list_item_2_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_3_title'|trans }}</strong>
                <p>{{ 'careers_list_item_3_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_4_title'|trans }}</strong>
                <p>{{ 'careers_list_item_4_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_5_title'|trans }}</strong>
                <p>{{ 'careers_list_item_5_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_6_title'|trans }}</strong>
                <p>{{ 'careers_list_item_6_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_7_title'|trans }}</strong>
                <p>{{ 'careers_list_item_7_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_8_title'|trans }}</strong>
                <p>{{ 'careers_list_item_8_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"text\">
                <strong class=\"title\">{{ 'careers_list_item_9_title'|trans }}</strong>
                <p>{{ 'careers_list_item_9_text'|trans }}</p>
            </div>
            <div class=\"link\">
                <a href=\"#\">{{ 'careers_list_btn_apply'|trans }}</a>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "careers.html.twig", "/Users/user/dev/lokalize/lokalise/templates/careers.html.twig");
    }
}
