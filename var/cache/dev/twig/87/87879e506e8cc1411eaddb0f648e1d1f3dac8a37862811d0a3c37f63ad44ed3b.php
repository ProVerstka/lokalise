<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* asana_integration.html.twig */
class __TwigTemplate_563b9b1499b48fe6f2a2d58832bb44c3ed5657a1676cbcc45a90f532ff41f724 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "asana_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "asana_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "asana_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_connect.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_invite_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_track.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_tools_list_text_3");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_text_block_title");
        echo "</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_3_title");
        echo "</strong>
                <p>";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_fuq_item_1_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 59
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_fuq_item_2_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_integration_fuq_item_2_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"managment bg-emptiness\">
        <h2 class=\"align-center heading-h1\">";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("asana_intergation_managment_title");
        echo "</h2>
        <ul class=\"app-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_jira.svg"), "html", null, true);
        echo "\" alt=\"Jira\"></div>
                <strong class=\"title\">Jira</strong>
            </li>
             <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_trello.svg"), "html", null, true);
        echo "\" alt=\"Trello\"></div>
                <strong class=\"title\">Trello</strong>
            </li>
        </ul> 
    </section>
   <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "asana_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 83,  270 => 82,  266 => 81,  257 => 75,  250 => 71,  244 => 68,  236 => 63,  232 => 62,  226 => 59,  222 => 58,  216 => 55,  208 => 50,  204 => 49,  198 => 46,  194 => 45,  188 => 42,  184 => 41,  178 => 38,  170 => 33,  166 => 32,  160 => 31,  154 => 28,  150 => 27,  144 => 26,  138 => 23,  134 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'asana_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'asana_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'asana_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'asana_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_connect.svg')}}\" alt=\"{{ 'asana_intergation_tools_list_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'asana_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'asana_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_invite_02.svg')}}\" alt=\"{{ 'asana_intergation_tools_list_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'asana_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'asana_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_track.svg')}}\" alt=\"{{ 'asana_intergation_tools_list_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'asana_intergation_tools_list_title_3'|trans }}</strong>
                <p>{{ 'asana_intergation_tools_list_text_3'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>{{ 'asana_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">{{ 'asana_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'asana_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'asana_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'asana_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'asana_intergation_privacy_list_item_3_title'|trans|raw }}</strong>
                <p>{{ 'asana_intergation_privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'asana_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'asana_integration_fuq_item_1_title'|trans }}</div>
                <div class=\"text\">{{ 'asana_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'asana_integration_fuq_item_2_title'|trans }}</div>
                <div class=\"text\">{{ 'asana_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"managment bg-emptiness\">
        <h2 class=\"align-center heading-h1\">{{ 'asana_intergation_managment_title'|trans|raw }}</h2>
        <ul class=\"app-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_jira.svg')}}\" alt=\"Jira\"></div>
                <strong class=\"title\">Jira</strong>
            </li>
             <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_trello.svg')}}\" alt=\"Trello\"></div>
                <strong class=\"title\">Trello</strong>
            </li>
        </ul> 
    </section>
   <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "asana_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/asana_integration.html.twig");
    }
}
