<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* deliver_to_end_users.html.twig */
class __TwigTemplate_41dfc76f3d9424d0098bfda7b8b8b573b7b34df21e060afaf1e7112fb45165a4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "deliver_to_end_users.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "deliver_to_end_users.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "deliver_to_end_users.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("deliver_to_end_users_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <div class=\"text\">
            <h1>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("deliver_to_end_users_promo_title"), "html", null, true);
        echo "</h1>
            <p>";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("deliver_to_end_users_promo_text");
        echo "</p>
        </div> 
    </section>
    <section class=\"explanation\">
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_32.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_32@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_32.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_24_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <h4>";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_24_title"), "html", null, true);
        echo "</h4>
            <p>";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_24_text"), "html", null, true);
        echo "</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_33.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_33@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_33.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_25_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <h4>";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_25_title"), "html", null, true);
        echo "</h4>
            <p>";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_25_text"), "html", null, true);
        echo "</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_34.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_34@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_34.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_26_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <h4>";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_26_title"), "html", null, true);
        echo "</h4>
            <p>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_26_text"), "html", null, true);
        echo "</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_35.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_35@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_35.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_27_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <h4>";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_27_title"), "html", null, true);
        echo "</h4>
            <p>";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("explanation_item_27_text"), "html", null, true);
        echo "</p>
        </div>
    </section>
    <section id=\"boost\">
        <h1>";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_title"), "html", null, true);
        echo "</h1>
        <p class=\"subtitle\">";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_subtitle"), "html", null, true);
        echo "</p>
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_developers.svg"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_text"), "html", null, true);
        echo "</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_product.svg"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_text"), "html", null, true);
        echo "</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translations.svg"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_text"), "html", null, true);
        echo "</p> 
            </li>
        </ul>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 86
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "deliver_to_end_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 96,  314 => 95,  310 => 94,  302 => 89,  298 => 88,  293 => 86,  285 => 83,  281 => 82,  277 => 81,  268 => 75,  264 => 74,  260 => 73,  254 => 70,  250 => 69,  246 => 68,  240 => 65,  236 => 64,  232 => 63,  226 => 60,  222 => 59,  215 => 55,  211 => 54,  203 => 51,  199 => 50,  195 => 49,  187 => 44,  183 => 43,  175 => 40,  171 => 39,  167 => 38,  159 => 33,  155 => 32,  147 => 29,  143 => 28,  139 => 27,  131 => 22,  127 => 21,  119 => 18,  115 => 17,  111 => 16,  101 => 9,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'deliver_to_end_users_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <div class=\"text\">
            <h1>{{ 'deliver_to_end_users_promo_title'|trans }}</h1>
            <p>{{ 'deliver_to_end_users_promo_text'|trans|raw }}</p>
        </div> 
    </section>
    <section class=\"explanation\">
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_32.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_32@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_32.png')}}\" alt=\"{{ 'explanation_item_24_title'|trans }}\">
                </picture>
            </div>
            <h4>{{ 'explanation_item_24_title'|trans }}</h4>
            <p>{{ 'explanation_item_24_text'|trans }}</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_33.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_33@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_33.png')}}\" alt=\"{{ 'explanation_item_25_title'|trans }}\">
                </picture>
            </div>
            <h4>{{ 'explanation_item_25_title'|trans }}</h4>
            <p>{{ 'explanation_item_25_text'|trans }}</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_34.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_34@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_34.png')}}\" alt=\"{{ 'explanation_item_26_title'|trans }}\">
                </picture>
            </div>
            <h4>{{ 'explanation_item_26_title'|trans }}</h4>
            <p>{{ 'explanation_item_26_text'|trans }}</p>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_35.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_35@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_35.png')}}\" alt=\"{{ 'explanation_item_27_title'|trans }}\">
                </picture>
            </div>
            <h4>{{ 'explanation_item_27_title'|trans }}</h4>
            <p>{{ 'explanation_item_27_text'|trans }}</p>
        </div>
    </section>
    <section id=\"boost\">
        <h1>{{ 'boost_title'|trans }}</h1>
        <p class=\"subtitle\">{{ 'boost_subtitle'|trans }}</p>
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_developers.svg')}}\"></div>  
                <h4>{{ 'boost_list_1_title'|trans }}</h4>
                <p>{{ 'boost_list_1_text'|trans }}</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_product.svg')}}\"></div>  
                <h4>{{ 'boost_list_2_title'|trans }}</h4>
                <p>{{ 'boost_list_2_text'|trans }}</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_translations.svg')}}\"></div>  
                <h4>{{ 'boost_list_3_title'|trans }}</h4>
                <p>{{ 'boost_list_3_text'|trans }}</p> 
            </li>
        </ul>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "deliver_to_end_users.html.twig", "/Users/user/dev/lokalize/lokalise/templates/deliver_to_end_users.html.twig");
    }
}
