<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* log_in.html.twig */
class __TwigTemplate_3e5015aef3135026bdc47c4f3133181049617a5412a926867adf8b3d3bbad082 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "log_in.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "log_in.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "log_in.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section class=\"form-block\">
        <div class=\"visual\">
            <picture class=\"\">
                <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_login.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_login@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_login.png"), "html", null, true);
        echo "\" alt=\"Log in\">
            </picture>
        </div>
        <div class=\"form\">
            <h1>";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_title"), "html", null, true);
        echo "</h1>
            <form action=\"#\" class=\"login-form\">
                <fieldset>
                    <div class=\"form-row\">
                        <button class=\"google\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_button_google"), "html", null, true);
        echo "</button>
                        <button class=\"github\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_button_github"), "html", null, true);
        echo "</button>
                        <button class=\"microsoft\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_button_microsoft"), "html", null, true);
        echo "</button>
                    </div>
                    <div class=\"note\"> 
                        <p>";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_note_text"), "html", null, true);
        echo "</p>
                    </div>
                    <div class=\"form-row\">
                        <label>";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_lable_1_text"), "html", null, true);
        echo "</label>
                        <input type=\"text\" placeholder=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_input_1_placeholder"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-row\">
                        <label>";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_lable_2_text"), "html", null, true);
        echo "</label>
                        <input type=\"password\" placeholder=\"password\">
                    </div>
                    <div class=\"form-row\">
                        <input type=\"submit\" class=\"btn-primary\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_button_text"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-row align-center login-with\">
                        <a href=\"#\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_sso_text"), "html", null, true);
        echo "</a>
                    </div>
                    <div class=\"form-row\">
                        <ul class=\"form-links\">
                            <li><a href=\"";
        // line 42
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_register_text"), "html", null, true);
        echo "</a></li>
                            <li><a href=\"#\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("login_forgot_text"), "html", null, true);
        echo "</a></li>
                        </ul>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "log_in.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 43,  170 => 42,  163 => 38,  157 => 35,  150 => 31,  144 => 28,  140 => 27,  134 => 24,  128 => 21,  124 => 20,  120 => 19,  113 => 15,  106 => 11,  102 => 10,  98 => 9,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'login_title'|trans }}
{% endblock %}
{% block body %}
    <section class=\"form-block\">
        <div class=\"visual\">
            <picture class=\"\">
                <data-src srcset=\"{{asset('build/img_login.png')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_login@2x.png')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_login.png')}}\" alt=\"Log in\">
            </picture>
        </div>
        <div class=\"form\">
            <h1>{{ 'login_title'|trans }}</h1>
            <form action=\"#\" class=\"login-form\">
                <fieldset>
                    <div class=\"form-row\">
                        <button class=\"google\">{{ 'login_button_google'|trans }}</button>
                        <button class=\"github\">{{ 'login_button_github'|trans }}</button>
                        <button class=\"microsoft\">{{ 'login_button_microsoft'|trans }}</button>
                    </div>
                    <div class=\"note\"> 
                        <p>{{ 'login_note_text'|trans }}</p>
                    </div>
                    <div class=\"form-row\">
                        <label>{{ 'login_lable_1_text'|trans }}</label>
                        <input type=\"text\" placeholder=\"{{ 'login_input_1_placeholder'|trans }}\">
                    </div>
                    <div class=\"form-row\">
                        <label>{{ 'login_lable_2_text'|trans }}</label>
                        <input type=\"password\" placeholder=\"password\">
                    </div>
                    <div class=\"form-row\">
                        <input type=\"submit\" class=\"btn-primary\" value=\"{{ 'login_button_text'|trans }}\">
                    </div>
                    <div class=\"form-row align-center login-with\">
                        <a href=\"#\">{{ 'login_sso_text'|trans }}</a>
                    </div>
                    <div class=\"form-row\">
                        <ul class=\"form-links\">
                            <li><a href=\"{{ path('signup_page') }}\">{{ 'login_register_text'|trans }}</a></li>
                            <li><a href=\"#\">{{ 'login_forgot_text'|trans }}</a></li>
                        </ul>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
{% endblock %}", "log_in.html.twig", "/Users/user/dev/lokalize/lokalise/templates/log_in.html.twig");
    }
}
