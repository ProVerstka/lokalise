<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* features.html.twig */
class __TwigTemplate_633af07b231faf1328f20871f1bbc62dea2eb9ce7526d8945708647be93e8eb0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "features.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "features.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "features.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section id=\"boost\" class=\"bg-coral no-margin\">
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_developers.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_title"), "html", null, true);
        echo "</h4>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_product.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_title"), "html", null, true);
        echo "</h4>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translations.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_title"), "html", null, true);
        echo "</h4>
            </li>
        </ul>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_title"), "html", null, true);
        echo "</h2>
            <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_text"), "html", null, true);
        echo "</p>
            <a href=\"#\" class=\"more\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_link_more"), "html", null, true);
        echo "</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_01.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_1_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_1_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_1_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_2_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_2_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_2_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_03.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_3_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_3_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_3_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_04.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_4_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_4_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_1_item_4_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 77
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Rebolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_title"), "html", null, true);
        echo "</h2>
            <p>";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_text"), "html", null, true);
        echo "</p>
            <a href=\"#\" class=\"more\">";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_link_more"), "html", null, true);
        echo "</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_05.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_1_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_1_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_1_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_06.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_2_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_2_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_2_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_07.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_3_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_3_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_3_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_08.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_4_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_4_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_2_item_4_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"client-review left-to-right bg-salmon\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_title"), "html", null, true);
        echo "</h2>
            <p>";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_text"), "html", null, true);
        echo "</p>
            <a href=\"#\" class=\"more\">";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_link_more"), "html", null, true);
        echo "</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_09.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_1_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_1_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 146
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_1_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_10.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_2_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_2_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_2_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_11.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_3_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_3_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_3_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_features_12.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_4_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_4_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_details_block_3_item_4_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 173
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 174
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 175
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "features.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  508 => 175,  504 => 174,  500 => 173,  491 => 167,  487 => 166,  480 => 164,  473 => 160,  469 => 159,  462 => 157,  455 => 153,  451 => 152,  444 => 150,  437 => 146,  433 => 145,  426 => 143,  419 => 139,  415 => 138,  411 => 137,  402 => 131,  398 => 130,  393 => 128,  385 => 125,  381 => 124,  377 => 123,  367 => 116,  363 => 115,  356 => 113,  349 => 109,  345 => 108,  338 => 106,  331 => 102,  327 => 101,  320 => 99,  313 => 95,  309 => 94,  302 => 92,  295 => 88,  291 => 87,  287 => 86,  278 => 80,  274 => 79,  269 => 77,  261 => 74,  257 => 73,  253 => 72,  243 => 65,  239 => 64,  232 => 62,  225 => 58,  221 => 57,  214 => 55,  207 => 51,  203 => 50,  196 => 48,  189 => 44,  185 => 43,  178 => 41,  171 => 37,  167 => 36,  163 => 35,  154 => 29,  148 => 28,  142 => 25,  136 => 24,  130 => 21,  124 => 20,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'features_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'features_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'features_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section id=\"boost\" class=\"bg-coral no-margin\">
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_developers.svg')}}\" alt=\"{{ 'boost_list_1_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_1_title'|trans }}</h4>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_product.svg')}}\" alt=\"{{ 'boost_list_2_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_2_title'|trans }}</h4>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_translations.svg')}}\" alt=\"{{ 'boost_list_3_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_3_title'|trans }}</h4>
            </li>
        </ul>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>{{ 'features_details_block_1_title'|trans }}</h2>
            <p>{{ 'features_details_block_1_text'|trans }}</p>
            <a href=\"#\" class=\"more\">{{ 'features_details_block_1_link_more'|trans }}</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_01.svg')}}\" alt=\"{{ 'features_details_block_1_item_1_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_1_item_1_title'|trans }}</strong>
                    <p>{{ 'features_details_block_1_item_1_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_02.svg')}}\" alt=\"{{ 'features_details_block_1_item_2_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_1_item_2_title'|trans }}</strong>
                    <p>{{ 'features_details_block_1_item_2_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_03.svg')}}\" alt=\"{{ 'features_details_block_1_item_3_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_1_item_3_title'|trans }}</strong>
                    <p>{{ 'features_details_block_1_item_3_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_04.svg')}}\" alt=\"{{ 'features_details_block_1_item_4_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_1_item_4_title'|trans }}</strong>
                    <p>{{ 'features_details_block_1_item_4_text'|trans }}</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Rebolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>{{ 'features_details_block_2_title'|trans }}</h2>
            <p>{{ 'features_details_block_2_text'|trans }}</p>
            <a href=\"#\" class=\"more\">{{ 'features_details_block_2_link_more'|trans }}</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_05.svg')}}\" alt=\"{{ 'features_details_block_2_item_1_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_2_item_1_title'|trans }}</strong>
                    <p>{{ 'features_details_block_2_item_1_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_06.svg')}}\" alt=\"{{ 'features_details_block_2_item_2_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_2_item_2_title'|trans }}</strong>
                    <p>{{ 'features_details_block_2_item_2_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_07.svg')}}\" alt=\"{{ 'features_details_block_2_item_3_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_2_item_3_title'|trans }}</strong>
                    <p>{{ 'features_details_block_2_item_3_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_08.svg')}}\" alt=\"{{ 'features_details_block_2_item_4_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_2_item_4_title'|trans }}</strong>
                    <p>{{ 'features_details_block_2_item_4_text'|trans }}</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"client-review left-to-right bg-salmon\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"features-details\">
        <header>
            <h2>{{ 'features_details_block_3_title'|trans }}</h2>
            <p>{{ 'features_details_block_3_text'|trans }}</p>
            <a href=\"#\" class=\"more\">{{ 'features_details_block_3_link_more'|trans }}</a>
        </header>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_09.svg')}}\" alt=\"{{ 'features_details_block_3_item_1_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_3_item_1_title'|trans }}</strong>
                    <p>{{ 'features_details_block_3_item_1_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_10.svg')}}\" alt=\"{{ 'features_details_block_3_item_2_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_3_item_2_title'|trans }}</strong>
                    <p>{{ 'features_details_block_3_item_2_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_11.svg')}}\" alt=\"{{ 'features_details_block_3_item_3_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_3_item_3_title'|trans }}</strong>
                    <p>{{ 'features_details_block_3_item_3_text'|trans }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_features_12.svg')}}\" alt=\"{{ 'features_details_block_3_item_4_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'features_details_block_3_item_4_title'|trans }}</strong>
                    <p>{{ 'features_details_block_3_item_4_text'|trans }}</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "features.html.twig", "/Users/user/dev/lokalize/lokalise/templates/features.html.twig");
    }
}
