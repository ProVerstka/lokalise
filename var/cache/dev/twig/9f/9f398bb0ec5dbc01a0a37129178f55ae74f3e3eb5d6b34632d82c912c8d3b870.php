<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* team.html.twig */
class __TwigTemplate_907d5b366016552ddf8dcf834370b6f5ae5cae0c25326580f92e395d0ec2979d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "team.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "team.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "team.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/hello_animation.gif"), "html", null, true);
        echo "\" width=\"276\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <h1>";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_promo_title"), "html", null, true);
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_promo_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"report low-paddings\">
        <h2>";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_report_title"), "html", null, true);
        echo "</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_3"), "html", null, true);
        echo "</p>
            </li>
        </ul> 
    </section>
    <section class=\"team-block\">
        <ul class=\"team-list\">
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_02.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_02@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_02.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_1_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_1_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_1_post"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_03.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_03@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_03.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_2_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_2_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_2_post"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_04.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_04@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_04.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_3_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_3_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_3_post"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_05.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_05@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_05.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_4_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_4_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_4_post"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_06.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_06@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_06.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_5_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_5_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_5_post"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_07.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_07@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                        <data-img src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_07.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_6_name"), "html", null, true);
        echo "\">
                    </picture>
                </div>
                <strong class=\"name\">";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_6_name"), "html", null, true);
        echo "</strong>
                <p>";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("team_list_item_6_post"), "html", null, true);
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 105
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "team.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 105,  330 => 104,  326 => 103,  318 => 98,  314 => 97,  306 => 94,  302 => 93,  298 => 92,  290 => 87,  286 => 86,  278 => 83,  274 => 82,  270 => 81,  262 => 76,  258 => 75,  250 => 72,  246 => 71,  242 => 70,  234 => 65,  230 => 64,  222 => 61,  218 => 60,  214 => 59,  206 => 54,  202 => 53,  194 => 50,  190 => 49,  186 => 48,  178 => 43,  174 => 42,  166 => 39,  162 => 38,  158 => 37,  146 => 28,  142 => 27,  136 => 24,  132 => 23,  126 => 20,  122 => 19,  116 => 16,  109 => 12,  104 => 10,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'team_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-img src=\"{{asset('build/hello_animation.gif')}}\" width=\"276\" alt=\"{{ 'team_promo_title'|trans }}\">
        </picture>
        <h1>{{ 'team_promo_title'|trans }}</h1>
        <div class=\"text\">
            <p>{{ 'team_promo_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"report low-paddings\">
        <h2>{{ 'team_report_title'|trans }}</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">{{ 'report_list_title_1'|trans }}</strong>
                <p>{{ 'report_list_text_1'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_2'|trans }}</strong>
                <p>{{ 'report_list_text_2'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_3'|trans }}</strong>
                <p>{{ 'report_list_text_3'|trans }}</p>
            </li>
        </ul> 
    </section>
    <section class=\"team-block\">
        <ul class=\"team-list\">
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_02.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_02@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_02.jpg')}}\" alt=\"{{ 'team_list_item_1_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_1_name'|trans }}</strong>
                <p>{{ 'team_list_item_1_post'|trans }}</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_03.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_03@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_03.jpg')}}\" alt=\"{{ 'team_list_item_2_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_2_name'|trans }}</strong>
                <p>{{ 'team_list_item_2_post'|trans }}</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_04.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_04@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_04.jpg')}}\" alt=\"{{ 'team_list_item_3_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_3_name'|trans }}</strong>
                <p>{{ 'team_list_item_3_post'|trans }}</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_05.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_05@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_05.jpg')}}\" alt=\"{{ 'team_list_item_4_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_4_name'|trans }}</strong>
                <p>{{ 'team_list_item_4_post'|trans }}</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_06.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_06@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_06.jpg')}}\" alt=\"{{ 'team_list_item_5_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_5_name'|trans }}</strong>
                <p>{{ 'team_list_item_5_post'|trans }}</p>
            </li>
            <li>
                <div class=\"photo\">
                    <picture>
                        <data-src srcset=\"{{asset('build/photo_07.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                        <data-src srcset=\"{{asset('build/photo_07@2x.jpg')}} 2x\"></data-src>
                        <data-img src=\"{{asset('build/photo_07.jpg')}}\" alt=\"{{ 'team_list_item_6_name'|trans }}\">
                    </picture>
                </div>
                <strong class=\"name\">{{ 'team_list_item_6_name'|trans }}</strong>
                <p>{{ 'team_list_item_6_post'|trans }}</p>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "team.html.twig", "/Users/user/dev/lokalize/lokalise/templates/team.html.twig");
    }
}
