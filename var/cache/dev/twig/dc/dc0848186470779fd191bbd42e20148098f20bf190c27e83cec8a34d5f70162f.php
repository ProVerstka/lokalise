<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* zendesk_integration.html.twig */
class __TwigTemplate_caa71aa3a297ec2f4c25bdfcada37f2c9bb96a8dbf59a5119add86c7a87598bd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "zendesk_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "zendesk_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "zendesk_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_free.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_one_minute.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_no_register.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_tools_list_text_3");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"estabilish\">
        <h2 class=\"heading-h1\">";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_title");
        echo "</h2>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                    <source srcset=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44@2x.png"), "html", null, true);
        echo " 2x\">
                    <img srcset=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_item_1_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                    <source srcset=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44@2x.png"), "html", null, true);
        echo " 2x\">
                    <img srcset=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_item_2_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                    <source srcset=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44@2x.png"), "html", null, true);
        echo " 2x\">
                    <img srcset=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>";
        // line 72
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_item_3_text");
        echo "</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                    <source srcset=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44@2x.png"), "html", null, true);
        echo " 2x\">
                    <img srcset=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_44.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_estabilish_item_4_text");
        echo "</p>
            </div>
        </div>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_text_block_title");
        echo "</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("zendesk_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 102
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 104
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "zendesk_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 104,  314 => 103,  310 => 102,  302 => 97,  298 => 96,  292 => 93,  288 => 92,  282 => 89,  274 => 84,  265 => 80,  261 => 79,  257 => 78,  248 => 72,  239 => 68,  235 => 67,  231 => 66,  222 => 60,  213 => 56,  209 => 55,  205 => 54,  196 => 48,  187 => 44,  183 => 43,  179 => 42,  172 => 38,  164 => 33,  160 => 32,  156 => 31,  150 => 28,  146 => 27,  142 => 26,  136 => 23,  132 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'zendesk_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'zendesk_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'zendesk_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'zendesk_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_free.svg')}}\"></div>
                <strong class=\"title\">{{ 'zendesk_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'zendesk_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_one_minute.svg')}}\"></div>
                <strong class=\"title\">{{ 'zendesk_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'zendesk_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_no_register.svg')}}\"></div>
                <strong class=\"title\">{{ 'zendesk_intergation_tools_list_title_3'|trans }}</strong>
                <p>{{ 'zendesk_intergation_tools_list_text_3'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"estabilish\">
        <h2 class=\"heading-h1\">{{ 'zendesk_intergation_estabilish_title'|trans|raw }}</h2>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"{{asset('build/img_44.png')}}\" media=\"(max-width: 768px)\">
                    <source srcset=\"{{asset('build/img_44@2x.png')}} 2x\">
                    <img srcset=\"{{asset('build/img_44.png')}}\" alt=\"{{ 'zendesk_intergation_estabilish_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>{{ 'zendesk_intergation_estabilish_item_1_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"{{asset('build/img_44.png')}}\" media=\"(max-width: 768px)\">
                    <source srcset=\"{{asset('build/img_44@2x.png')}} 2x\">
                    <img srcset=\"{{asset('build/img_44.png')}}\" alt=\"{{ 'zendesk_intergation_estabilish_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>{{ 'zendesk_intergation_estabilish_item_2_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"{{asset('build/img_44.png')}}\" media=\"(max-width: 768px)\">
                    <source srcset=\"{{asset('build/img_44@2x.png')}} 2x\">
                    <img srcset=\"{{asset('build/img_44.png')}}\" alt=\"{{ 'zendesk_intergation_estabilish_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>{{ 'zendesk_intergation_estabilish_item_3_text'|trans|raw }}</p>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <source srcset=\"{{asset('build/img_44.png')}}\" media=\"(max-width: 768px)\">
                    <source srcset=\"{{asset('build/img_44@2x.png')}} 2x\">
                    <img srcset=\"{{asset('build/img_44.png')}}\" alt=\"{{ 'zendesk_intergation_estabilish_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text large\">
                <p>{{ 'zendesk_intergation_estabilish_item_4_text'|trans|raw }}</p>
            </div>
        </div>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>{{ 'zendesk_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">{{ 'zendesk_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'zendesk_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'zendesk_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'zendesk_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "zendesk_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/zendesk_integration.html.twig");
    }
}
