<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sketch_integration.html.twig */
class __TwigTemplate_5da8b55263dfb595db25f6b78c0c6f9c5e492e774ba5e24defdc6496205c2247 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sketch_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sketch_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "sketch_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_upload_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translate_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_preview.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_text_3");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_export.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_4"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_title_4"), "html", null, true);
        echo "</strong>
                <p>";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_tools_list_text_4");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_text_block_title");
        echo "</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_3_title");
        echo "</strong>
                <p>";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_intergation_privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_title");
        echo "</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_45.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                <source srcset=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_45@2x.png"), "html", null, true);
        echo " 2x\">
                <img srcset=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_45.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_title"), "html", null, true);
        echo "\">
            </picture>
        </div>
        <div class=\"two-columns\">
            <div class=\"col\">
                <h3>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_1_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_1_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col\">
                <h3>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_2_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_2_text"), "html", null, true);
        echo "</p>
            </div>
        </div>
        <div class=\"two-columns\">
            <div class=\"col\">
                <h3>";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_3_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_3_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col\">
                <h3>";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_4_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_item_4_text"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">";
        // line 90
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_title_2");
        echo "</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_46.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                <source srcset=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_46@2x.png"), "html", null, true);
        echo " 2x\">
                <img srcset=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_46.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_info_block_title_2"), "html", null, true);
        echo "\">
            </picture>
        </div>
        <div class=\"align-center\">
             <li><a href=\"#\" class=\"btn-primary btn-large\">";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_btn_learn_more"), "html", null, true);
        echo "</a></li>
        </div>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 103
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_1_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 107
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_2_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_2_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_3_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 115
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_3_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_4_title"), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_fuq_item_4_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">";
        // line 124
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sketch_integration_connect_title");
        echo "</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"";
        // line 127
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("amazon_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_amazon.svg"), "html", null, true);
        echo "\" alt=\"Amazon S3\"></span>
                    <strong>Amazon S3</strong>
                    <span>";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_android.svg"), "html", null, true);
        echo "\" alt=\"Android\"></span>
                    <strong>Android</strong>
                    <span>";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_api.svg"), "html", null, true);
        echo "\" alt=\"API\"></span>
                    <strong>API</strong>
                    <span>";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 148
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("asana_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_asana.svg"), "html", null, true);
        echo "\" alt=\"Asana\"></span>
                    <strong>Asana</strong>
                    <span>";
        // line 151
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 155
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbucket_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_bitbucket.svg"), "html", null, true);
        echo "\" alt=\"Bitbucket\"></span>
                    <strong>Bitbucket</strong>
                    <span>";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_cake_php.svg"), "html", null, true);
        echo "\" alt=\"Cake PHP\"></span>
                    <strong>Cake PHP</strong>
                    <span>";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
        </ul>
    </section>
   <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 171
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 172
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 173
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "sketch_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  481 => 173,  477 => 172,  473 => 171,  464 => 165,  459 => 163,  451 => 158,  446 => 156,  442 => 155,  435 => 151,  430 => 149,  426 => 148,  419 => 144,  414 => 142,  406 => 137,  401 => 135,  393 => 130,  388 => 128,  384 => 127,  378 => 124,  370 => 119,  366 => 118,  360 => 115,  356 => 114,  350 => 111,  346 => 110,  340 => 107,  336 => 106,  330 => 103,  323 => 99,  314 => 95,  310 => 94,  306 => 93,  300 => 90,  292 => 85,  288 => 84,  282 => 81,  278 => 80,  270 => 75,  266 => 74,  260 => 71,  256 => 70,  246 => 65,  242 => 64,  238 => 63,  232 => 60,  224 => 55,  220 => 54,  214 => 51,  210 => 50,  204 => 47,  200 => 46,  194 => 43,  186 => 38,  182 => 37,  176 => 36,  170 => 33,  166 => 32,  160 => 31,  154 => 28,  150 => 27,  144 => 26,  138 => 23,  134 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'sketch_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'sketch_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'sketch_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'sketch_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_upload_02.svg')}}\" alt=\"{{ 'sketch_intergation_tools_list_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'sketch_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'sketch_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_translate_02.svg')}}\" alt=\"{{ 'sketch_intergation_tools_list_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'sketch_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'sketch_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_preview.svg')}}\" alt=\"{{ 'sketch_intergation_tools_list_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'sketch_intergation_tools_list_title_3'|trans }}</strong>
                <p>{{ 'sketch_intergation_tools_list_text_3'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_export.svg')}}\" alt=\"{{ 'sketch_intergation_tools_list_title_4'|trans }}\"></div>
                <strong class=\"title\">{{ 'sketch_intergation_tools_list_title_4'|trans }}</strong>
                <p>{{ 'sketch_intergation_tools_list_text_4'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"additional-block bg-emptiness\">
        <h2>{{ 'sketch_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"additional-list\">   
            <li>
                <strong class=\"title\">{{ 'sketch_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'sketch_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'sketch_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'sketch_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'sketch_intergation_privacy_list_item_3_title'|trans|raw }}</strong>
                <p>{{ 'sketch_intergation_privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">{{ 'sketch_info_block_title'|trans|raw }}</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"{{asset('build/img_45.png')}}\" media=\"(max-width: 768px)\">
                <source srcset=\"{{asset('build/img_45@2x.png')}} 2x\">
                <img srcset=\"{{asset('build/img_45.png')}}\" alt=\"{{ 'sketch_info_block_title'|trans }}\">
            </picture>
        </div>
        <div class=\"two-columns\">
            <div class=\"col\">
                <h3>{{ 'sketch_info_block_item_1_title'|trans }}</h3>
                <p>{{ 'sketch_info_block_item_1_text'|trans }}</p>
            </div>
            <div class=\"col\">
                <h3>{{ 'sketch_info_block_item_2_title'|trans }}</h3>
                <p>{{ 'sketch_info_block_item_2_text'|trans }}</p>
            </div>
        </div>
        <div class=\"two-columns\">
            <div class=\"col\">
                <h3>{{ 'sketch_info_block_item_3_title'|trans }}</h3>
                <p>{{ 'sketch_info_block_item_3_text'|trans }}</p>
            </div>
            <div class=\"col\">
                <h3>{{ 'sketch_info_block_item_4_title'|trans }}</h3>
                <p>{{ 'sketch_info_block_item_4_text'|trans }}</p>
            </div>
        </div>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">{{ 'sketch_info_block_title_2'|trans|raw }}</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"{{asset('build/img_46.png')}}\" media=\"(max-width: 768px)\">
                <source srcset=\"{{asset('build/img_46@2x.png')}} 2x\">
                <img srcset=\"{{asset('build/img_46.png')}}\" alt=\"{{ 'sketch_info_block_title_2'|trans }}\">
            </picture>
        </div>
        <div class=\"align-center\">
             <li><a href=\"#\" class=\"btn-primary btn-large\">{{ 'sketch_btn_learn_more'|trans }}</a></li>
        </div>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'sketch_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'sketch_integration_fuq_item_1_title'|trans }}</div>
                <div class=\"text\">{{ 'sketch_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'sketch_integration_fuq_item_2_title'|trans }}</div>
                <div class=\"text\">{{ 'sketch_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'sketch_integration_fuq_item_3_title'|trans }}</div>
                <div class=\"text\">{{ 'sketch_integration_fuq_item_3_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'sketch_integration_fuq_item_4_title'|trans }}</div>
                <div class=\"text\">{{ 'sketch_integration_fuq_item_4_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">{{ 'sketch_integration_connect_title'|trans|raw }}</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"{{ path('amazon_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_amazon.svg') }}\" alt=\"Amazon S3\"></span>
                    <strong>Amazon S3</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_android.svg') }}\" alt=\"Android\"></span>
                    <strong>Android</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_api.svg') }}\" alt=\"API\"></span>
                    <strong>API</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('asana_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_asana.svg') }}\" alt=\"Asana\"></span>
                    <strong>Asana</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('bitbucket_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_bitbucket.svg') }}\" alt=\"Bitbucket\"></span>
                    <strong>Bitbucket</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_cake_php.svg') }}\" alt=\"Cake PHP\"></span>
                    <strong>Cake PHP</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
        </ul>
    </section>
   <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "sketch_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/sketch_integration.html.twig");
    }
}
