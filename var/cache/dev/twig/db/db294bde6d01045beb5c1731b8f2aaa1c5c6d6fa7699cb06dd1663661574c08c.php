<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security.html.twig */
class __TwigTemplate_847ec8ffd921e54bc69e34f0c1625b6ee700323f6f4b2a6e978287894840c225 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("secutiry_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_05.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_05@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_05.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("secutiry_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <h1>";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("secutiry_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 14
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("secutiry_promo_text");
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
            </ul>
        </div> 
    </section>
    <section class=\"privacy-block\">
        <ul class=\"privacy-list\">   
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_certificate.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_title"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_privaci.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_title"), "html", null, true);
        echo "\"></div>
                 <strong class=\"title\">";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_gpr.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_title"), "html", null, true);
        echo "\"></div>
                 <strong class=\"title\">";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"security-info\">
        <ul class=\"security-info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_monitored.png"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_1_text");
        echo "\"></div>
                <p>";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_1_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_uptime.svg"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_2_text");
        echo "\"></div>
                <p>";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_2_text");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_sound.svg"), "html", null, true);
        echo "\" alt=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_3_text");
        echo "\"></div>
                <p>";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_info_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"security\">
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_Information_security.svg"), "html", null, true);
        echo "\" alt=\"{ 'security_item_1_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_1_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 61
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_1_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_human_resources.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_2_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_2_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 68
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_2_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_access_control.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_3_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_3_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_3_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_cryptography.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_4_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_4_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 82
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_4_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_physical.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_5_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 88
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_5_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_5_text");
        echo "</p>
                </div>
            </div>
        </div>
        <div class=\"text-block\">
            <h2 class=\"align-center heading-h1\">";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_title"), "html", null, true);
        echo "</h2>
            <div class=\"three-columns\">
                <div class=\"col\">
                    <strong class=\"title\">";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_1_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_1_text");
        echo "</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_2_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 102
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_2_text");
        echo "</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_3_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 106
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_3_text");
        echo "</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_4_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 110
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_4_text");
        echo "</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">";
        // line 113
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_5_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 114
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_text_block_item_5_text");
        echo "</p>
                </div>
            </div>
        </div>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_system_development.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_6_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_6_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 123
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_6_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_supplier_relations.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_7_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_7_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 130
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_7_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_business_continuity.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_8_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_8_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 137
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_8_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_governance.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_9_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_9_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 144
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_9_text");
        echo "</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_what_happens.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_10_title"), "html", null, true);
        echo "\"></div>
                <div class=\"text\">
                    <strong class=\"title\">";
        // line 150
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_10_title"), "html", null, true);
        echo "</strong>
                    <p>";
        // line 151
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security_item_10_text");
        echo "</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"block-note align-center bg-exballium\">
        <p>";
        // line 157
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("secure_note_text");
        echo "</p>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 162,  476 => 161,  472 => 160,  466 => 157,  457 => 151,  453 => 150,  446 => 148,  439 => 144,  435 => 143,  428 => 141,  421 => 137,  417 => 136,  410 => 134,  403 => 130,  399 => 129,  392 => 127,  385 => 123,  381 => 122,  374 => 120,  365 => 114,  361 => 113,  355 => 110,  351 => 109,  345 => 106,  341 => 105,  335 => 102,  331 => 101,  325 => 98,  321 => 97,  315 => 94,  307 => 89,  303 => 88,  296 => 86,  289 => 82,  285 => 81,  278 => 79,  271 => 75,  267 => 74,  260 => 72,  253 => 68,  249 => 67,  242 => 65,  235 => 61,  231 => 60,  226 => 58,  216 => 51,  210 => 50,  204 => 47,  198 => 46,  192 => 43,  186 => 42,  176 => 35,  172 => 34,  166 => 33,  160 => 30,  156 => 29,  150 => 28,  144 => 25,  140 => 24,  134 => 23,  122 => 16,  117 => 14,  112 => 12,  105 => 10,  101 => 9,  97 => 8,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'secutiry_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <picture class=\"image mb-order-1\">
            <data-src srcset=\"{{asset('build/promo_05.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_05@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_05.png')}}\" alt=\"{{ 'secutiry_promo_title'|trans }}\">
        </picture>
        <h1>{{ 'secutiry_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'secutiry_promo_text'|trans|raw }}</p>
            <ul class=\"buttons\">
                <li><a href=\"{{ path('signup_page') }}\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
            </ul>
        </div> 
    </section>
    <section class=\"privacy-block\">
        <ul class=\"privacy-list\">   
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_certificate.svg')}}\" alt=\"{{ 'privacy_list_item_1_title'|trans }}\"></div>
                <strong class=\"title\">{{ 'privacy_list_item_1_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_privaci.svg')}}\" alt=\"{{ 'privacy_list_item_2_title'|trans }}\"></div>
                 <strong class=\"title\">{{ 'privacy_list_item_2_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_gpr.svg')}}\" alt=\"{{ 'privacy_list_item_3_title'|trans }}\"></div>
                 <strong class=\"title\">{{ 'privacy_list_item_3_title'|trans }}</strong>
                <p>{{ 'privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"security-info\">
        <ul class=\"security-info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_monitored.png')}}\" alt=\"{{ 'security_info_list_item_1_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_uptime.svg')}}\" alt=\"{{ 'security_info_list_item_2_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_sound.svg')}}\" alt=\"{{ 'security_info_list_item_3_text'|trans|raw }}\"></div>
                <p>{{ 'security_info_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"security\">
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_Information_security.svg')}}\" alt=\"{ 'security_item_1_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_1_title'|trans }}</strong>
                    <p>{{ 'security_item_1_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_human_resources.svg')}}\" alt=\"{{ 'security_item_2_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_2_title'|trans }}</strong>
                    <p>{{ 'security_item_2_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_access_control.svg')}}\" alt=\"{{ 'security_item_3_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_3_title'|trans }}</strong>
                    <p>{{ 'security_item_3_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_cryptography.svg')}}\" alt=\"{{ 'security_item_4_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_4_title'|trans }}</strong>
                    <p>{{ 'security_item_4_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_physical.svg')}}\" alt=\"{{ 'security_item_5_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_5_title'|trans }}</strong>
                    <p>{{ 'security_item_5_text'|trans|raw }}</p>
                </div>
            </div>
        </div>
        <div class=\"text-block\">
            <h2 class=\"align-center heading-h1\">{{ 'security_text_block_title'|trans }}</h2>
            <div class=\"three-columns\">
                <div class=\"col\">
                    <strong class=\"title\">{{ 'security_text_block_item_1_title'|trans }}</strong>
                    <p>{{ 'security_text_block_item_1_text'|trans|raw }}</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">{{ 'security_text_block_item_2_title'|trans }}</strong>
                    <p>{{ 'security_text_block_item_2_text'|trans|raw }}</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">{{ 'security_text_block_item_3_title'|trans }}</strong>
                    <p>{{ 'security_text_block_item_3_text'|trans|raw }}</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">{{ 'security_text_block_item_4_title'|trans }}</strong>
                    <p>{{ 'security_text_block_item_4_text'|trans|raw }}</p>
                </div>
                <div class=\"col\">
                    <strong class=\"title\">{{ 'security_text_block_item_5_title'|trans }}</strong>
                    <p>{{ 'security_text_block_item_5_text'|trans|raw }}</p>
                </div>
            </div>
        </div>
        <div class=\"items\">
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_system_development.svg')}}\" alt=\"{{ 'security_item_6_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_6_title'|trans }}</strong>
                    <p>{{ 'security_item_6_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_supplier_relations.svg')}}\" alt=\"{{ 'security_item_7_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_7_title'|trans }}</strong>
                    <p>{{ 'security_item_7_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_business_continuity.svg')}}\" alt=\"{{ 'security_item_8_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_8_title'|trans }}</strong>
                    <p>{{ 'security_item_8_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_governance.svg')}}\" alt=\"{{ 'security_item_9_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_9_title'|trans }}</strong>
                    <p>{{ 'security_item_9_text'|trans|raw }}</p>
                </div>
            </div>
            <div class=\"item\">
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_what_happens.svg')}}\" alt=\"{{ 'security_item_10_title'|trans }}\"></div>
                <div class=\"text\">
                    <strong class=\"title\">{{ 'security_item_10_title'|trans }}</strong>
                    <p>{{ 'security_item_10_text'|trans|raw }}</p>
                </div>
            </div>
        </div>
    </section>
    <section class=\"block-note align-center bg-exballium\">
        <p>{{ 'secure_note_text'|trans|raw }}</p>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "security.html.twig", "/Users/user/dev/lokalize/lokalise/templates/security.html.twig");
    }
}
