<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* case_studies.html.twig */
class __TwigTemplate_b8e8be8563a27185dd823e9e5569e63bd792fb571b3c540a7ce731d0a1ee11bb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "case_studies.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "case_studies.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "case_studies.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section class=\"posts\">
        <article class=\"post large-post img-left\">
            <div class=\"text\">
                <header>
                    ";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <h2>";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
                <footer>
                    <a href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("case_study");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_btn_read_more_text"), "html", null, true);
        echo "</a>
                </footer>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
        <article class=\"post img-left\">
            <div class=\"text\">
                <header>
                    ";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <p>";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
        <article class=\"post img-left\">
            <div class=\"text\">
                <header>
                    ";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <p>";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03.jpg"), "html", null, true);
        echo "\" alt=\" ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
        <article class=\"post large-post\">
            <div class=\"text\">
                <header>
                    ";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <h2>";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 80
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
                <footer>
                    <a href=\"";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("case_study");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_btn_read_more_text"), "html", null, true);
        echo "</a>
                </footer>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
        <article class=\"post\">
            <div class=\"text\">
                <header>
                    ";
        // line 98
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <p>";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_02.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
        <article class=\"post\">
            <div class=\"text\">
                <header>
                    ";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "
                    <img src=\"/\" data-src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\">
                </header>
                <p>";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_text"), "html", null, true);
        echo "</p>
                <div class=\"author\">
                    <span class=\"title\">";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_title"), "html", null, true);
        echo "</span>
                    <p>";
        // line 124
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_author_text"), "html", null, true);
        echo "</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/post_03.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("case_studies_post_1_head"), "html", null, true);
        echo "\">
                </picture>
            </div>
        </article>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 139
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "case_studies.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  406 => 139,  402 => 138,  398 => 137,  387 => 131,  383 => 130,  379 => 129,  371 => 124,  367 => 123,  362 => 121,  357 => 119,  353 => 118,  341 => 111,  337 => 110,  333 => 109,  325 => 104,  321 => 103,  316 => 101,  311 => 99,  307 => 98,  295 => 91,  291 => 90,  287 => 89,  277 => 84,  271 => 81,  267 => 80,  262 => 78,  258 => 77,  253 => 75,  249 => 74,  237 => 67,  233 => 66,  229 => 65,  221 => 60,  217 => 59,  212 => 57,  207 => 55,  203 => 54,  191 => 47,  187 => 46,  183 => 45,  175 => 40,  171 => 39,  166 => 37,  161 => 35,  157 => 34,  145 => 27,  141 => 26,  137 => 25,  127 => 20,  121 => 17,  117 => 16,  112 => 14,  108 => 13,  103 => 11,  99 => 10,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'case_studies_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section class=\"posts\">
        <article class=\"post large-post img-left\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <h2>{{ 'case_studies_post_1_title'|trans }}</h2>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
                <footer>
                    <a href=\"{{ path('case_study') }}\" class=\"btn-primary btn-large\">{{ 'case_studies_btn_read_more_text'|trans }}</a>
                </footer>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_01@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_01.jpg')}}\" alt=\"{{ 'case_studies_post_1_title'|trans }}\">
                </picture>
            </div>
        </article>
        <article class=\"post img-left\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_02.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_02@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_02.jpg')}}\" alt=\"{{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
        <article class=\"post img-left\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_03.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_03@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_03.jpg')}}\" alt=\" {{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
        <article class=\"post large-post\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <h2>{{ 'case_studies_post_1_title'|trans }}</h2>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
                <footer>
                    <a href=\"{{ path('case_study') }}\" class=\"btn-primary btn-large\">{{ 'case_studies_btn_read_more_text'|trans }}</a>
                </footer>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_01@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_01.jpg')}}\" alt=\"{{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
        <article class=\"post\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_02.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_02@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_02.jpg')}}\" alt=\"{{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
        <article class=\"post\">
            <div class=\"text\">
                <header>
                    {{ 'case_studies_post_1_head'|trans }}
                    <img src=\"/\" data-src=\"{{ asset('build/icon_slack.svg') }}\" alt=\"Slack\">
                </header>
                <p>{{ 'case_studies_post_1_text'|trans }}</p>
                <div class=\"author\">
                    <span class=\"title\">{{ 'case_studies_post_1_author_title'|trans }}</span>
                    <p>{{ 'case_studies_post_1_author_text'|trans }}</p>
                </div>
            </div>
            <div class=\"visual\">
                 <picture>
                    <data-src srcset=\"{{asset('build/post_03.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/post_03@2x.jpg')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/post_03.jpg')}}\" alt=\"{{ 'case_studies_post_1_head'|trans }}\">
                </picture>
            </div>
        </article>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "case_studies.html.twig", "/Users/user/dev/lokalize/lokalise/templates/case_studies.html.twig");
    }
}
