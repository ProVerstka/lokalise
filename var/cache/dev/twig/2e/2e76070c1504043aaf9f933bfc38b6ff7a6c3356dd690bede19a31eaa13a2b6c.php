<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_9c55a5c4de814536b9d467339baedfe70604115180857d39222c094fa52ab247 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <section id=\"promo\" class=\"index\">
        <picture class=\"image\">
            <data-src srcset=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_01.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_01@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_01.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <div class=\"text\">
            <h1>";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_title"), "html", null, true);
        echo "</h1>
            <p>";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preferred_title"), "html", null, true);
        echo "</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolute\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stripe.svg"), "html", null, true);
        echo "\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/yelp.svg"), "html", null, true);
        echo "\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lime.svg"), "html", null, true);
        echo "\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lemonade.svg"), "html", null, true);
        echo "\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section id=\"presentation\">
        <div class=\"block\">
            <div class=\"text\">
                <h2>";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_1_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_1_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_start_button"), "html", null, true);
        echo "</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/video_1.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div> 
        <div class=\"block\">
            <div class=\"text\">
                <h2>";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_2_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_2_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_start_button"), "html", null, true);
        echo "</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/video_2.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>
        <div class=\"block\">
            <div class=\"text\">
                <h2>";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_3_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_3_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_start_button"), "html", null, true);
        echo "</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/video_3.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>
        <div class=\"block\">
            <div class=\"text\">
                <h2>";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_4_title"), "html", null, true);
        echo "</h2>
                <p>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_block_4_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("presentation_start_button"), "html", null, true);
        echo "</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/video_3.mp4"), "html", null, true);
        echo "\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>  
    </section>
    <section id=\"info\">
        <h2>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_title"), "html", null, true);
        echo "</h2>
        <ul class=\"info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_websites.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_1_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_mobile.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_2_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_games.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 96
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_3_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_docs.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_4_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_market.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_5_text"), "html", null, true);
        echo "</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_iot.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_title"), "html", null, true);
        echo "\"></div>
                <strong>";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_title"), "html", null, true);
        echo "</strong>
                <p>";
        // line 111
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("info_list_6_text"), "html", null, true);
        echo "</p>
            </li>
        </ul>
    </section>
    <section id=\"boost\">
        <h2 class=\"heading-h1\">";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_title"), "html", null, true);
        echo "</h2>
        <p class=\"subtitle\">";
        // line 117
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_subtitle"), "html", null, true);
        echo "</p>
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_developers.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_1_text"), "html", null, true);
        echo "</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_product.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_2_text"), "html", null, true);
        echo "</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translations.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_title"), "html", null, true);
        echo "\"></div>  
                <h4>";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_title"), "html", null, true);
        echo "</h4>
                <p>";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("boost_list_3_text"), "html", null, true);
        echo "</p> 
            </li>
        </ul>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 143
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 145
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"lovely\">
        <div class=\"title\">
            <h3>";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("lovely_title"), "html", null, true);
        echo "</h3>
        </div>
        <div class=\"raitings\">
            <div class=\"col\">
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/growd.svg"), "html", null, true);
        echo "\" alt=\"Growd\"></div>
                <div class=\"raiting\"><img src=\"/\" data-src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stars.svg"), "html", null, true);
        echo "\" alt=\"Stars\"></div>
                <div class=\"text\">";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("lovely_users"), "html", null, true);
        echo "</div>
            </div>
            <div class=\"col\">
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/capterra.svg"), "html", null, true);
        echo "\" alt=\"Capterra\"></div>
                <div class=\"raiting\"><img src=\"/\" data-src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stars.svg"), "html", null, true);
        echo "\" alt=\"Stars\"></div>
                <div class=\"text\">";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("lovely_rates"), "html", null, true);
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"integrate\">
        <h2 class=\"heading-h1\">";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_text"), "html", null, true);
        echo "</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01_mobile.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_01.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrate_title"), "html", null, true);
        echo "\">
            </picture>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 180
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 181
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  517 => 181,  513 => 180,  509 => 179,  499 => 174,  495 => 173,  491 => 172,  485 => 169,  481 => 168,  473 => 163,  469 => 162,  465 => 161,  459 => 158,  455 => 157,  451 => 156,  444 => 152,  435 => 146,  431 => 145,  426 => 143,  418 => 140,  414 => 139,  410 => 138,  401 => 132,  397 => 131,  391 => 130,  385 => 127,  381 => 126,  375 => 125,  369 => 122,  365 => 121,  359 => 120,  353 => 117,  349 => 116,  341 => 111,  337 => 110,  331 => 109,  325 => 106,  321 => 105,  315 => 104,  309 => 101,  305 => 100,  299 => 99,  293 => 96,  289 => 95,  283 => 94,  277 => 91,  273 => 90,  267 => 89,  261 => 86,  257 => 85,  251 => 84,  245 => 81,  236 => 75,  229 => 71,  225 => 70,  221 => 69,  212 => 63,  205 => 59,  201 => 58,  197 => 57,  188 => 51,  181 => 47,  177 => 46,  173 => 45,  164 => 39,  157 => 35,  153 => 34,  149 => 33,  140 => 27,  136 => 26,  132 => 25,  128 => 24,  124 => 23,  120 => 22,  115 => 20,  108 => 16,  103 => 14,  97 => 13,  92 => 11,  88 => 10,  80 => 7,  76 => 6,  72 => 5,  68 => 3,  58 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block body %}
    <section id=\"promo\" class=\"index\">
        <picture class=\"image\">
            <data-src srcset=\"{{asset('build/promo_01.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_01@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_01.png')}}\" alt=\"{{ 'index_promo_title'|trans }}\">
        </picture>
        <div class=\"text\">
            <h1>{{ 'index_promo_title'|trans }}</h1>
            <p>{{ 'index_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"{{ path('signup_page') }}\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>{{ 'preferred_title'|trans }}</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"{{ asset('build/slack.svg') }}\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"{{ asset('build/revolut.svg') }}\" alt=\"Revolute\"></li>
            <li><img src=\"/\" data-src=\"{{ asset('build/stripe.svg') }}\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"{{ asset('build/yelp.svg') }}\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"{{ asset('build/lime.svg') }}\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"{{ asset('build/lemonade.svg') }}\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section id=\"presentation\">
        <div class=\"block\">
            <div class=\"text\">
                <h2>{{ 'presentation_block_1_title'|trans }}</h2>
                <p>{{ 'presentation_block_1_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'presentation_start_button'|trans }}</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"{{asset('build/video_1.mp4')}}\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div> 
        <div class=\"block\">
            <div class=\"text\">
                <h2>{{ 'presentation_block_2_title'|trans }}</h2>
                <p>{{ 'presentation_block_2_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'presentation_start_button'|trans }}</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"{{asset('build/video_2.mp4')}}\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>
        <div class=\"block\">
            <div class=\"text\">
                <h2>{{ 'presentation_block_3_title'|trans }}</h2>
                <p>{{ 'presentation_block_3_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'presentation_start_button'|trans }}</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"{{asset('build/video_3.mp4')}}\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>
        <div class=\"block\">
            <div class=\"text\">
                <h2>{{ 'presentation_block_4_title'|trans }}</h2>
                <p>{{ 'presentation_block_4_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'presentation_start_button'|trans }}</a>
            </div>
            <div class=\"visual\">
                <video  autoplay muted loop playsinline>
                    <data-src src=\"{{asset('build/video_3.mp4')}}\" type=\"video/mp4\"></data-src>
                </video>
            </div>
        </div>  
    </section>
    <section id=\"info\">
        <h2>{{ 'info_title'|trans }}</h2>
        <ul class=\"info-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_websites.svg')}}\" alt=\"{{ 'info_list_1_title'|trans }}\"></div>
                <strong>{{ 'info_list_1_title'|trans }}</strong>
                <p>{{ 'info_list_1_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_mobile.svg')}}\" alt=\"{{ 'info_list_2_title'|trans }}\"></div>
                <strong>{{ 'info_list_2_title'|trans }}</strong>
                <p>{{ 'info_list_2_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_games.svg')}}\" alt=\"{{ 'info_list_3_title'|trans }}\"></div>
                <strong>{{ 'info_list_3_title'|trans }}</strong>
                <p>{{ 'info_list_3_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_docs.svg')}}\" alt=\"{{ 'info_list_4_title'|trans }}\"></div>
                <strong>{{ 'info_list_4_title'|trans }}</strong>
                <p>{{ 'info_list_4_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_market.svg')}}\" alt=\"{{ 'info_list_5_title'|trans }}\"></div>
                <strong>{{ 'info_list_5_title'|trans }}</strong>
                <p>{{ 'info_list_5_text'|trans }}</p>
            </li>
            <li>
               <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_iot.svg')}}\" alt=\"{{ 'info_list_6_title'|trans }}\"></div>
                <strong>{{ 'info_list_6_title'|trans }}</strong>
                <p>{{ 'info_list_6_text'|trans }}</p>
            </li>
        </ul>
    </section>
    <section id=\"boost\">
        <h2 class=\"heading-h1\">{{ 'boost_title'|trans }}</h2>
        <p class=\"subtitle\">{{ 'boost_subtitle'|trans }}</p>
        <ul class=\"boost-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_developers.svg')}}\" alt=\"{{ 'boost_list_1_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_1_title'|trans }}</h4>
                <p>{{ 'boost_list_1_text'|trans }}</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_product.svg')}}\" alt=\"{{ 'boost_list_2_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_2_title'|trans }}</h4>
                <p>{{ 'boost_list_2_text'|trans }}</p> 
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_translations.svg')}}\" alt=\"{{ 'boost_list_3_title'|trans }}\"></div>  
                <h4>{{ 'boost_list_3_title'|trans }}</h4>
                <p>{{ 'boost_list_3_text'|trans }}</p> 
            </li>
        </ul>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"lovely\">
        <div class=\"title\">
            <h3>{{ 'lovely_title'|trans }}</h3>
        </div>
        <div class=\"raitings\">
            <div class=\"col\">
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/growd.svg')}}\" alt=\"Growd\"></div>
                <div class=\"raiting\"><img src=\"/\" data-src=\"{{asset('build/stars.svg')}}\" alt=\"Stars\"></div>
                <div class=\"text\">{{ 'lovely_users'|trans }}</div>
            </div>
            <div class=\"col\">
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/capterra.svg')}}\" alt=\"Capterra\"></div>
                <div class=\"raiting\"><img src=\"/\" data-src=\"{{asset('build/stars.svg')}}\" alt=\"Stars\"></div>
                <div class=\"text\">{{ 'lovely_rates'|trans }}</div>
            </div>
        </div>
    </section>
    <section class=\"integrate\">
        <h2 class=\"heading-h1\">{{ 'integrate_title'|trans }}</h2>
        <p>{{ 'integrate_text'|trans }}</p>
        <div class=\"visual\">
            <picture>
                <data-src srcset=\"{{asset('build/img_01_mobile.png')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_01@2x.png')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_01.png')}}\" alt=\"{{ 'integrate_title'|trans }}\">
            </picture>
        </div>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "index.html.twig", "/Users/user/dev/lokalize/lokalise/templates/index.html.twig");
    }
}
