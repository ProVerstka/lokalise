<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product_for_translators.html.twig */
class __TwigTemplate_7c565f3fd1dc94799cf8301b07884a70ff6467bca0123d4957d9af5c8f14445b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_translators.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "product_for_translators.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "product_for_translators.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_promo_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_promo_title");
        echo "</h1>
        <picture class=\"image\">
            <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_04.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_04@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/promo_04.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_promo_title"), "html", null, true);
        echo "\">
        </picture>
        <div class=\"text\">
            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-primary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("preferred_title"), "html", null, true);
        echo "</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/slack.svg"), "html", null, true);
        echo "\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/stripe.svg"), "html", null, true);
        echo "\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/yelp.svg"), "html", null, true);
        echo "\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lime.svg"), "html", null, true);
        echo "\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/lemonade.svg"), "html", null, true);
        echo "\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h1>";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title");
        echo "</h1>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_mobile_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_web_02.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_machine.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_text_3"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translate.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_4"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_title_4"), "html", null, true);
        echo "</strong>
                <p>";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_features_text_4"), "html", null, true);
        echo "</p>
            </li>
        </ul>
        <a href=\"/features\" class=\"btn-primary btn-large\">";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("features_btn_text"), "html", null, true);
        echo "</a>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01@2x.jpg"), "html", null, true);
        echo " 2x\"></data-src>
            <data-img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/photo_01.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "\">
        </picture>
        <blockquote>
            <p>";
        // line 66
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_text");
        echo "</p>
            <footer>
                <cite>";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("review_author"), "html", null, true);
        echo "</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_revolut.svg"), "html", null, true);
        echo "\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"report\">
        <h2>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_title"), "html", null, true);
        echo "</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_1"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_2"), "html", null, true);
        echo "</p>
            </li>
            <li>
                <strong class=\"title\">";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("report_list_text_3"), "html", null, true);
        echo "</p>
            </li>
        </ul> 
    </section>
    <section class=\"speed-up no-background\">
        <h2>";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_translators_speed_up_title"), "html", null, true);
        echo "</h2>
        <ul class=\"speed-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_join.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_1"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_recieve.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_2"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 100
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_start.svg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_3"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 106
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("speed_up_list_text_3");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"on-board\">
        <h1>";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_title");
        echo "</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_02.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_1_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                    <data-img src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_03.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_title"), "html", null, true);
        echo "\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("product_for_managers_on_board_item_2_text"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"more\">";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("on_board_more_link"), "html", null, true);
        echo "</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 144
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product_for_translators.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  458 => 144,  454 => 143,  450 => 142,  442 => 137,  438 => 136,  434 => 135,  425 => 131,  421 => 130,  417 => 129,  408 => 123,  404 => 122,  400 => 121,  391 => 117,  387 => 116,  383 => 115,  376 => 111,  368 => 106,  364 => 105,  358 => 104,  352 => 101,  348 => 100,  342 => 99,  336 => 96,  332 => 95,  326 => 94,  320 => 91,  312 => 86,  308 => 85,  302 => 82,  298 => 81,  292 => 78,  288 => 77,  282 => 74,  274 => 69,  270 => 68,  265 => 66,  257 => 63,  253 => 62,  249 => 61,  242 => 57,  236 => 54,  232 => 53,  226 => 52,  220 => 49,  216 => 48,  210 => 47,  204 => 44,  200 => 43,  194 => 42,  188 => 39,  184 => 38,  178 => 37,  172 => 34,  165 => 30,  161 => 29,  157 => 28,  153 => 27,  149 => 26,  145 => 25,  140 => 23,  133 => 19,  128 => 17,  122 => 16,  117 => 14,  109 => 11,  105 => 10,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'product_for_translators_promo_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'product_for_translators_promo_title'|trans|raw }}</h1>
        <picture class=\"image\">
            <data-src srcset=\"{{asset('build/promo_04.png')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/promo_04@2x.png')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/promo_04.png')}}\" alt=\"{{ 'product_for_translators_promo_title'|trans }}\">
        </picture>
        <div class=\"text\">
            <p>{{ 'product_for_translators_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"{{ path('signup_page') }}\" class=\"btn-primary btn-large\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn btn-large\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section id=\"prefered\">
        <h2>{{ 'preferred_title'|trans }}</h2>
        <ul class=\"icons\"`>
            <li><img src=\"/\" data-src=\"{{asset('build/slack.svg')}}\" alt=\"Slack\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/revolut.svg')}}\" alt=\"Revolut\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/stripe.svg')}}\" alt=\"Stripe\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/yelp.svg')}}\" alt=\"Yelp\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lime.svg')}}\" alt=\"Lime\"></li>
            <li><img src=\"/\" data-src=\"{{asset('build/lemonade.svg')}}\" alt=\"Lemonade\"></li>
        </ul> 
    </section>
    <section class=\"features\">
        <h1>{{ 'product_for_translators_features_title'|trans|raw }}</h1>
        <ul class=\"features-list\"`>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_mobile_02.svg')}}\" alt=\"{{ 'product_for_translators_features_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_translators_features_title_1'|trans }}</strong>
                <p>{{ 'product_for_translators_features_text_1'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_web_02.svg')}}\" alt=\"{{ 'product_for_translators_features_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_translators_features_title_2'|trans }}</strong>
                <p>{{ 'product_for_translators_features_text_2'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_machine.svg')}}\" alt=\"{{ 'product_for_translators_features_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_translators_features_title_3'|trans }}</strong>
                <p>{{ 'product_for_translators_features_text_3'|trans }}</p>
            </li>
            <li>
                <div class=\"icon\"><img src=\"/\" data-src=\"{{asset('build/icon_translate.svg')}}\" alt=\"{{ 'product_for_translators_features_title_4'|trans }}\"></div>
                <strong class=\"title\">{{ 'product_for_translators_features_title_4'|trans }}</strong>
                <p>{{ 'product_for_translators_features_text_4'|trans }}</p>
            </li>
        </ul>
        <a href=\"/features\" class=\"btn-primary btn-large\">{{ 'features_btn_text'|trans }}</a>
    </section>
    <section class=\"client-review\">
        <picture class=\"photo\">
            <data-src srcset=\"{{asset('build/photo_01.jpg')}}\" media=\"(max-width: 768px)\"></data-src>
            <data-src srcset=\"{{asset('build/photo_01@2x.jpg')}} 2x\"></data-src>
            <data-img src=\"{{asset('build/photo_01.jpg')}}\" alt=\"{{ 'review_author'|trans }}\">
        </picture>
        <blockquote>
            <p>{{ 'review_text'|trans|raw }}</p>
            <footer>
                <cite>{{ 'review_author'|trans }}</cite>
                <div class=\"logotype\"><img src=\"/\" data-src=\"{{asset('build/icon_revolut.svg')}}\" alt=\"Revolut\"></div>
            </footer>
        </blockquote>
    </section>
    <section class=\"report\">
        <h2>{{ 'report_title'|trans }}</h2>
        <ul class=\"report-list\">
            <li>
                <strong class=\"title\">{{ 'report_list_title_1'|trans }}</strong>
                <p>{{ 'report_list_text_1'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_2'|trans }}</strong>
                <p>{{ 'report_list_text_2'|trans }}</p>
            </li>
            <li>
                <strong class=\"title\">{{ 'report_list_title_3'|trans }}</strong>
                <p>{{ 'report_list_text_3'|trans }}</p>
            </li>
        </ul> 
    </section>
    <section class=\"speed-up no-background\">
        <h2>{{ 'product_for_translators_speed_up_title'|trans }}</h2>
        <ul class=\"speed-list\">
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_join.svg')}}\" alt=\"{{ 'speed_up_list_title_1'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_1'|trans }}</strong>
                <p>{{ 'speed_up_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_recieve.svg')}}\" alt=\"{{ 'speed_up_list_title_2'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_2'|trans }}</strong>
                <p>{{ 'speed_up_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"/\" data-src=\"{{asset('build/icon_start.svg')}}\" alt=\"{{ 'speed_up_list_title_3'|trans }}\"></div>
                <strong class=\"title\">{{ 'speed_up_list_title_3'|trans }}</strong>
                <p>{{ 'speed_up_list_text_3'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"on-board\">
        <h1>{{ 'on_board_title'|trans|raw }}</h1>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_02.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_02@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_02.png')}}\" alt=\"{{ 'product_for_managers_on_board_item_1_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'product_for_managers_on_board_item_1_title'|trans }}</h3>
                <p>{{ 'product_for_managers_on_board_item_1_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"visual\">
                <picture>
                    <data-src srcset=\"{{asset('build/img_03.png')}}\" media=\"(max-width: 768px)\"></data-src>
                    <data-src srcset=\"{{asset('build/img_03@2x.png')}} 2x\"></data-src>
                    <data-img src=\"{{asset('build/img_03.png')}}\" alt=\"{{ 'product_for_managers_on_board_item_2_title'|trans }}\">
                </picture>
            </div>
            <div class=\"text\">
                <h3>{{ 'product_for_managers_on_board_item_2_title'|trans }}</h3>
                <p>{{ 'product_for_managers_on_board_item_2_text'|trans }}</p>
                <a href=\"#\" class=\"more\">{{ 'on_board_more_link'|trans }}</a>
            </div>
        </div> 
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "product_for_translators.html.twig", "/Users/user/dev/lokalize/lokalise/templates/product_for_translators.html.twig");
    }
}
