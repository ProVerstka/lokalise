<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_da09a3ec5d685d7c9297f2ec80d26caaca7daeb933cf8797adee49b064df9994 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-US\">
    <head>
        <meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-57x57.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-60x60.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-72x72.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-76x76.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-114x114.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-120x120.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-144x144.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-152x152.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apple-icon-180x180.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/android-icon-192x192.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/favicon-32x32.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/favicon-96x96.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/favicon-16x16.png"), "html", null, true);
        echo "\">
        <link rel=\"manifest\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("manifest.json"), "html", null, true);
        echo "\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"msapplication-TileImage\" content=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/ms-icon-144x144.png"), "html", null, true);
        echo "\">
        <meta name=\"theme-color\" content=\"#ffffff\">
        <title>";
        // line 24
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 25
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 28
        echo "    </head>
    <body>
        <div id=\"wrapper\">  
            <header id=\"header\">
                <div class=\"container\">
                    <a href=\"/\" class=\"logo\">";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo "</a>
                    <nav id=\"navigation\">
                        <ul class=\"menu\">
                            <li>
                                <span class=\"has-drop\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_product"), "html", null, true);
        echo "</span>
                                <ul>
                                    <li ";
        // line 39
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "request", [], "any", false, false, false, 39), "get", [0 => "_route"], "method", false, false, false, 39) == "product_for_developers_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_developers_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_product_link_for_developers"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 40
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 40, $this->source); })()), "request", [], "any", false, false, false, 40), "get", [0 => "_route"], "method", false, false, false, 40) == "product_for_managers_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_managers_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_product_link_for_managers"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 41
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 41, $this->source); })()), "request", [], "any", false, false, false, 41), "get", [0 => "_route"], "method", false, false, false, 41) == "product_for_translators_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_translators_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_product_link_for_translators"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 42
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "request", [], "any", false, false, false, 42), "get", [0 => "_route"], "method", false, false, false, 42) == "integrations")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("integrations");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_product_link_integrations"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 43
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 43, $this->source); })()), "request", [], "any", false, false, false, 43), "get", [0 => "_route"], "method", false, false, false, 43) == "security_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_product_link_security"), "html", null, true);
        echo "</a></li>
                                </ul>
                            </li>
                            <li>
                                <span class=\"has-drop\">";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_why"), "html", null, true);
        echo "</span>
                                <ul>
                                    <li ";
        // line 49
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 49, $this->source); })()), "request", [], "any", false, false, false, 49), "get", [0 => "_route"], "method", false, false, false, 49) == "collaborative_translation")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("collaborative_translation");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_collaborative_translation"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 50
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 50, $this->source); })()), "request", [], "any", false, false, false, 50), "get", [0 => "_route"], "method", false, false, false, 50) == "localization_workflow_management")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("localization_workflow_management");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_localization_workflow_management"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 51
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 51, $this->source); })()), "request", [], "any", false, false, false, 51), "get", [0 => "_route"], "method", false, false, false, 51) == "process_automation")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("process_automation");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_process_automation"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 52
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 52, $this->source); })()), "request", [], "any", false, false, false, 52), "get", [0 => "_route"], "method", false, false, false, 52) == "quality_assurance")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quality_assurance");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_quality_assurance"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 53
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 53, $this->source); })()), "request", [], "any", false, false, false, 53), "get", [0 => "_route"], "method", false, false, false, 53) == "in_context_editing")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("in_context_editing");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_in_context_editing"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 54
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 54, $this->source); })()), "request", [], "any", false, false, false, 54), "get", [0 => "_route"], "method", false, false, false, 54) == "deliver_to_end_users")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("deliver_to_end_users");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_deliver_to_end_users"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 55
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 55, $this->source); })()), "request", [], "any", false, false, false, 55), "get", [0 => "_route"], "method", false, false, false, 55) == "support_ticket_and_chat_translation")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("support_ticket_and_chat_translation");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_support_ticket_and_chat_translation"), "html", null, true);
        echo "</a></li>
                                    <li ";
        // line 56
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 56, $this->source); })()), "request", [], "any", false, false, false, 56), "get", [0 => "_route"], "method", false, false, false, 56) == "professional_translation_services")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("professional_translation_services");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_why_link_professional_translation_services"), "html", null, true);
        echo "</a></li>
                                </ul>
                            </li>
                            <li ";
        // line 59
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 59, $this->source); })()), "request", [], "any", false, false, false, 59), "get", [0 => "_route"], "method", false, false, false, 59) == "pricing")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pricing");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_pricing"), "html", null, true);
        echo "</a></li>
                            <li ";
        // line 60
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 60, $this->source); })()), "request", [], "any", false, false, false, 60), "get", [0 => "_route"], "method", false, false, false, 60) == "case_studies")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("case_studies");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_case"), "html", null, true);
        echo "</a></li>
                            <li>
                                <span class=\"has-drop\">";
        // line 62
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_docs"), "html", null, true);
        echo "</span>
                                <ul>
                                    <li><a href=\"#\">";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_documentaiton"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_tool"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_api"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_sdk"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_blog"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_tutorials"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"#\">";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sublink_docs_link_product_updates"), "html", null, true);
        echo "</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class=\"user-menu\">
                            <li><a href=\"";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_page");
        echo "\" class=\"btn\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_login"), "html", null, true);
        echo "</a></li>
                            <li><a href=\"";
        // line 76
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-primary\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("link_try"), "html", null, true);
        echo "</a></li>
                        </ul>
                    </nav>
                    <a href=\"#\" class=\"mb-burger\"><span>menu</span></a>
                </div> 
            </header>
            <main id=\"main\">
                <div class=\"container\">
                    ";
        // line 84
        $this->displayBlock('body', $context, $blocks);
        // line 85
        echo "                </div>
            </main>
            <footer id=\"footer\">
                <div class=\"container\">
                    <div class=\"columns\">
                        <div class=\"col\">
                            <strong class=\"title\">";
        // line 91
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_title"), "html", null, true);
        echo "</strong>
                            <div class=\"frame\">
                                <img src=\"/\" data-src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/frame.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_title"), "html", null, true);
        echo "\">
                            </div>
                            <p class=\"case\">";
        // line 95
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_case"), "html", null, true);
        echo "</p>
                        </div>
                        <div class=\"col\">
                            <nav class=\"footer-nav\">
                                <ul>
                                    <li>
                                        ";
        // line 101
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_link_product"), "html", null, true);
        echo "
                                        <ul>
                                            <li ";
        // line 103
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 103, $this->source); })()), "request", [], "any", false, false, false, 103), "get", [0 => "_route"], "method", false, false, false, 103) == "features_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("features_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_features"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 104
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 104, $this->source); })()), "request", [], "any", false, false, false, 104), "get", [0 => "_route"], "method", false, false, false, 104) == "integrations")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("integrations");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_integrations"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 105
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 105, $this->source); })()), "request", [], "any", false, false, false, 105), "get", [0 => "_route"], "method", false, false, false, 105) == "security_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_security"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 106
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 106, $this->source); })()), "request", [], "any", false, false, false, 106), "get", [0 => "_route"], "method", false, false, false, 106) == "product_for_developers_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_developers_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_for_developers"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 107
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 107, $this->source); })()), "request", [], "any", false, false, false, 107), "get", [0 => "_route"], "method", false, false, false, 107) == "product_for_managers_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_managers_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_for_managers"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 108
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 108, $this->source); })()), "request", [], "any", false, false, false, 108), "get", [0 => "_route"], "method", false, false, false, 108) == "product_for_translators_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_for_translators_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_product_for_translators"), "html", null, true);
        echo "</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        ";
        // line 112
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_link_support"), "html", null, true);
        echo "
                                        <ul>
                                            <li><a href=\"#\">";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_contact"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_documentation"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_status"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 117
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_feature_requests"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_product_updates"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_cli"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 120
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_api"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_sdk"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_support_supported_file_formats"), "html", null, true);
        echo "</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        ";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_link_company"), "html", null, true);
        echo "
                                        <ul>
                                            <li><a href=\"";
        // line 128
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("team_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_company_about"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 129
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_company_blog"), "html", null, true);
        echo "</a></li>
                                            <li ";
        // line 130
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 130, $this->source); })()), "request", [], "any", false, false, false, 130), "get", [0 => "_route"], "method", false, false, false, 130) == "careers_page")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("careers_page");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_company_careers"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_company_case_studies"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_company_media_kit"), "html", null, true);
        echo "</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        ";
        // line 136
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_link_legal"), "html", null, true);
        echo "
                                        <ul>
                                            <li ";
        // line 138
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 138, $this->source); })()), "request", [], "any", false, false, false, 138), "get", [0 => "_route"], "method", false, false, false, 138) == "legal")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("legal");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_terms_of_service"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 139
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_privacy_policy"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_security_policy"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_cookies_policy"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_dpa"), "html", null, true);
        echo "</a></li>
                                            <li><a href=\"#\">";
        // line 143
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_sublink_legal_gdpr"), "html", null, true);
        echo "</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"holder\">
                        <div class=\"logo-holder\">
                            <strong class=\"logo\"><a href=\"/\">";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo "</a></strong>
                            <em class=\"slogan\">";
        // line 153
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_slogan");
        echo "</em>
                        </div>
                        <span class=\"copyright\">";
        // line 155
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("footer_copyright");
        echo "</span>
                    </div>
                </div>
            </footer>
        </div>
        <div class=\"cookie-popup\">
            <a href=\"#\" class=\"close\"></a>
            <p>";
        // line 162
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("cookie_popup_text");
        echo "</p>
        </div>
       
        
       <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/build/jquery-1.10.2.min.js"), "html", null, true);
        echo "\"></script>
       <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/build/swiper.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 168
        $this->displayBlock('javascripts', $context, $blocks);
        // line 171
        echo "    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 26
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 84
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 168
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 169
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  699 => 169,  689 => 168,  671 => 84,  658 => 26,  648 => 25,  629 => 24,  617 => 171,  615 => 168,  611 => 167,  607 => 166,  600 => 162,  590 => 155,  585 => 153,  581 => 152,  569 => 143,  565 => 142,  561 => 141,  557 => 140,  553 => 139,  543 => 138,  538 => 136,  531 => 132,  527 => 131,  517 => 130,  513 => 129,  507 => 128,  502 => 126,  495 => 122,  491 => 121,  487 => 120,  483 => 119,  479 => 118,  475 => 117,  471 => 116,  467 => 115,  463 => 114,  458 => 112,  445 => 108,  435 => 107,  425 => 106,  415 => 105,  405 => 104,  395 => 103,  390 => 101,  381 => 95,  374 => 93,  369 => 91,  361 => 85,  359 => 84,  346 => 76,  340 => 75,  332 => 70,  328 => 69,  324 => 68,  320 => 67,  316 => 66,  312 => 65,  308 => 64,  303 => 62,  292 => 60,  282 => 59,  270 => 56,  260 => 55,  250 => 54,  240 => 53,  230 => 52,  220 => 51,  210 => 50,  200 => 49,  195 => 47,  182 => 43,  172 => 42,  162 => 41,  152 => 40,  142 => 39,  137 => 37,  130 => 33,  123 => 28,  121 => 25,  117 => 24,  112 => 22,  107 => 20,  103 => 19,  99 => 18,  95 => 17,  91 => 16,  87 => 15,  83 => 14,  79 => 13,  75 => 12,  71 => 11,  67 => 10,  63 => 9,  59 => 8,  55 => 7,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en-US\">
    <head>
        <meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\" />
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"{{ asset('build/apple-icon-57x57.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"{{ asset('build/apple-icon-60x60.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"{{ asset('build/apple-icon-72x72.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{ asset('build/apple-icon-76x76.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"{{ asset('build/apple-icon-114x114.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"{{ asset('build/apple-icon-120x120.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"{{ asset('build/apple-icon-144x144.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"{{ asset('build/apple-icon-152x152.png') }}\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"{{ asset('build/apple-icon-180x180.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"{{ asset('build/android-icon-192x192.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"{{ asset('build/favicon-32x32.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"{{ asset('build/favicon-96x96.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{ asset('build/favicon-16x16.png') }}\">
        <link rel=\"manifest\" href=\"{{ asset('manifest.json') }}\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"msapplication-TileImage\" content=\"{{ asset('build/ms-icon-144x144.png') }}\">
        <meta name=\"theme-color\" content=\"#ffffff\">
        <title>{% block title %}{{ 'header_logotype'|trans }}{% endblock %}</title>
        {% block stylesheets %}
            {{ encore_entry_link_tags('app') }}
        {% endblock %}
    </head>
    <body>
        <div id=\"wrapper\">  
            <header id=\"header\">
                <div class=\"container\">
                    <a href=\"/\" class=\"logo\">{{ 'header_logotype'|trans }}</a>
                    <nav id=\"navigation\">
                        <ul class=\"menu\">
                            <li>
                                <span class=\"has-drop\">{{ 'link_product'|trans }}</span>
                                <ul>
                                    <li {% if app.request.get('_route') == 'product_for_developers_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_developers_page') }}\">{{ 'sublink_product_link_for_developers'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'product_for_managers_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_managers_page') }}\">{{ 'sublink_product_link_for_managers'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'product_for_translators_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_translators_page') }}\">{{ 'sublink_product_link_for_translators'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'integrations' %} class=\"active\"{% endif %}><a href=\"{{ path('integrations') }}\">{{ 'sublink_product_link_integrations'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'security_page' %} class=\"active\"{% endif %}><a href=\"{{ path('security_page') }}\">{{ 'sublink_product_link_security'|trans }}</a></li>
                                </ul>
                            </li>
                            <li>
                                <span class=\"has-drop\">{{ 'link_why'|trans }}</span>
                                <ul>
                                    <li {% if app.request.get('_route') == 'collaborative_translation' %} class=\"active\"{% endif %}><a href=\"{{ path('collaborative_translation') }}\">{{ 'sublink_why_link_collaborative_translation'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'localization_workflow_management' %} class=\"active\"{% endif %}><a href=\"{{ path('localization_workflow_management') }}\">{{ 'sublink_why_link_localization_workflow_management'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'process_automation' %} class=\"active\"{% endif %}><a href=\"{{ path('process_automation') }}\">{{ 'sublink_why_link_process_automation'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'quality_assurance' %} class=\"active\"{% endif %}><a href=\"{{ path('quality_assurance') }}\">{{ 'sublink_why_link_quality_assurance'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'in_context_editing' %} class=\"active\"{% endif %}><a href=\"{{ path('in_context_editing') }}\">{{ 'sublink_why_link_in_context_editing'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'deliver_to_end_users' %} class=\"active\"{% endif %}><a href=\"{{ path('deliver_to_end_users') }}\">{{ 'sublink_why_link_deliver_to_end_users'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'support_ticket_and_chat_translation' %} class=\"active\"{% endif %}><a href=\"{{ path('support_ticket_and_chat_translation') }}\">{{ 'sublink_why_link_support_ticket_and_chat_translation'|trans }}</a></li>
                                    <li {% if app.request.get('_route') == 'professional_translation_services' %} class=\"active\"{% endif %}><a href=\"{{ path('professional_translation_services') }}\">{{ 'sublink_why_link_professional_translation_services'|trans }}</a></li>
                                </ul>
                            </li>
                            <li {% if app.request.get('_route') == 'pricing' %} class=\"active\"{% endif %}><a href=\"{{ path('pricing') }}\">{{ 'link_pricing'|trans }}</a></li>
                            <li {% if app.request.get('_route') == 'case_studies' %} class=\"active\"{% endif %}><a href=\"{{ path('case_studies') }}\">{{ 'link_case'|trans }}</a></li>
                            <li>
                                <span class=\"has-drop\">{{ 'link_docs'|trans }}</span>
                                <ul>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_documentaiton'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_tool'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_api'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_sdk'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_blog'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_tutorials'|trans }}</a></li>
                                    <li><a href=\"#\">{{ 'sublink_docs_link_product_updates'|trans }}</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class=\"user-menu\">
                            <li><a href=\"{{ path('login_page') }}\" class=\"btn\">{{ 'link_login'|trans }}</a></li>
                            <li><a href=\"{{ path('signup_page') }}\" class=\"btn-primary\">{{ 'link_try'|trans }}</a></li>
                        </ul>
                    </nav>
                    <a href=\"#\" class=\"mb-burger\"><span>menu</span></a>
                </div> 
            </header>
            <main id=\"main\">
                <div class=\"container\">
                    {% block body %}{% endblock %}
                </div>
            </main>
            <footer id=\"footer\">
                <div class=\"container\">
                    <div class=\"columns\">
                        <div class=\"col\">
                            <strong class=\"title\">{{ 'footer_title'|trans }}</strong>
                            <div class=\"frame\">
                                <img src=\"/\" data-src=\"{{asset('build/frame.png')}}\" alt=\"{{ 'footer_title'|trans }}\">
                            </div>
                            <p class=\"case\">{{ 'footer_case'|trans }}</p>
                        </div>
                        <div class=\"col\">
                            <nav class=\"footer-nav\">
                                <ul>
                                    <li>
                                        {{ 'footer_link_product'|trans }}
                                        <ul>
                                            <li {% if app.request.get('_route') == 'features_page' %} class=\"active\"{% endif %}><a href=\"{{ path('features_page') }}\">{{ 'footer_sublink_product_features'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'integrations' %} class=\"active\"{% endif %}><a href=\"{{ path('integrations') }}\">{{ 'footer_sublink_product_integrations'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'security_page' %} class=\"active\"{% endif %}><a href=\"{{ path('security_page') }}\">{{ 'footer_sublink_product_security'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'product_for_developers_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_developers_page') }}\">{{ 'footer_sublink_product_for_developers'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'product_for_managers_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_managers_page') }}\">{{ 'footer_sublink_product_for_managers'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'product_for_translators_page' %} class=\"active\"{% endif %}><a href=\"{{ path('product_for_translators_page') }}\">{{ 'footer_sublink_product_for_translators'|trans }}</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        {{ 'footer_link_support'|trans }}
                                        <ul>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_contact'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_documentation'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_status'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_feature_requests'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_product_updates'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_cli'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_api'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_sdk'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_support_supported_file_formats'|trans }}</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        {{ 'footer_link_company'|trans }}
                                        <ul>
                                            <li><a href=\"{{ path('team_page') }}\">{{ 'footer_sublink_company_about'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_company_blog'|trans }}</a></li>
                                            <li {% if app.request.get('_route') == 'careers_page' %} class=\"active\"{% endif %}><a href=\"{{ path('careers_page') }}\">{{ 'footer_sublink_company_careers'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_company_case_studies'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_company_media_kit'|trans }}</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        {{ 'footer_link_legal'|trans }}
                                        <ul>
                                            <li {% if app.request.get('_route') == 'legal' %} class=\"active\"{% endif %}><a href=\"{{ path('legal') }}\">{{ 'footer_sublink_legal_terms_of_service'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_legal_privacy_policy'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_legal_security_policy'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_legal_cookies_policy'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_legal_dpa'|trans }}</a></li>
                                            <li><a href=\"#\">{{ 'footer_sublink_legal_gdpr'|trans }}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"holder\">
                        <div class=\"logo-holder\">
                            <strong class=\"logo\"><a href=\"/\">{{ 'header_logotype'|trans }}</a></strong>
                            <em class=\"slogan\">{{ 'footer_slogan'|trans|raw }}</em>
                        </div>
                        <span class=\"copyright\">{{ 'footer_copyright'|trans|raw }}</span>
                    </div>
                </div>
            </footer>
        </div>
        <div class=\"cookie-popup\">
            <a href=\"#\" class=\"close\"></a>
            <p>{{ 'cookie_popup_text'|trans|raw }}</p>
        </div>
       
        
       <script src=\"{{ asset('/build/jquery-1.10.2.min.js') }}\"></script>
       <script src=\"{{ asset('/build/swiper.min.js') }}\"></script>
        {% block javascripts %}
            {{ encore_entry_script_tags('app') }}
        {% endblock %}
    </body>
</html>
", "base.html.twig", "/Users/user/dev/lokalize/lokalise/templates/base.html.twig");
    }
}
