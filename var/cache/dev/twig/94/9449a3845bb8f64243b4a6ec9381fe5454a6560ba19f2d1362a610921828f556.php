<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sign_up.html.twig */
class __TwigTemplate_0ad1595289705ed996bfffd8b3c9903d9c7e34ec59eaa9065a89bf1a12601c66 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sign_up.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "sign_up.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "sign_up.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section class=\"form-block\">
        <div class=\"visual\">
            <picture class=\"\">
                <data-src srcset=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_signup.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_signup@2x.png"), "html", null, true);
        echo " 2x\"></data-src>
                <data-img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_signup.png"), "html", null, true);
        echo "\" alt=\"Log in\">
            </picture>
        </div>
        <div class=\"form\">
            <h1>";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_title"), "html", null, true);
        echo "</h1>
            <form action=\"#\" class=\"login-form\">
                <fieldset>
                    <div class=\"form-row\">
                        <button class=\"google\">";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_button_google"), "html", null, true);
        echo "</button>
                        <button class=\"github\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_button_github"), "html", null, true);
        echo "</button>
                        <button class=\"microsoft\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_button_microsoft"), "html", null, true);
        echo "</button>
                    </div>
                    <div class=\"note\"> 
                        <p>";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_note_text"), "html", null, true);
        echo "</p>
                    </div>
                    <div class=\"form-row\">
                        <label>";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_lable_1_text"), "html", null, true);
        echo "</label>
                        <input type=\"text\" placeholder=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_input_1_placeholder"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-row\">
                        <label>";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_lable_2_text"), "html", null, true);
        echo "</label>
                        <input type=\"email\" placeholder=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_input_2_placeholder"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-row\">
                        <label>";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_lable_3_text"), "html", null, true);
        echo "</label>
                        <input type=\"password\" placeholder=\"password\">
                    </div>
                    <div class=\"form-row privacy\">
                        <p>";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("signup_privacy_text");
        echo "</p>
                    </div>
                    <div class=\"form-row\">
                        <input type=\"submit\" class=\"btn-primary\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_button_text"), "html", null, true);
        echo "\">
                    </div>
                    <div class=\"form-row align-center singup-with\">
                        <a href=\"#\">";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("singup_sso_text"), "html", null, true);
        echo "</a>
                    </div>
                    <div class=\"form-row\">
                        <ul class=\"form-links align-center\">
                            <li>Already have an account? <a href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_page");
        echo "\">Log in</a>.</li>
                        </ul>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "sign_up.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 49,  179 => 45,  173 => 42,  167 => 39,  160 => 35,  154 => 32,  150 => 31,  144 => 28,  140 => 27,  134 => 24,  128 => 21,  124 => 20,  120 => 19,  113 => 15,  106 => 11,  102 => 10,  98 => 9,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'singup_title'|trans }}
{% endblock %}
{% block body %}
    <section class=\"form-block\">
        <div class=\"visual\">
            <picture class=\"\">
                <data-src srcset=\"{{asset('build/img_signup.png')}}\" media=\"(max-width: 768px)\"></data-src>
                <data-src srcset=\"{{asset('build/img_signup@2x.png')}} 2x\"></data-src>
                <data-img src=\"{{asset('build/img_signup.png')}}\" alt=\"Log in\">
            </picture>
        </div>
        <div class=\"form\">
            <h1>{{ 'singup_title'|trans }}</h1>
            <form action=\"#\" class=\"login-form\">
                <fieldset>
                    <div class=\"form-row\">
                        <button class=\"google\">{{ 'singup_button_google'|trans }}</button>
                        <button class=\"github\">{{ 'singup_button_github'|trans }}</button>
                        <button class=\"microsoft\">{{ 'singup_button_microsoft'|trans }}</button>
                    </div>
                    <div class=\"note\"> 
                        <p>{{ 'singup_note_text'|trans }}</p>
                    </div>
                    <div class=\"form-row\">
                        <label>{{ 'singup_lable_1_text'|trans }}</label>
                        <input type=\"text\" placeholder=\"{{ 'singup_input_1_placeholder'|trans }}\">
                    </div>
                    <div class=\"form-row\">
                        <label>{{ 'singup_lable_2_text'|trans }}</label>
                        <input type=\"email\" placeholder=\"{{ 'singup_input_2_placeholder'|trans }}\">
                    </div>
                    <div class=\"form-row\">
                        <label>{{ 'singup_lable_3_text'|trans }}</label>
                        <input type=\"password\" placeholder=\"password\">
                    </div>
                    <div class=\"form-row privacy\">
                        <p>{{ 'signup_privacy_text'|trans|raw }}</p>
                    </div>
                    <div class=\"form-row\">
                        <input type=\"submit\" class=\"btn-primary\" value=\"{{ 'singup_button_text'|trans }}\">
                    </div>
                    <div class=\"form-row align-center singup-with\">
                        <a href=\"#\">{{ 'singup_sso_text'|trans }}</a>
                    </div>
                    <div class=\"form-row\">
                        <ul class=\"form-links align-center\">
                            <li>Already have an account? <a href=\"{{ path('login_page') }}\">Log in</a>.</li>
                        </ul>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>
{% endblock %}", "sign_up.html.twig", "/Users/user/dev/lokalize/lokalise/templates/sign_up.html.twig");
    }
}
