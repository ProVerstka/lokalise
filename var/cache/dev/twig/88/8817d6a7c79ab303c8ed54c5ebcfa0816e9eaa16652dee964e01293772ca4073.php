<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* gitlab_integration.html.twig */
class __TwigTemplate_08003303f632b8c2013c31e40cb6e5928b61960b8cf2a2855ec8db0083fff4bd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gitlab_integration.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "gitlab_integration.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "gitlab_integration.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("header_logotype"), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_page_title"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <section id=\"promo\">
        <h1>";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_promo_title");
        echo "</h1>
        <div class=\"text\">
            <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_promo_text"), "html", null, true);
        echo "</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_try"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\" class=\"btn\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_btn_demo"), "html", null, true);
        echo "</a></li>
            </ul>
            <p class=\"note\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("index_promo_note_text"), "html", null, true);
        echo "</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_title");
        echo "</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_connect.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_title_1"), "html", null, true);
        echo "</strong>
                <p>";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_text_1");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_push.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_title_2"), "html", null, true);
        echo "</strong>
                <p>";
        // line 28
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_text_2");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_translate_02.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_title_3"), "html", null, true);
        echo "</strong>
                <p>";
        // line 33
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_text_3");
        echo "</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_pull_02.svg"), "html", null, true);
        echo "\"></div>
                <strong class=\"title\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_title_4"), "html", null, true);
        echo "</strong>
                <p>";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_tools_list_text_4");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>";
        // line 43
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_text_block_title");
        echo "</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_1_title");
        echo "</strong>
                <p>";
        // line 47
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_1_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_2_title");
        echo "</strong>
                <p>";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_2_text");
        echo "</p>
            </li>
            <li>
                 <strong class=\"title\">";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_3_title");
        echo "</strong>
                <p>";
        // line 55
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_intergation_privacy_list_item_3_text");
        echo "</p>
            </li>
        </ul>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_title");
        echo "</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_43.png"), "html", null, true);
        echo "\" media=\"(max-width: 768px)\">
                <source srcset=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_43@2x.png"), "html", null, true);
        echo " 2x\">
                <img srcset=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/img_43.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_title"), "html", null, true);
        echo "\">
            </picture>
        </div>
        <div class=\"three-columns\">
            <div class=\"col\">
                <h3>";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_1_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_1_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col\">
                <h3>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_2_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_2_text"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"col\">
                <h3>";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_3_title"), "html", null, true);
        echo "</h3>
                <p>";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_info_block_item_3_text"), "html", null, true);
        echo "</p>
            </div>
        </div>
    </section>
    <section class=\"fuq\">
        <h2>";
        // line 84
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_title");
        echo "</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">";
        // line 87
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_1_title");
        echo "</div>
                <div class=\"text\">";
        // line 88
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_1_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_2_title");
        echo "</div>
                <div class=\"text\">";
        // line 92
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_2_text");
        echo "</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_3_title");
        echo "</div>
                <div class=\"text\">";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_fuq_item_3_text");
        echo "</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">";
        // line 101
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("gitlab_integration_connect_title");
        echo "</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"";
        // line 104
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("gitlab_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_amazon.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Amazon S3</strong>
                    <span>";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_android.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Android</strong>
                    <span>";
        // line 114
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_api.svg"), "html", null, true);
        echo "\"></span>
                    <strong>API</strong>
                    <span>";
        // line 121
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"";
        // line 125
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("asana_integration_page");
        echo "\">
                    <span class=\"icon\"><img src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_asana.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Asana</strong>
                    <span>";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_bitbucket.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Bitbucket</strong>
                    <span>";
        // line 135
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/icon_int_cake_php.svg"), "html", null, true);
        echo "\"></span>
                    <strong>Cake PHP</strong>
                    <span>";
        // line 142
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("integrations_list_item_text"), "html", null, true);
        echo "</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">";
        // line 148
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_title"), "html", null, true);
        echo "</h2>
        <p>";
        // line 149
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_text"), "html", null, true);
        echo "</p>
        <a href=\"";
        // line 150
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signup_page");
        echo "\" class=\"btn-secondary btn-large\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("try_link"), "html", null, true);
        echo "</a>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "gitlab_integration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 150,  414 => 149,  410 => 148,  401 => 142,  396 => 140,  388 => 135,  383 => 133,  375 => 128,  370 => 126,  366 => 125,  359 => 121,  354 => 119,  346 => 114,  341 => 112,  333 => 107,  328 => 105,  324 => 104,  318 => 101,  310 => 96,  306 => 95,  300 => 92,  296 => 91,  290 => 88,  286 => 87,  280 => 84,  272 => 79,  268 => 78,  262 => 75,  258 => 74,  252 => 71,  248 => 70,  238 => 65,  234 => 64,  230 => 63,  224 => 60,  216 => 55,  212 => 54,  206 => 51,  202 => 50,  196 => 47,  192 => 46,  186 => 43,  178 => 38,  174 => 37,  170 => 36,  164 => 33,  160 => 32,  156 => 31,  150 => 28,  146 => 27,  142 => 26,  136 => 23,  132 => 22,  128 => 21,  122 => 18,  115 => 14,  110 => 12,  106 => 11,  101 => 9,  96 => 7,  93 => 6,  83 => 5,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}
{% block title %}
{{ 'header_logotype'|trans }} | {{ 'gitlab_integration_page_title'|trans }}
{% endblock %}
{% block body %}
    <section id=\"promo\">
        <h1>{{ 'gitlab_integration_promo_title'|trans|raw }}</h1>
        <div class=\"text\">
            <p>{{ 'gitlab_integration_promo_text'|trans }}</p>
            <ul class=\"buttons\">
                <li><a href=\"#\" class=\"btn-primary\">{{ 'index_promo_btn_try'|trans }}</a></li>
                <li><a href=\"#\" class=\"btn\">{{ 'index_promo_btn_demo'|trans }}</a></li>
            </ul>
            <p class=\"note\">{{ 'index_promo_note_text'|trans }}</p>
        </div> 
    </section>
    <section class=\"tools bg-wood\">
        <h2>{{ 'gitlab_intergation_tools_title'|trans|raw }}</h2>
        <ul class=\"tools-list\">
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_connect.svg')}}\"></div>
                <strong class=\"title\">{{ 'gitlab_intergation_tools_list_title_1'|trans }}</strong>
                <p>{{ 'gitlab_intergation_tools_list_text_1'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_push.svg')}}\"></div>
                <strong class=\"title\">{{ 'gitlab_intergation_tools_list_title_2'|trans }}</strong>
                <p>{{ 'gitlab_intergation_tools_list_text_2'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_translate_02.svg')}}\"></div>
                <strong class=\"title\">{{ 'gitlab_intergation_tools_list_title_3'|trans }}</strong>
                <p>{{ 'gitlab_intergation_tools_list_text_3'|trans|raw }}</p>
            </li>
            <li>
                <div class=\"visual\"><img src=\"{{asset('build/icon_pull_02.svg')}}\"></div>
                <strong class=\"title\">{{ 'gitlab_intergation_tools_list_title_4'|trans }}</strong>
                <p>{{ 'gitlab_intergation_tools_list_text_4'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"privacy-block bg-emptiness\">
        <h2>{{ 'gitlab_intergation_text_block_title'|trans|raw }}</h2>
        <ul class=\"privacy-list\">   
            <li>
                <strong class=\"title\">{{ 'gitlab_intergation_privacy_list_item_1_title'|trans|raw }}</strong>
                <p>{{ 'gitlab_intergation_privacy_list_item_1_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'gitlab_intergation_privacy_list_item_2_title'|trans|raw }}</strong>
                <p>{{ 'gitlab_intergation_privacy_list_item_2_text'|trans|raw }}</p>
            </li>
            <li>
                 <strong class=\"title\">{{ 'gitlab_intergation_privacy_list_item_3_title'|trans|raw }}</strong>
                <p>{{ 'gitlab_intergation_privacy_list_item_3_text'|trans|raw }}</p>
            </li>
        </ul>
    </section>
    <section class=\"info-block\">
        <h2 class=\"heading-h1 align-center\">{{ 'gitlab_info_block_title'|trans|raw }}</h2>
        <div class=\"image align-center\">
            <picture>
                <source srcset=\"{{asset('build/img_43.png')}}\" media=\"(max-width: 768px)\">
                <source srcset=\"{{asset('build/img_43@2x.png')}} 2x\">
                <img srcset=\"{{asset('build/img_43.png')}}\" alt=\"{{ 'gitlab_info_block_title'|trans }}\">
            </picture>
        </div>
        <div class=\"three-columns\">
            <div class=\"col\">
                <h3>{{ 'gitlab_info_block_item_1_title'|trans }}</h3>
                <p>{{ 'gitlab_info_block_item_1_text'|trans }}</p>
            </div>
            <div class=\"col\">
                <h3>{{ 'gitlab_info_block_item_2_title'|trans }}</h3>
                <p>{{ 'gitlab_info_block_item_2_text'|trans }}</p>
            </div>
            <div class=\"col\">
                <h3>{{ 'gitlab_info_block_item_3_title'|trans }}</h3>
                <p>{{ 'gitlab_info_block_item_3_text'|trans }}</p>
            </div>
        </div>
    </section>
    <section class=\"fuq\">
        <h2>{{ 'gitlab_integration_fuq_title'|trans|raw }}</h2>
        <div class=\"fuq-list\">
            <div class=\"item\">
                <div class=\"head\">{{ 'gitlab_integration_fuq_item_1_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'gitlab_integration_fuq_item_1_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'gitlab_integration_fuq_item_2_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'gitlab_integration_fuq_item_2_text'|trans|raw }}</div>
            </div>
            <div class=\"item\">
                <div class=\"head\">{{ 'gitlab_integration_fuq_item_3_title'|trans|raw }}</div>
                <div class=\"text\">{{ 'gitlab_integration_fuq_item_3_text'|trans|raw }}</div>
            </div>
        </div>
    </section>
    <section class=\"connect bg-emptiness\">
        <h2 class=\"align-center\">{{ 'gitlab_integration_connect_title'|trans|raw }}</h2>
        <ul class=\"integration-list\">
            <li>
                <a href=\"{{ path('gitlab_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_amazon.svg') }}\"></span>
                    <strong>Amazon S3</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_android.svg') }}\"></span>
                    <strong>Android</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_api.svg') }}\"></span>
                    <strong>API</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"{{ path('asana_integration_page') }}\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_asana.svg') }}\"></span>
                    <strong>Asana</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_bitbucket.svg') }}\"></span>
                    <strong>Bitbucket</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
            <li>
                <a href=\"#\">
                    <span class=\"icon\"><img src=\"{{ asset('build/icon_int_cake_php.svg') }}\"></span>
                    <strong>Cake PHP</strong>
                    <span>{{ 'integrations_list_item_text'|trans }}</span>
                </a>
            </li>
        </ul>
    </section>
    <section class=\"try-now\">
        <h2 class=\"heading-h1\">{{ 'try_title'|trans }}</h2>
        <p>{{ 'try_text'|trans }}</p>
        <a href=\"{{ path('signup_page') }}\" class=\"btn-secondary btn-large\">{{ 'try_link'|trans }}</a>
    </section>
{% endblock %}", "gitlab_integration.html.twig", "/Users/user/dev/lokalize/lokalise/templates/gitlab_integration.html.twig");
    }
}
