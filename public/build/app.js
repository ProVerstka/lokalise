(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/app.scss":
/*!*****************************!*\
  !*** ./assets/css/app.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! core-js/modules/es.array.filter */ "./node_modules/core-js/modules/es.array.filter.js");

__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

__webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.date.to-string */ "./node_modules/core-js/modules/es.date.to-string.js");

__webpack_require__(/*! core-js/modules/es.function.bind */ "./node_modules/core-js/modules/es.function.bind.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");

__webpack_require__(/*! core-js/modules/es.string.trim */ "./node_modules/core-js/modules/es.string.trim.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you require will output into a single css file (app.css in this case)
__webpack_require__(/*! ../css/app.scss */ "./assets/css/app.scss"); // Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
//const $ = require('jquery');


$(document).ready(function () {
  initPriceTable(); // match height on price table

  initFuqList(); // slideDown fuq block on click

  initScroll(); // adding border on header by scroll

  initClosePopup(); // closing cookie popup

  initMobileMenu(); // mobile menu

  initTabs(); // tabs and gallery swyper

  initLazyLoad(); // lazy load plugin

  initIntegrationSwitcher();

  function initLazyLoad() {
    $('img, video, picture').Lazy();
  }

  function initPriceTable() {
    $(window).resize(function () {
      $('.price-table .columns').each(function () {
        $('.row', $(this)).removeAttr('style');
        var $rows = [];
        $('.col', $(this)).each(function () {
          $('.row', $(this)).each(function (index) {
            if ($(this).height() > $rows[index] || $rows[index] == undefined) {
              $rows[index] = $(this).height();
            }
          });
        });
        $('.col', $(this)).each(function () {
          $('.row', $(this)).each(function (index) {
            $(this).css('height', $rows[index]);
          });
        });
      });

      if (window.innerWidth >= 800) {
        $('.price-table .columns .col').removeAttr('style');
        $('.table-nav .active').removeClass('active');
        $('.table-nav li:first').addClass('active');
      }
    }).trigger('resize');
    $('.table-nav span').click(function () {
      $('.price-table .columns .col').hide().eq($(this).closest('li').index() + 1).show();
      $('.price-table .columns.no-borders .col').hide().eq($(this).closest('li').index() + 1).show();
      $('.price-table .columns.no-borders .col:first').show();
      $(this).closest('li').addClass('active').siblings().removeClass('active');
      return false;
    });
  }

  function initFuqList() {
    $('.fuq-list .head').click(function () {
      $(this).next('.text').slideToggle(300, function () {
        $(this).closest('.item').toggleClass('active');
      });
      return false;
    });
  }

  function initScroll() {
    $(window).scroll(function () {
      if ($(window).scrollTop() > 0) {
        $('#header').addClass('scroll');
      } else {
        $('#header').removeClass('scroll');
      }

      if ($('.price-table').length) {
        if ($(window).scrollTop() + 200 > $('.price-table').offset().top) {
          $('.price-table').addClass('scroll');
        } else {
          $('.price-table').removeClass('scroll');
        }
      }
    });
  }

  function initClosePopup() {
    $('.cookie-popup .close').click(function () {
      $(this).closest('.cookie-popup').fadeOut(300);
      return false;
    });
  }

  function initMobileMenu() {
    $('#header .mb-burger').click(function () {
      $('#header').toggleClass('mb-open-nav');
      $('#navigation .menu .has-drop').closest('li').removeClass('active');
      $('#header .back').hide();
      return false;
    });
    $('.sidebar .mb-burger, .open-categories-menu').click(function () {
      $('.sidebar').toggleClass('mb-open-nav');
      return false;
    });
    $('#navigation .has-drop').click(function () {
      if ($(window).innerWidth() <= 768) {
        $('#header .back').text($(this).text()).show();
        $(this).closest('li').addClass('active');
        return false;
      }
    });
    $('#header').prepend('<span class="back"></span>');
    $('#header .back').click(function () {
      $('#navigation .menu .has-drop').closest('li').removeClass('active');
      $(this).hide();
      return false;
    });
    $(window).resize(function () {
      if ($(window).innerWidth() > 768) {
        $('#header').removeClass('mb-open-nav');
        $('#navigation .menu li.active').removeClass('active');
      }
    });
  }

  function initTabs() {
    var swiper = new Swiper('.tabcontrol', {
      direction: 'horizontal',
      slidesPerView: 'auto',
      centeredSlides: true,
      freeMode: true,
      mousewheel: true,
      breakpoints: {
        768: {
          centeredSlides: false
        }
      },
      on: {
        slideChange: function slideChange() {
          $('.tabset .tabs .tab').hide().eq($('.swiper-slide-active').index()).show();
        }
      }
    });
    $('.tabcontrol .item').click(function () {
      swiper.slideTo($(this).index());
      $('.tabset .tabs .tab').hide().eq($(this).index()).show();
      $(this).addClass('swiper-slide-active').siblings().removeClass('swiper-slide-active');
    });
  }

  function initIntegrationSwitcher() {
    $('.categories-switcher a').click(function () {
      $(this).closest('li').addClass('active').siblings().removeClass('active');
      $('.sidebar').removeClass('mb-open-nav');
      $('.integration-list li').hide();
      $('.integration-list .' + $(this).data('category')).fadeIn(300);
      $(window).trigger('scroll');
      return false;
    });
    $('.sidebar .link-holder .more').click(function () {
      $('.categories-switcher li').removeClass('active');
      $('.integration-list li').fadeIn(300);
      return false;
    });
  }
});
/*! jQuery & Zepto Lazy v1.7.10 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */

!function (t, e) {
  "use strict";

  function r(r, a, i, u, l) {
    function f() {
      L = t.devicePixelRatio > 1, i = c(i), a.delay >= 0 && setTimeout(function () {
        s(!0);
      }, a.delay), (a.delay < 0 || a.combined) && (u.e = v(a.throttle, function (t) {
        "resize" === t.type && (w = B = -1), s(t.all);
      }), u.a = function (t) {
        t = c(t), i.push.apply(i, t);
      }, u.g = function () {
        return i = n(i).filter(function () {
          return !n(this).data(a.loadedName);
        });
      }, u.f = function (t) {
        for (var e = 0; e < t.length; e++) {
          var r = i.filter(function () {
            return this === t[e];
          });
          r.length && s(!1, r);
        }
      }, s(), n(a.appendScroll).on("scroll." + l + " resize." + l, u.e));
    }

    function c(t) {
      var i = a.defaultImage,
          o = a.placeholder,
          u = a.imageBase,
          l = a.srcsetAttribute,
          f = a.loaderAttribute,
          c = a._f || {};
      t = n(t).filter(function () {
        var t = n(this),
            r = m(this);
        return !t.data(a.handledName) && (t.attr(a.attribute) || t.attr(l) || t.attr(f) || c[r] !== e);
      }).data("plugin_" + a.name, r);

      for (var s = 0, d = t.length; s < d; s++) {
        var A = n(t[s]),
            g = m(t[s]),
            h = A.attr(a.imageBaseAttribute) || u;
        g === N && h && A.attr(l) && A.attr(l, b(A.attr(l), h)), c[g] === e || A.attr(f) || A.attr(f, c[g]), g === N && i && !A.attr(E) ? A.attr(E, i) : g === N || !o || A.css(O) && "none" !== A.css(O) || A.css(O, "url('" + o + "')");
      }

      return t;
    }

    function s(t, e) {
      if (!i.length) return void (a.autoDestroy && r.destroy());

      for (var o = e || i, u = !1, l = a.imageBase || "", f = a.srcsetAttribute, c = a.handledName, s = 0; s < o.length; s++) {
        if (t || e || A(o[s])) {
          var g = n(o[s]),
              h = m(o[s]),
              b = g.attr(a.attribute),
              v = g.attr(a.imageBaseAttribute) || l,
              p = g.attr(a.loaderAttribute);
          g.data(c) || a.visibleOnly && !g.is(":visible") || !((b || g.attr(f)) && (h === N && (v + b !== g.attr(E) || g.attr(f) !== g.attr(F)) || h !== N && v + b !== g.css(O)) || p) || (u = !0, g.data(c, !0), d(g, h, v, p));
        }
      }

      u && (i = n(i).filter(function () {
        return !n(this).data(c);
      }));
    }

    function d(t, e, r, i) {
      ++z;

      var _o = function o() {
        y("onError", t), p(), _o = n.noop;
      };

      y("beforeLoad", t);
      var u = a.attribute,
          l = a.srcsetAttribute,
          f = a.sizesAttribute,
          c = a.retinaAttribute,
          s = a.removeAttribute,
          d = a.loadedName,
          A = t.attr(c);

      if (i) {
        var _g = function g() {
          s && t.removeAttr(a.loaderAttribute), t.data(d, !0), y(T, t), setTimeout(p, 1), _g = n.noop;
        };

        t.off(I).one(I, _o).one(D, _g), y(i, t, function (e) {
          e ? (t.off(D), _g()) : (t.off(I), _o());
        }) || t.trigger(I);
      } else {
        var h = n(new Image());
        h.one(I, _o).one(D, function () {
          t.hide(), e === N ? t.attr(C, h.attr(C)).attr(F, h.attr(F)).attr(E, h.attr(E)) : t.css(O, "url('" + h.attr(E) + "')"), t[a.effect](a.effectTime), s && (t.removeAttr(u + " " + l + " " + c + " " + a.imageBaseAttribute), f !== C && t.removeAttr(f)), t.data(d, !0), y(T, t), h.remove(), p();
        });
        var m = (L && A ? A : t.attr(u)) || "";
        h.attr(C, t.attr(f)).attr(F, t.attr(l)).attr(E, m ? r + m : null), h.complete && h.trigger(D);
      }
    }

    function A(t) {
      var e = t.getBoundingClientRect(),
          r = a.scrollDirection,
          n = a.threshold,
          i = h() + n > e.top && -n < e.bottom,
          o = g() + n > e.left && -n < e.right;
      return "vertical" === r ? i : "horizontal" === r ? o : i && o;
    }

    function g() {
      return w >= 0 ? w : w = n(t).width();
    }

    function h() {
      return B >= 0 ? B : B = n(t).height();
    }

    function m(t) {
      return t.tagName.toLowerCase();
    }

    function b(t, e) {
      if (e) {
        var r = t.split(",");
        t = "";

        for (var a = 0, n = r.length; a < n; a++) {
          t += e + r[a].trim() + (a !== n - 1 ? "," : "");
        }
      }

      return t;
    }

    function v(t, e) {
      var n,
          i = 0;
      return function (o, u) {
        function l() {
          i = +new Date(), e.call(r, o);
        }

        var f = +new Date() - i;
        n && clearTimeout(n), f > t || !a.enableThrottle || u ? l() : n = setTimeout(l, t - f);
      };
    }

    function p() {
      --z, i.length || z || y("onFinishedAll");
    }

    function y(t, e, n) {
      return !!(t = a[t]) && (t.apply(r, [].slice.call(arguments, 1)), !0);
    }

    var z = 0,
        w = -1,
        B = -1,
        L = !1,
        T = "afterLoad",
        D = "load",
        I = "error",
        N = "img",
        E = "src",
        F = "srcset",
        C = "sizes",
        O = "background-image";
    "event" === a.bind || o ? f() : n(t).on(D + "." + l, f);
  }

  function a(a, o) {
    var u = this,
        l = n.extend({}, u.config, o),
        f = {},
        c = l.name + "-" + ++i;
    return u.config = function (t, r) {
      return r === e ? l[t] : (l[t] = r, u);
    }, u.addItems = function (t) {
      return f.a && f.a("string" === n.type(t) ? n(t) : t), u;
    }, u.getItems = function () {
      return f.g ? f.g() : {};
    }, u.update = function (t) {
      return f.e && f.e({}, !t), u;
    }, u.force = function (t) {
      return f.f && f.f("string" === n.type(t) ? n(t) : t), u;
    }, u.loadAll = function () {
      return f.e && f.e({
        all: !0
      }, !0), u;
    }, u.destroy = function () {
      return n(l.appendScroll).off("." + c, f.e), n(t).off("." + c), f = {}, e;
    }, r(u, l, a, f, c), l.chainable ? a : u;
  }

  var n = t.jQuery || t.Zepto,
      i = 0,
      o = !1;
  n.fn.Lazy = n.fn.lazy = function (t) {
    return new a(this, t);
  }, n.Lazy = n.lazy = function (t, r, i) {
    if (n.isFunction(r) && (i = r, r = []), n.isFunction(i)) {
      t = n.isArray(t) ? t : [t], r = n.isArray(r) ? r : [r];

      for (var o = a.prototype.config, u = o._f || (o._f = {}), l = 0, f = t.length; l < f; l++) {
        (o[t[l]] === e || n.isFunction(o[t[l]])) && (o[t[l]] = i);
      }

      for (var c = 0, s = r.length; c < s; c++) {
        u[r[c]] = t[0];
      }
    }
  }, a.prototype.config = {
    name: "lazy",
    chainable: !0,
    autoDestroy: !0,
    bind: "load",
    threshold: 500,
    visibleOnly: !1,
    appendScroll: t,
    scrollDirection: "both",
    imageBase: null,
    defaultImage: "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
    placeholder: null,
    delay: -1,
    combined: !1,
    attribute: "data-src",
    srcsetAttribute: "data-srcset",
    sizesAttribute: "data-sizes",
    retinaAttribute: "data-retina",
    loaderAttribute: "data-loader",
    imageBaseAttribute: "data-imagebase",
    removeAttribute: !0,
    handledName: "handled",
    loadedName: "loaded",
    effect: "show",
    effectTime: 0,
    enableThrottle: !0,
    throttle: 250,
    beforeLoad: e,
    afterLoad: e,
    onError: e,
    onFinishedAll: e
  }, n(t).on("load", function () {
    o = !0;
  });
}(window);
/*! jQuery & Zepto Lazy - AV Plugin v1.4 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */

!function (t) {
  t.lazy(["av", "audio", "video"], ["audio", "video"], function (a, e) {
    var r = a[0].tagName.toLowerCase();

    if ("audio" === r || "video" === r) {
      var o = a.find("data-src"),
          i = a.find("data-track"),
          n = 0,
          c = function c() {
        ++n === o.length && e(!1);
      },
          d = function d() {
        var a = t(this),
            e = a[0].tagName.toLowerCase(),
            r = a.prop("attributes"),
            o = t("data-src" === e ? "<source>" : "<track>");
        "data-src" === e && o.one("error", c), t.each(r, function (t, a) {
          o.attr(a.name, a.value);
        }), a.replaceWith(o);
      };

      a.one("loadedmetadata", function () {
        e(!0);
      }).off("load error").attr("poster", a.attr("data-poster")), o.length ? o.each(d) : a.attr("data-src") ? (t.each(a.attr("data-src").split(","), function (e, r) {
        var o = r.split("|");
        a.append(t("<source>").one("error", c).attr({
          src: o[0].trim(),
          type: o[1].trim()
        }));
      }), this.config("removeAttribute") && a.removeAttr("data-src")) : e(!1), i.length && i.each(d);
    } else e(!1);
  });
}(window.jQuery || window.Zepto);
/*! jQuery & Zepto Lazy - Picture Plugin v1.3 - http://jquery.eisbehr.de/lazy - MIT&GPL-2.0 license - Copyright 2012-2018 Daniel 'Eisbehr' Kern */

!function (t) {
  function e(e, a, n) {
    var o = e.prop("attributes"),
        c = t("<" + a + ">");
    return t.each(o, function (t, e) {
      "srcset" !== e.name && e.name !== i || (e.value = r(e.value, n)), c.attr(e.name, e.value);
    }), e.replaceWith(c), c;
  }

  function a(e, a, r) {
    var i = t("<img>").one("load", function () {
      r(!0);
    }).one("error", function () {
      r(!1);
    }).appendTo(e).attr("src", a);
    i.complete && i.load();
  }

  function r(t, e) {
    if (e) {
      var a = t.split(",");
      t = "";

      for (var r = 0, i = a.length; r < i; r++) {
        t += e + a[r].trim() + (r !== i - 1 ? "," : "");
      }
    }

    return t;
  }

  var i = "data-src";
  t.lazy(["pic", "picture"], ["picture"], function (n, o) {
    if ("picture" === n[0].tagName.toLowerCase()) {
      var c = n.find(i),
          s = n.find("data-img"),
          u = this.config("imageBase") || "";
      c.length ? (c.each(function () {
        e(t(this), "source", u);
      }), 1 === s.length ? (s = e(s, "img", u), s.on("load", function () {
        o(!0);
      }).on("error", function () {
        o(!1);
      }), s.attr("src", s.attr(i)), this.config("removeAttribute") && s.removeAttr(i)) : n.attr(i) ? (a(n, u + n.attr(i), o), this.config("removeAttribute") && n.removeAttr(i)) : o(!1)) : n.attr("data-srcset") ? (t("<source>").attr({
        media: n.attr("data-media"),
        sizes: n.attr("data-sizes"),
        type: n.attr("data-type"),
        srcset: r(n.attr("data-srcset"), u)
      }).appendTo(n), a(n, u + n.attr(i), o), this.config("removeAttribute") && n.removeAttr(i + " data-srcset data-media data-sizes data-type")) : o(!1);
    } else o(!1);
  });
}(window.jQuery || window.Zepto);

/***/ })

},[["./assets/js/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2FwcC5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9hcHAuanMiXSwibmFtZXMiOlsicmVxdWlyZSIsIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiaW5pdFByaWNlVGFibGUiLCJpbml0RnVxTGlzdCIsImluaXRTY3JvbGwiLCJpbml0Q2xvc2VQb3B1cCIsImluaXRNb2JpbGVNZW51IiwiaW5pdFRhYnMiLCJpbml0TGF6eUxvYWQiLCJpbml0SW50ZWdyYXRpb25Td2l0Y2hlciIsIkxhenkiLCJ3aW5kb3ciLCJyZXNpemUiLCJlYWNoIiwicmVtb3ZlQXR0ciIsIiRyb3dzIiwiaW5kZXgiLCJoZWlnaHQiLCJ1bmRlZmluZWQiLCJjc3MiLCJpbm5lcldpZHRoIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsInRyaWdnZXIiLCJjbGljayIsImhpZGUiLCJlcSIsImNsb3Nlc3QiLCJzaG93Iiwic2libGluZ3MiLCJuZXh0Iiwic2xpZGVUb2dnbGUiLCJ0b2dnbGVDbGFzcyIsInNjcm9sbCIsInNjcm9sbFRvcCIsImxlbmd0aCIsIm9mZnNldCIsInRvcCIsImZhZGVPdXQiLCJ0ZXh0IiwicHJlcGVuZCIsInN3aXBlciIsIlN3aXBlciIsImRpcmVjdGlvbiIsInNsaWRlc1BlclZpZXciLCJjZW50ZXJlZFNsaWRlcyIsImZyZWVNb2RlIiwibW91c2V3aGVlbCIsImJyZWFrcG9pbnRzIiwib24iLCJzbGlkZUNoYW5nZSIsInNsaWRlVG8iLCJkYXRhIiwiZmFkZUluIiwidCIsImUiLCJyIiwiYSIsImkiLCJ1IiwibCIsImYiLCJMIiwiZGV2aWNlUGl4ZWxSYXRpbyIsImMiLCJkZWxheSIsInNldFRpbWVvdXQiLCJzIiwiY29tYmluZWQiLCJ2IiwidGhyb3R0bGUiLCJ0eXBlIiwidyIsIkIiLCJhbGwiLCJwdXNoIiwiYXBwbHkiLCJnIiwibiIsImZpbHRlciIsImxvYWRlZE5hbWUiLCJhcHBlbmRTY3JvbGwiLCJkZWZhdWx0SW1hZ2UiLCJvIiwicGxhY2Vob2xkZXIiLCJpbWFnZUJhc2UiLCJzcmNzZXRBdHRyaWJ1dGUiLCJsb2FkZXJBdHRyaWJ1dGUiLCJfZiIsIm0iLCJoYW5kbGVkTmFtZSIsImF0dHIiLCJhdHRyaWJ1dGUiLCJuYW1lIiwiZCIsIkEiLCJoIiwiaW1hZ2VCYXNlQXR0cmlidXRlIiwiTiIsImIiLCJFIiwiTyIsImF1dG9EZXN0cm95IiwiZGVzdHJveSIsInAiLCJ2aXNpYmxlT25seSIsImlzIiwiRiIsInoiLCJ5Iiwibm9vcCIsInNpemVzQXR0cmlidXRlIiwicmV0aW5hQXR0cmlidXRlIiwicmVtb3ZlQXR0cmlidXRlIiwiVCIsIm9mZiIsIkkiLCJvbmUiLCJEIiwiSW1hZ2UiLCJDIiwiZWZmZWN0IiwiZWZmZWN0VGltZSIsInJlbW92ZSIsImNvbXBsZXRlIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0Iiwic2Nyb2xsRGlyZWN0aW9uIiwidGhyZXNob2xkIiwiYm90dG9tIiwibGVmdCIsInJpZ2h0Iiwid2lkdGgiLCJ0YWdOYW1lIiwidG9Mb3dlckNhc2UiLCJzcGxpdCIsInRyaW0iLCJEYXRlIiwiY2FsbCIsImNsZWFyVGltZW91dCIsImVuYWJsZVRocm90dGxlIiwic2xpY2UiLCJhcmd1bWVudHMiLCJiaW5kIiwiZXh0ZW5kIiwiY29uZmlnIiwiYWRkSXRlbXMiLCJnZXRJdGVtcyIsInVwZGF0ZSIsImZvcmNlIiwibG9hZEFsbCIsImNoYWluYWJsZSIsImpRdWVyeSIsIlplcHRvIiwiZm4iLCJsYXp5IiwiaXNGdW5jdGlvbiIsImlzQXJyYXkiLCJwcm90b3R5cGUiLCJiZWZvcmVMb2FkIiwiYWZ0ZXJMb2FkIiwib25FcnJvciIsIm9uRmluaXNoZWRBbGwiLCJmaW5kIiwicHJvcCIsInZhbHVlIiwicmVwbGFjZVdpdGgiLCJhcHBlbmQiLCJzcmMiLCJhcHBlbmRUbyIsImxvYWQiLCJtZWRpYSIsInNpemVzIiwic3Jjc2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSx1Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0NBOzs7Ozs7QUFPQTtBQUNBQSxtQkFBTyxDQUFDLDhDQUFELENBQVAsQyxDQUVBO0FBQ0E7OztBQUdBQyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVU7QUFDekJDLGdCQUFjLEdBRFcsQ0FDSjs7QUFDckJDLGFBQVcsR0FGYyxDQUVSOztBQUNqQkMsWUFBVSxHQUhlLENBR1I7O0FBQ2pCQyxnQkFBYyxHQUpXLENBSUo7O0FBQ3JCQyxnQkFBYyxHQUxXLENBS047O0FBQ25CQyxVQUFRLEdBTmlCLENBTVo7O0FBQ2JDLGNBQVksR0FQYSxDQU9SOztBQUNqQkMseUJBQXVCOztBQUV2QixXQUFTRCxZQUFULEdBQXlCO0FBQ3BCVCxLQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QlcsSUFBekI7QUFDSjs7QUFFRCxXQUFTUixjQUFULEdBQTBCO0FBQ3RCSCxLQUFDLENBQUNZLE1BQUQsQ0FBRCxDQUFVQyxNQUFWLENBQWlCLFlBQVU7QUFDdEJiLE9BQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCYyxJQUEzQixDQUFnQyxZQUFVO0FBQ3RDZCxTQUFDLENBQUMsTUFBRCxFQUFRQSxDQUFDLENBQUMsSUFBRCxDQUFULENBQUQsQ0FBa0JlLFVBQWxCLENBQTZCLE9BQTdCO0FBQ0EsWUFBSUMsS0FBSyxHQUFHLEVBQVo7QUFDQWhCLFNBQUMsQ0FBQyxNQUFELEVBQVFBLENBQUMsQ0FBQyxJQUFELENBQVQsQ0FBRCxDQUFrQmMsSUFBbEIsQ0FBdUIsWUFBVTtBQUM3QmQsV0FBQyxDQUFDLE1BQUQsRUFBUUEsQ0FBQyxDQUFDLElBQUQsQ0FBVCxDQUFELENBQWtCYyxJQUFsQixDQUF1QixVQUFTRyxLQUFULEVBQWU7QUFDbEMsZ0JBQUlqQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFrQixNQUFSLEtBQW1CRixLQUFLLENBQUNDLEtBQUQsQ0FBeEIsSUFBbUNELEtBQUssQ0FBQ0MsS0FBRCxDQUFMLElBQWdCRSxTQUF2RCxFQUFrRTtBQUM5REgsbUJBQUssQ0FBQ0MsS0FBRCxDQUFMLEdBQWVqQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFrQixNQUFSLEVBQWY7QUFDSDtBQUNKLFdBSkQ7QUFLSCxTQU5EO0FBT0FsQixTQUFDLENBQUMsTUFBRCxFQUFRQSxDQUFDLENBQUMsSUFBRCxDQUFULENBQUQsQ0FBa0JjLElBQWxCLENBQXVCLFlBQVU7QUFDN0JkLFdBQUMsQ0FBQyxNQUFELEVBQVFBLENBQUMsQ0FBQyxJQUFELENBQVQsQ0FBRCxDQUFrQmMsSUFBbEIsQ0FBdUIsVUFBU0csS0FBVCxFQUFlO0FBQ2xDakIsYUFBQyxDQUFDLElBQUQsQ0FBRCxDQUFRb0IsR0FBUixDQUFZLFFBQVosRUFBcUJKLEtBQUssQ0FBQ0MsS0FBRCxDQUExQjtBQUNILFdBRkQ7QUFHSCxTQUpEO0FBS0gsT0FmRDs7QUFnQkEsVUFBSUwsTUFBTSxDQUFDUyxVQUFQLElBQXFCLEdBQXpCLEVBQThCO0FBQzFCckIsU0FBQyxDQUFDLDRCQUFELENBQUQsQ0FBZ0NlLFVBQWhDLENBQTJDLE9BQTNDO0FBQ0FmLFNBQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCc0IsV0FBeEIsQ0FBb0MsUUFBcEM7QUFDQXRCLFNBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCdUIsUUFBekIsQ0FBa0MsUUFBbEM7QUFDSDtBQUNMLEtBdEJELEVBc0JHQyxPQXRCSCxDQXNCVyxRQXRCWDtBQXVCQXhCLEtBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCeUIsS0FBckIsQ0FBMkIsWUFBVTtBQUNqQ3pCLE9BQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDMEIsSUFBaEMsR0FBdUNDLEVBQXZDLENBQTBDM0IsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNEIsT0FBUixDQUFnQixJQUFoQixFQUFzQlgsS0FBdEIsS0FBOEIsQ0FBeEUsRUFBMkVZLElBQTNFO0FBQ0E3QixPQUFDLENBQUMsdUNBQUQsQ0FBRCxDQUEyQzBCLElBQTNDLEdBQWtEQyxFQUFsRCxDQUFxRDNCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTRCLE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JYLEtBQXRCLEtBQThCLENBQW5GLEVBQXNGWSxJQUF0RjtBQUNBN0IsT0FBQyxDQUFDLDZDQUFELENBQUQsQ0FBaUQ2QixJQUFqRDtBQUNBN0IsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNEIsT0FBUixDQUFnQixJQUFoQixFQUFzQkwsUUFBdEIsQ0FBK0IsUUFBL0IsRUFBeUNPLFFBQXpDLEdBQW9EUixXQUFwRCxDQUFnRSxRQUFoRTtBQUNBLGFBQU8sS0FBUDtBQUNILEtBTkQ7QUFPSDs7QUFFRCxXQUFTbEIsV0FBVCxHQUF1QjtBQUNuQkosS0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5QixLQUFyQixDQUEyQixZQUFVO0FBQ2pDekIsT0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRK0IsSUFBUixDQUFhLE9BQWIsRUFBc0JDLFdBQXRCLENBQWtDLEdBQWxDLEVBQXVDLFlBQVU7QUFBQ2hDLFNBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTRCLE9BQVIsQ0FBZ0IsT0FBaEIsRUFBeUJLLFdBQXpCLENBQXFDLFFBQXJDO0FBQStDLE9BQWpHO0FBQ0EsYUFBTyxLQUFQO0FBQ0gsS0FIRDtBQUlIOztBQUVELFdBQVM1QixVQUFULEdBQXNCO0FBQ2pCTCxLQUFDLENBQUNZLE1BQUQsQ0FBRCxDQUFVc0IsTUFBVixDQUFpQixZQUFVO0FBQ3ZCLFVBQUlsQyxDQUFDLENBQUNZLE1BQUQsQ0FBRCxDQUFVdUIsU0FBVixLQUF3QixDQUE1QixFQUErQjtBQUMzQm5DLFNBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYXVCLFFBQWIsQ0FBc0IsUUFBdEI7QUFDSCxPQUZELE1BRU87QUFDSHZCLFNBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYXNCLFdBQWIsQ0FBeUIsUUFBekI7QUFDSDs7QUFDRCxVQUFJdEIsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQm9DLE1BQXRCLEVBQThCO0FBQzFCLFlBQUlwQyxDQUFDLENBQUNZLE1BQUQsQ0FBRCxDQUFVdUIsU0FBVixLQUF3QixHQUF4QixHQUE4Qm5DLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JxQyxNQUFsQixHQUEyQkMsR0FBN0QsRUFBa0U7QUFDOUR0QyxXQUFDLENBQUMsY0FBRCxDQUFELENBQWtCdUIsUUFBbEIsQ0FBMkIsUUFBM0I7QUFDSCxTQUZELE1BRU87QUFDSHZCLFdBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JzQixXQUFsQixDQUE4QixRQUE5QjtBQUNIO0FBQ0o7QUFDSixLQWJEO0FBY0g7O0FBRUQsV0FBU2hCLGNBQVQsR0FBMEI7QUFDdEJOLEtBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCeUIsS0FBMUIsQ0FBZ0MsWUFBVTtBQUN0Q3pCLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTRCLE9BQVIsQ0FBZ0IsZUFBaEIsRUFBaUNXLE9BQWpDLENBQXlDLEdBQXpDO0FBQ0EsYUFBTyxLQUFQO0FBQ0gsS0FIRDtBQUlIOztBQUVELFdBQVNoQyxjQUFULEdBQTBCO0FBQ3RCUCxLQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QnlCLEtBQXhCLENBQThCLFlBQVU7QUFDcEN6QixPQUFDLENBQUMsU0FBRCxDQUFELENBQWFpQyxXQUFiLENBQXlCLGFBQXpCO0FBQ0FqQyxPQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQzRCLE9BQWpDLENBQXlDLElBQXpDLEVBQStDTixXQUEvQyxDQUEyRCxRQUEzRDtBQUNBdEIsT0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQjBCLElBQW5CO0FBQ0EsYUFBTyxLQUFQO0FBQ0gsS0FMRDtBQU1BMUIsS0FBQyxDQUFDLDRDQUFELENBQUQsQ0FBZ0R5QixLQUFoRCxDQUFzRCxZQUFVO0FBQzVEekIsT0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjaUMsV0FBZCxDQUEwQixhQUExQjtBQUNBLGFBQU8sS0FBUDtBQUNILEtBSEQ7QUFJQWpDLEtBQUMsQ0FBQyx1QkFBRCxDQUFELENBQTJCeUIsS0FBM0IsQ0FBaUMsWUFBVTtBQUN2QyxVQUFJekIsQ0FBQyxDQUFDWSxNQUFELENBQUQsQ0FBVVMsVUFBVixNQUEwQixHQUE5QixFQUFtQztBQUMvQnJCLFNBQUMsQ0FBQyxlQUFELENBQUQsQ0FDS3dDLElBREwsQ0FDVXhDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FDTHdDLElBREssRUFEVixFQUVhWCxJQUZiO0FBR0E3QixTQUFDLENBQUMsSUFBRCxDQUFELENBQVE0QixPQUFSLENBQWdCLElBQWhCLEVBQXNCTCxRQUF0QixDQUErQixRQUEvQjtBQUNBLGVBQU8sS0FBUDtBQUNIO0FBQ0osS0FSRDtBQVNBdkIsS0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFheUMsT0FBYixDQUFxQiw0QkFBckI7QUFDQXpDLEtBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJ5QixLQUFuQixDQUF5QixZQUFVO0FBQy9CekIsT0FBQyxDQUFDLDZCQUFELENBQUQsQ0FBaUM0QixPQUFqQyxDQUF5QyxJQUF6QyxFQUErQ04sV0FBL0MsQ0FBMkQsUUFBM0Q7QUFDQXRCLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTBCLElBQVI7QUFDQSxhQUFPLEtBQVA7QUFDSCxLQUpEO0FBS0ExQixLQUFDLENBQUNZLE1BQUQsQ0FBRCxDQUFVQyxNQUFWLENBQWlCLFlBQVU7QUFDdkIsVUFBSWIsQ0FBQyxDQUFDWSxNQUFELENBQUQsQ0FBVVMsVUFBVixLQUF5QixHQUE3QixFQUFrQztBQUM5QnJCLFNBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYXNCLFdBQWIsQ0FBeUIsYUFBekI7QUFDQXRCLFNBQUMsQ0FBQyw2QkFBRCxDQUFELENBQWlDc0IsV0FBakMsQ0FBNkMsUUFBN0M7QUFDSDtBQUNKLEtBTEQ7QUFNSDs7QUFFRCxXQUFTZCxRQUFULEdBQW9CO0FBQ2hCLFFBQUlrQyxNQUFNLEdBQUcsSUFBSUMsTUFBSixDQUFXLGFBQVgsRUFBMEI7QUFDbkNDLGVBQVMsRUFBRSxZQUR3QjtBQUVuQ0MsbUJBQWEsRUFBRSxNQUZvQjtBQUduQ0Msb0JBQWMsRUFBRSxJQUhtQjtBQUluQ0MsY0FBUSxFQUFFLElBSnlCO0FBS25DQyxnQkFBVSxFQUFFLElBTHVCO0FBTW5DQyxpQkFBVyxFQUFFO0FBQ1QsYUFBTTtBQUNGSCx3QkFBYyxFQUFFO0FBRGQ7QUFERyxPQU5zQjtBQVduQ0ksUUFBRSxFQUFFO0FBQ0FDLG1CQUFXLEVBQUUsdUJBQVU7QUFDbkJuRCxXQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QjBCLElBQXhCLEdBQStCQyxFQUEvQixDQUFrQzNCLENBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCaUIsS0FBMUIsRUFBbEMsRUFBcUVZLElBQXJFO0FBQ0g7QUFIRDtBQVgrQixLQUExQixDQUFiO0FBaUJBN0IsS0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJ5QixLQUF2QixDQUE2QixZQUFVO0FBQ25DaUIsWUFBTSxDQUFDVSxPQUFQLENBQWVwRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQixLQUFSLEVBQWY7QUFDQWpCLE9BQUMsQ0FBQyxvQkFBRCxDQUFELENBQXdCMEIsSUFBeEIsR0FBK0JDLEVBQS9CLENBQWtDM0IsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUIsS0FBUixFQUFsQyxFQUFtRFksSUFBbkQ7QUFDQTdCLE9BQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXVCLFFBQVIsQ0FBaUIscUJBQWpCLEVBQXdDTyxRQUF4QyxHQUFtRFIsV0FBbkQsQ0FBK0QscUJBQS9EO0FBQ0gsS0FKRDtBQUtIOztBQUVELFdBQVNaLHVCQUFULEdBQW1DO0FBQy9CVixLQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QnlCLEtBQTVCLENBQWtDLFlBQVU7QUFDeEN6QixPQUFDLENBQUMsSUFBRCxDQUFELENBQVE0QixPQUFSLENBQWdCLElBQWhCLEVBQXNCTCxRQUF0QixDQUErQixRQUEvQixFQUF5Q08sUUFBekMsR0FBb0RSLFdBQXBELENBQWdFLFFBQWhFO0FBQ0F0QixPQUFDLENBQUMsVUFBRCxDQUFELENBQWNzQixXQUFkLENBQTBCLGFBQTFCO0FBQ0F0QixPQUFDLENBQUMsc0JBQUQsQ0FBRCxDQUEwQjBCLElBQTFCO0FBQ0ExQixPQUFDLENBQUMsd0JBQXNCQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxRCxJQUFSLENBQWEsVUFBYixDQUF2QixDQUFELENBQWtEQyxNQUFsRCxDQUF5RCxHQUF6RDtBQUNBdEQsT0FBQyxDQUFDWSxNQUFELENBQUQsQ0FBVVksT0FBVixDQUFrQixRQUFsQjtBQUNBLGFBQU8sS0FBUDtBQUNILEtBUEQ7QUFRQXhCLEtBQUMsQ0FBQyw2QkFBRCxDQUFELENBQWlDeUIsS0FBakMsQ0FBdUMsWUFBVTtBQUM3Q3pCLE9BQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCc0IsV0FBN0IsQ0FBeUMsUUFBekM7QUFDQXRCLE9BQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCc0QsTUFBMUIsQ0FBaUMsR0FBakM7QUFDQSxhQUFPLEtBQVA7QUFDSCxLQUpEO0FBS0g7QUFFSixDQXpKRDtBQTJKQzs7QUFDQSxDQUFDLFVBQVNDLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUM7O0FBQWEsV0FBU0MsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZUMsQ0FBZixFQUFpQkMsQ0FBakIsRUFBbUJDLENBQW5CLEVBQXFCO0FBQUMsYUFBU0MsQ0FBVCxHQUFZO0FBQUNDLE9BQUMsR0FBQ1IsQ0FBQyxDQUFDUyxnQkFBRixHQUFtQixDQUFyQixFQUF1QkwsQ0FBQyxHQUFDTSxDQUFDLENBQUNOLENBQUQsQ0FBMUIsRUFBOEJELENBQUMsQ0FBQ1EsS0FBRixJQUFTLENBQVQsSUFBWUMsVUFBVSxDQUFDLFlBQVU7QUFBQ0MsU0FBQyxDQUFDLENBQUMsQ0FBRixDQUFEO0FBQU0sT0FBbEIsRUFBbUJWLENBQUMsQ0FBQ1EsS0FBckIsQ0FBcEQsRUFBZ0YsQ0FBQ1IsQ0FBQyxDQUFDUSxLQUFGLEdBQVEsQ0FBUixJQUFXUixDQUFDLENBQUNXLFFBQWQsTUFBMEJULENBQUMsQ0FBQ0osQ0FBRixHQUFJYyxDQUFDLENBQUNaLENBQUMsQ0FBQ2EsUUFBSCxFQUFZLFVBQVNoQixDQUFULEVBQVc7QUFBQyxxQkFBV0EsQ0FBQyxDQUFDaUIsSUFBYixLQUFvQkMsQ0FBQyxHQUFDQyxDQUFDLEdBQUMsQ0FBQyxDQUF6QixHQUE0Qk4sQ0FBQyxDQUFDYixDQUFDLENBQUNvQixHQUFILENBQTdCO0FBQXFDLE9BQTdELENBQUwsRUFBb0VmLENBQUMsQ0FBQ0YsQ0FBRixHQUFJLFVBQVNILENBQVQsRUFBVztBQUFDQSxTQUFDLEdBQUNVLENBQUMsQ0FBQ1YsQ0FBRCxDQUFILEVBQU9JLENBQUMsQ0FBQ2lCLElBQUYsQ0FBT0MsS0FBUCxDQUFhbEIsQ0FBYixFQUFlSixDQUFmLENBQVA7QUFBeUIsT0FBN0csRUFBOEdLLENBQUMsQ0FBQ2tCLENBQUYsR0FBSSxZQUFVO0FBQUMsZUFBT25CLENBQUMsR0FBQ29CLENBQUMsQ0FBQ3BCLENBQUQsQ0FBRCxDQUFLcUIsTUFBTCxDQUFZLFlBQVU7QUFBQyxpQkFBTSxDQUFDRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVExQixJQUFSLENBQWFLLENBQUMsQ0FBQ3VCLFVBQWYsQ0FBUDtBQUFrQyxTQUF6RCxDQUFUO0FBQW9FLE9BQWpNLEVBQWtNckIsQ0FBQyxDQUFDRSxDQUFGLEdBQUksVUFBU1AsQ0FBVCxFQUFXO0FBQUMsYUFBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNELENBQUMsQ0FBQ25CLE1BQWhCLEVBQXVCb0IsQ0FBQyxFQUF4QixFQUEyQjtBQUFDLGNBQUlDLENBQUMsR0FBQ0UsQ0FBQyxDQUFDcUIsTUFBRixDQUFTLFlBQVU7QUFBQyxtQkFBTyxTQUFPekIsQ0FBQyxDQUFDQyxDQUFELENBQWY7QUFBbUIsV0FBdkMsQ0FBTjtBQUErQ0MsV0FBQyxDQUFDckIsTUFBRixJQUFVZ0MsQ0FBQyxDQUFDLENBQUMsQ0FBRixFQUFJWCxDQUFKLENBQVg7QUFBa0I7QUFBQyxPQUFoVCxFQUFpVFcsQ0FBQyxFQUFsVCxFQUFxVFcsQ0FBQyxDQUFDckIsQ0FBQyxDQUFDd0IsWUFBSCxDQUFELENBQWtCaEMsRUFBbEIsQ0FBcUIsWUFBVVcsQ0FBVixHQUFZLFVBQVosR0FBdUJBLENBQTVDLEVBQThDRCxDQUFDLENBQUNKLENBQWhELENBQS9VLENBQWhGO0FBQW1kOztBQUFBLGFBQVNTLENBQVQsQ0FBV1YsQ0FBWCxFQUFhO0FBQUMsVUFBSUksQ0FBQyxHQUFDRCxDQUFDLENBQUN5QixZQUFSO0FBQUEsVUFBcUJDLENBQUMsR0FBQzFCLENBQUMsQ0FBQzJCLFdBQXpCO0FBQUEsVUFBcUN6QixDQUFDLEdBQUNGLENBQUMsQ0FBQzRCLFNBQXpDO0FBQUEsVUFBbUR6QixDQUFDLEdBQUNILENBQUMsQ0FBQzZCLGVBQXZEO0FBQUEsVUFBdUV6QixDQUFDLEdBQUNKLENBQUMsQ0FBQzhCLGVBQTNFO0FBQUEsVUFBMkZ2QixDQUFDLEdBQUNQLENBQUMsQ0FBQytCLEVBQUYsSUFBTSxFQUFuRztBQUFzR2xDLE9BQUMsR0FBQ3dCLENBQUMsQ0FBQ3hCLENBQUQsQ0FBRCxDQUFLeUIsTUFBTCxDQUFZLFlBQVU7QUFBQyxZQUFJekIsQ0FBQyxHQUFDd0IsQ0FBQyxDQUFDLElBQUQsQ0FBUDtBQUFBLFlBQWN0QixDQUFDLEdBQUNpQyxDQUFDLENBQUMsSUFBRCxDQUFqQjtBQUF3QixlQUFNLENBQUNuQyxDQUFDLENBQUNGLElBQUYsQ0FBT0ssQ0FBQyxDQUFDaUMsV0FBVCxDQUFELEtBQXlCcEMsQ0FBQyxDQUFDcUMsSUFBRixDQUFPbEMsQ0FBQyxDQUFDbUMsU0FBVCxLQUFxQnRDLENBQUMsQ0FBQ3FDLElBQUYsQ0FBTy9CLENBQVAsQ0FBckIsSUFBZ0NOLENBQUMsQ0FBQ3FDLElBQUYsQ0FBTzlCLENBQVAsQ0FBaEMsSUFBMkNHLENBQUMsQ0FBQ1IsQ0FBRCxDQUFELEtBQU9ELENBQTNFLENBQU47QUFBb0YsT0FBbkksRUFBcUlILElBQXJJLENBQTBJLFlBQVVLLENBQUMsQ0FBQ29DLElBQXRKLEVBQTJKckMsQ0FBM0osQ0FBRjs7QUFBZ0ssV0FBSSxJQUFJVyxDQUFDLEdBQUMsQ0FBTixFQUFRMkIsQ0FBQyxHQUFDeEMsQ0FBQyxDQUFDbkIsTUFBaEIsRUFBdUJnQyxDQUFDLEdBQUMyQixDQUF6QixFQUEyQjNCLENBQUMsRUFBNUIsRUFBK0I7QUFBQyxZQUFJNEIsQ0FBQyxHQUFDakIsQ0FBQyxDQUFDeEIsQ0FBQyxDQUFDYSxDQUFELENBQUYsQ0FBUDtBQUFBLFlBQWNVLENBQUMsR0FBQ1ksQ0FBQyxDQUFDbkMsQ0FBQyxDQUFDYSxDQUFELENBQUYsQ0FBakI7QUFBQSxZQUF3QjZCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDSixJQUFGLENBQU9sQyxDQUFDLENBQUN3QyxrQkFBVCxLQUE4QnRDLENBQXhEO0FBQTBEa0IsU0FBQyxLQUFHcUIsQ0FBSixJQUFPRixDQUFQLElBQVVELENBQUMsQ0FBQ0osSUFBRixDQUFPL0IsQ0FBUCxDQUFWLElBQXFCbUMsQ0FBQyxDQUFDSixJQUFGLENBQU8vQixDQUFQLEVBQVN1QyxDQUFDLENBQUNKLENBQUMsQ0FBQ0osSUFBRixDQUFPL0IsQ0FBUCxDQUFELEVBQVdvQyxDQUFYLENBQVYsQ0FBckIsRUFBOENoQyxDQUFDLENBQUNhLENBQUQsQ0FBRCxLQUFPdEIsQ0FBUCxJQUFVd0MsQ0FBQyxDQUFDSixJQUFGLENBQU85QixDQUFQLENBQVYsSUFBcUJrQyxDQUFDLENBQUNKLElBQUYsQ0FBTzlCLENBQVAsRUFBU0csQ0FBQyxDQUFDYSxDQUFELENBQVYsQ0FBbkUsRUFBa0ZBLENBQUMsS0FBR3FCLENBQUosSUFBT3hDLENBQVAsSUFBVSxDQUFDcUMsQ0FBQyxDQUFDSixJQUFGLENBQU9TLENBQVAsQ0FBWCxHQUFxQkwsQ0FBQyxDQUFDSixJQUFGLENBQU9TLENBQVAsRUFBUzFDLENBQVQsQ0FBckIsR0FBaUNtQixDQUFDLEtBQUdxQixDQUFKLElBQU8sQ0FBQ2YsQ0FBUixJQUFXWSxDQUFDLENBQUM1RSxHQUFGLENBQU1rRixDQUFOLEtBQVUsV0FBU04sQ0FBQyxDQUFDNUUsR0FBRixDQUFNa0YsQ0FBTixDQUE5QixJQUF3Q04sQ0FBQyxDQUFDNUUsR0FBRixDQUFNa0YsQ0FBTixFQUFRLFVBQVFsQixDQUFSLEdBQVUsSUFBbEIsQ0FBM0o7QUFBbUw7O0FBQUEsYUFBTzdCLENBQVA7QUFBUzs7QUFBQSxhQUFTYSxDQUFULENBQVdiLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBRyxDQUFDRyxDQUFDLENBQUN2QixNQUFOLEVBQWEsT0FBTyxNQUFLc0IsQ0FBQyxDQUFDNkMsV0FBRixJQUFlOUMsQ0FBQyxDQUFDK0MsT0FBRixFQUFwQixDQUFQOztBQUF3QyxXQUFJLElBQUlwQixDQUFDLEdBQUM1QixDQUFDLElBQUVHLENBQVQsRUFBV0MsQ0FBQyxHQUFDLENBQUMsQ0FBZCxFQUFnQkMsQ0FBQyxHQUFDSCxDQUFDLENBQUM0QixTQUFGLElBQWEsRUFBL0IsRUFBa0N4QixDQUFDLEdBQUNKLENBQUMsQ0FBQzZCLGVBQXRDLEVBQXNEdEIsQ0FBQyxHQUFDUCxDQUFDLENBQUNpQyxXQUExRCxFQUFzRXZCLENBQUMsR0FBQyxDQUE1RSxFQUE4RUEsQ0FBQyxHQUFDZ0IsQ0FBQyxDQUFDaEQsTUFBbEYsRUFBeUZnQyxDQUFDLEVBQTFGO0FBQTZGLFlBQUdiLENBQUMsSUFBRUMsQ0FBSCxJQUFNd0MsQ0FBQyxDQUFDWixDQUFDLENBQUNoQixDQUFELENBQUYsQ0FBVixFQUFpQjtBQUFDLGNBQUlVLENBQUMsR0FBQ0MsQ0FBQyxDQUFDSyxDQUFDLENBQUNoQixDQUFELENBQUYsQ0FBUDtBQUFBLGNBQWM2QixDQUFDLEdBQUNQLENBQUMsQ0FBQ04sQ0FBQyxDQUFDaEIsQ0FBRCxDQUFGLENBQWpCO0FBQUEsY0FBd0JnQyxDQUFDLEdBQUN0QixDQUFDLENBQUNjLElBQUYsQ0FBT2xDLENBQUMsQ0FBQ21DLFNBQVQsQ0FBMUI7QUFBQSxjQUE4Q3ZCLENBQUMsR0FBQ1EsQ0FBQyxDQUFDYyxJQUFGLENBQU9sQyxDQUFDLENBQUN3QyxrQkFBVCxLQUE4QnJDLENBQTlFO0FBQUEsY0FBZ0Y0QyxDQUFDLEdBQUMzQixDQUFDLENBQUNjLElBQUYsQ0FBT2xDLENBQUMsQ0FBQzhCLGVBQVQsQ0FBbEY7QUFBNEdWLFdBQUMsQ0FBQ3pCLElBQUYsQ0FBT1ksQ0FBUCxLQUFXUCxDQUFDLENBQUNnRCxXQUFGLElBQWUsQ0FBQzVCLENBQUMsQ0FBQzZCLEVBQUYsQ0FBSyxVQUFMLENBQTNCLElBQTZDLEVBQUUsQ0FBQ1AsQ0FBQyxJQUFFdEIsQ0FBQyxDQUFDYyxJQUFGLENBQU85QixDQUFQLENBQUosTUFBaUJtQyxDQUFDLEtBQUdFLENBQUosS0FBUTdCLENBQUMsR0FBQzhCLENBQUYsS0FBTXRCLENBQUMsQ0FBQ2MsSUFBRixDQUFPUyxDQUFQLENBQU4sSUFBaUJ2QixDQUFDLENBQUNjLElBQUYsQ0FBTzlCLENBQVAsTUFBWWdCLENBQUMsQ0FBQ2MsSUFBRixDQUFPZ0IsQ0FBUCxDQUFyQyxLQUFpRFgsQ0FBQyxLQUFHRSxDQUFKLElBQU83QixDQUFDLEdBQUM4QixDQUFGLEtBQU10QixDQUFDLENBQUMxRCxHQUFGLENBQU1rRixDQUFOLENBQS9FLEtBQTBGRyxDQUE1RixDQUE3QyxLQUE4STdDLENBQUMsR0FBQyxDQUFDLENBQUgsRUFBS2tCLENBQUMsQ0FBQ3pCLElBQUYsQ0FBT1ksQ0FBUCxFQUFTLENBQUMsQ0FBVixDQUFMLEVBQWtCOEIsQ0FBQyxDQUFDakIsQ0FBRCxFQUFHbUIsQ0FBSCxFQUFLM0IsQ0FBTCxFQUFPbUMsQ0FBUCxDQUFqSztBQUE0SztBQUF2WTs7QUFBdVk3QyxPQUFDLEtBQUdELENBQUMsR0FBQ29CLENBQUMsQ0FBQ3BCLENBQUQsQ0FBRCxDQUFLcUIsTUFBTCxDQUFZLFlBQVU7QUFBQyxlQUFNLENBQUNELENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTFCLElBQVIsQ0FBYVksQ0FBYixDQUFQO0FBQXVCLE9BQTlDLENBQUwsQ0FBRDtBQUF1RDs7QUFBQSxhQUFTOEIsQ0FBVCxDQUFXeEMsQ0FBWCxFQUFhQyxDQUFiLEVBQWVDLENBQWYsRUFBaUJFLENBQWpCLEVBQW1CO0FBQUMsUUFBRWtELENBQUY7O0FBQUksVUFBSXpCLEVBQUMsR0FBQyxhQUFVO0FBQUMwQixTQUFDLENBQUMsU0FBRCxFQUFXdkQsQ0FBWCxDQUFELEVBQWVrRCxDQUFDLEVBQWhCLEVBQW1CckIsRUFBQyxHQUFDTCxDQUFDLENBQUNnQyxJQUF2QjtBQUE0QixPQUE3Qzs7QUFBOENELE9BQUMsQ0FBQyxZQUFELEVBQWN2RCxDQUFkLENBQUQ7QUFBa0IsVUFBSUssQ0FBQyxHQUFDRixDQUFDLENBQUNtQyxTQUFSO0FBQUEsVUFBa0JoQyxDQUFDLEdBQUNILENBQUMsQ0FBQzZCLGVBQXRCO0FBQUEsVUFBc0N6QixDQUFDLEdBQUNKLENBQUMsQ0FBQ3NELGNBQTFDO0FBQUEsVUFBeUQvQyxDQUFDLEdBQUNQLENBQUMsQ0FBQ3VELGVBQTdEO0FBQUEsVUFBNkU3QyxDQUFDLEdBQUNWLENBQUMsQ0FBQ3dELGVBQWpGO0FBQUEsVUFBaUduQixDQUFDLEdBQUNyQyxDQUFDLENBQUN1QixVQUFyRztBQUFBLFVBQWdIZSxDQUFDLEdBQUN6QyxDQUFDLENBQUNxQyxJQUFGLENBQU8zQixDQUFQLENBQWxIOztBQUE0SCxVQUFHTixDQUFILEVBQUs7QUFBQyxZQUFJbUIsRUFBQyxHQUFDLGFBQVU7QUFBQ1YsV0FBQyxJQUFFYixDQUFDLENBQUN4QyxVQUFGLENBQWEyQyxDQUFDLENBQUM4QixlQUFmLENBQUgsRUFBbUNqQyxDQUFDLENBQUNGLElBQUYsQ0FBTzBDLENBQVAsRUFBUyxDQUFDLENBQVYsQ0FBbkMsRUFBZ0RlLENBQUMsQ0FBQ0ssQ0FBRCxFQUFHNUQsQ0FBSCxDQUFqRCxFQUF1RFksVUFBVSxDQUFDc0MsQ0FBRCxFQUFHLENBQUgsQ0FBakUsRUFBdUUzQixFQUFDLEdBQUNDLENBQUMsQ0FBQ2dDLElBQTNFO0FBQWdGLFNBQWpHOztBQUFrR3hELFNBQUMsQ0FBQzZELEdBQUYsQ0FBTUMsQ0FBTixFQUFTQyxHQUFULENBQWFELENBQWIsRUFBZWpDLEVBQWYsRUFBa0JrQyxHQUFsQixDQUFzQkMsQ0FBdEIsRUFBd0J6QyxFQUF4QixHQUEyQmdDLENBQUMsQ0FBQ25ELENBQUQsRUFBR0osQ0FBSCxFQUFLLFVBQVNDLENBQVQsRUFBVztBQUFDQSxXQUFDLElBQUVELENBQUMsQ0FBQzZELEdBQUYsQ0FBTUcsQ0FBTixHQUFTekMsRUFBQyxFQUFaLEtBQWlCdkIsQ0FBQyxDQUFDNkQsR0FBRixDQUFNQyxDQUFOLEdBQVNqQyxFQUFDLEVBQTNCLENBQUQ7QUFBZ0MsU0FBakQsQ0FBRCxJQUFxRDdCLENBQUMsQ0FBQy9CLE9BQUYsQ0FBVTZGLENBQVYsQ0FBaEY7QUFBNkYsT0FBck0sTUFBeU07QUFBQyxZQUFJcEIsQ0FBQyxHQUFDbEIsQ0FBQyxDQUFDLElBQUl5QyxLQUFKLEVBQUQsQ0FBUDtBQUFtQnZCLFNBQUMsQ0FBQ3FCLEdBQUYsQ0FBTUQsQ0FBTixFQUFRakMsRUFBUixFQUFXa0MsR0FBWCxDQUFlQyxDQUFmLEVBQWlCLFlBQVU7QUFBQ2hFLFdBQUMsQ0FBQzdCLElBQUYsSUFBUzhCLENBQUMsS0FBRzJDLENBQUosR0FBTTVDLENBQUMsQ0FBQ3FDLElBQUYsQ0FBTzZCLENBQVAsRUFBU3hCLENBQUMsQ0FBQ0wsSUFBRixDQUFPNkIsQ0FBUCxDQUFULEVBQW9CN0IsSUFBcEIsQ0FBeUJnQixDQUF6QixFQUEyQlgsQ0FBQyxDQUFDTCxJQUFGLENBQU9nQixDQUFQLENBQTNCLEVBQXNDaEIsSUFBdEMsQ0FBMkNTLENBQTNDLEVBQTZDSixDQUFDLENBQUNMLElBQUYsQ0FBT1MsQ0FBUCxDQUE3QyxDQUFOLEdBQThEOUMsQ0FBQyxDQUFDbkMsR0FBRixDQUFNa0YsQ0FBTixFQUFRLFVBQVFMLENBQUMsQ0FBQ0wsSUFBRixDQUFPUyxDQUFQLENBQVIsR0FBa0IsSUFBMUIsQ0FBdkUsRUFBdUc5QyxDQUFDLENBQUNHLENBQUMsQ0FBQ2dFLE1BQUgsQ0FBRCxDQUFZaEUsQ0FBQyxDQUFDaUUsVUFBZCxDQUF2RyxFQUFpSXZELENBQUMsS0FBR2IsQ0FBQyxDQUFDeEMsVUFBRixDQUFhNkMsQ0FBQyxHQUFDLEdBQUYsR0FBTUMsQ0FBTixHQUFRLEdBQVIsR0FBWUksQ0FBWixHQUFjLEdBQWQsR0FBa0JQLENBQUMsQ0FBQ3dDLGtCQUFqQyxHQUFxRHBDLENBQUMsS0FBRzJELENBQUosSUFBT2xFLENBQUMsQ0FBQ3hDLFVBQUYsQ0FBYStDLENBQWIsQ0FBL0QsQ0FBbEksRUFBa05QLENBQUMsQ0FBQ0YsSUFBRixDQUFPMEMsQ0FBUCxFQUFTLENBQUMsQ0FBVixDQUFsTixFQUErTmUsQ0FBQyxDQUFDSyxDQUFELEVBQUc1RCxDQUFILENBQWhPLEVBQXNPMEMsQ0FBQyxDQUFDMkIsTUFBRixFQUF0TyxFQUFpUG5CLENBQUMsRUFBbFA7QUFBcVAsU0FBalI7QUFBbVIsWUFBSWYsQ0FBQyxHQUFDLENBQUMzQixDQUFDLElBQUVpQyxDQUFILEdBQUtBLENBQUwsR0FBT3pDLENBQUMsQ0FBQ3FDLElBQUYsQ0FBT2hDLENBQVAsQ0FBUixLQUFvQixFQUExQjtBQUE2QnFDLFNBQUMsQ0FBQ0wsSUFBRixDQUFPNkIsQ0FBUCxFQUFTbEUsQ0FBQyxDQUFDcUMsSUFBRixDQUFPOUIsQ0FBUCxDQUFULEVBQW9COEIsSUFBcEIsQ0FBeUJnQixDQUF6QixFQUEyQnJELENBQUMsQ0FBQ3FDLElBQUYsQ0FBTy9CLENBQVAsQ0FBM0IsRUFBc0MrQixJQUF0QyxDQUEyQ1MsQ0FBM0MsRUFBNkNYLENBQUMsR0FBQ2pDLENBQUMsR0FBQ2lDLENBQUgsR0FBSyxJQUFuRCxHQUF5RE8sQ0FBQyxDQUFDNEIsUUFBRixJQUFZNUIsQ0FBQyxDQUFDekUsT0FBRixDQUFVK0YsQ0FBVixDQUFyRTtBQUFrRjtBQUFDOztBQUFBLGFBQVN2QixDQUFULENBQVd6QyxDQUFYLEVBQWE7QUFBQyxVQUFJQyxDQUFDLEdBQUNELENBQUMsQ0FBQ3VFLHFCQUFGLEVBQU47QUFBQSxVQUFnQ3JFLENBQUMsR0FBQ0MsQ0FBQyxDQUFDcUUsZUFBcEM7QUFBQSxVQUFvRGhELENBQUMsR0FBQ3JCLENBQUMsQ0FBQ3NFLFNBQXhEO0FBQUEsVUFBa0VyRSxDQUFDLEdBQUNzQyxDQUFDLEtBQUdsQixDQUFKLEdBQU12QixDQUFDLENBQUNsQixHQUFSLElBQWEsQ0FBQ3lDLENBQUQsR0FBR3ZCLENBQUMsQ0FBQ3lFLE1BQXRGO0FBQUEsVUFBNkY3QyxDQUFDLEdBQUNOLENBQUMsS0FBR0MsQ0FBSixHQUFNdkIsQ0FBQyxDQUFDMEUsSUFBUixJQUFjLENBQUNuRCxDQUFELEdBQUd2QixDQUFDLENBQUMyRSxLQUFsSDtBQUF3SCxhQUFNLGVBQWExRSxDQUFiLEdBQWVFLENBQWYsR0FBaUIsaUJBQWVGLENBQWYsR0FBaUIyQixDQUFqQixHQUFtQnpCLENBQUMsSUFBRXlCLENBQTdDO0FBQStDOztBQUFBLGFBQVNOLENBQVQsR0FBWTtBQUFDLGFBQU9MLENBQUMsSUFBRSxDQUFILEdBQUtBLENBQUwsR0FBT0EsQ0FBQyxHQUFDTSxDQUFDLENBQUN4QixDQUFELENBQUQsQ0FBSzZFLEtBQUwsRUFBaEI7QUFBNkI7O0FBQUEsYUFBU25DLENBQVQsR0FBWTtBQUFDLGFBQU92QixDQUFDLElBQUUsQ0FBSCxHQUFLQSxDQUFMLEdBQU9BLENBQUMsR0FBQ0ssQ0FBQyxDQUFDeEIsQ0FBRCxDQUFELENBQUtyQyxNQUFMLEVBQWhCO0FBQThCOztBQUFBLGFBQVN3RSxDQUFULENBQVduQyxDQUFYLEVBQWE7QUFBQyxhQUFPQSxDQUFDLENBQUM4RSxPQUFGLENBQVVDLFdBQVYsRUFBUDtBQUErQjs7QUFBQSxhQUFTbEMsQ0FBVCxDQUFXN0MsQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQyxVQUFHQSxDQUFILEVBQUs7QUFBQyxZQUFJQyxDQUFDLEdBQUNGLENBQUMsQ0FBQ2dGLEtBQUYsQ0FBUSxHQUFSLENBQU47QUFBbUJoRixTQUFDLEdBQUMsRUFBRjs7QUFBSyxhQUFJLElBQUlHLENBQUMsR0FBQyxDQUFOLEVBQVFxQixDQUFDLEdBQUN0QixDQUFDLENBQUNyQixNQUFoQixFQUF1QnNCLENBQUMsR0FBQ3FCLENBQXpCLEVBQTJCckIsQ0FBQyxFQUE1QjtBQUErQkgsV0FBQyxJQUFFQyxDQUFDLEdBQUNDLENBQUMsQ0FBQ0MsQ0FBRCxDQUFELENBQUs4RSxJQUFMLEVBQUYsSUFBZTlFLENBQUMsS0FBR3FCLENBQUMsR0FBQyxDQUFOLEdBQVEsR0FBUixHQUFZLEVBQTNCLENBQUg7QUFBL0I7QUFBaUU7O0FBQUEsYUFBT3hCLENBQVA7QUFBUzs7QUFBQSxhQUFTZSxDQUFULENBQVdmLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBSXVCLENBQUo7QUFBQSxVQUFNcEIsQ0FBQyxHQUFDLENBQVI7QUFBVSxhQUFPLFVBQVN5QixDQUFULEVBQVd4QixDQUFYLEVBQWE7QUFBQyxpQkFBU0MsQ0FBVCxHQUFZO0FBQUNGLFdBQUMsR0FBQyxDQUFDLElBQUk4RSxJQUFKLEVBQUgsRUFBWWpGLENBQUMsQ0FBQ2tGLElBQUYsQ0FBT2pGLENBQVAsRUFBUzJCLENBQVQsQ0FBWjtBQUF3Qjs7QUFBQSxZQUFJdEIsQ0FBQyxHQUFDLENBQUMsSUFBSTJFLElBQUosRUFBRCxHQUFVOUUsQ0FBaEI7QUFBa0JvQixTQUFDLElBQUU0RCxZQUFZLENBQUM1RCxDQUFELENBQWYsRUFBbUJqQixDQUFDLEdBQUNQLENBQUYsSUFBSyxDQUFDRyxDQUFDLENBQUNrRixjQUFSLElBQXdCaEYsQ0FBeEIsR0FBMEJDLENBQUMsRUFBM0IsR0FBOEJrQixDQUFDLEdBQUNaLFVBQVUsQ0FBQ04sQ0FBRCxFQUFHTixDQUFDLEdBQUNPLENBQUwsQ0FBN0Q7QUFBcUUsT0FBako7QUFBa0o7O0FBQUEsYUFBUzJDLENBQVQsR0FBWTtBQUFDLFFBQUVJLENBQUYsRUFBSWxELENBQUMsQ0FBQ3ZCLE1BQUYsSUFBVXlFLENBQVYsSUFBYUMsQ0FBQyxDQUFDLGVBQUQsQ0FBbEI7QUFBb0M7O0FBQUEsYUFBU0EsQ0FBVCxDQUFXdkQsQ0FBWCxFQUFhQyxDQUFiLEVBQWV1QixDQUFmLEVBQWlCO0FBQUMsYUFBTSxDQUFDLEVBQUV4QixDQUFDLEdBQUNHLENBQUMsQ0FBQ0gsQ0FBRCxDQUFMLENBQUQsS0FBYUEsQ0FBQyxDQUFDc0IsS0FBRixDQUFRcEIsQ0FBUixFQUFVLEdBQUdvRixLQUFILENBQVNILElBQVQsQ0FBY0ksU0FBZCxFQUF3QixDQUF4QixDQUFWLEdBQXNDLENBQUMsQ0FBcEQsQ0FBTjtBQUE2RDs7QUFBQSxRQUFJakMsQ0FBQyxHQUFDLENBQU47QUFBQSxRQUFRcEMsQ0FBQyxHQUFDLENBQUMsQ0FBWDtBQUFBLFFBQWFDLENBQUMsR0FBQyxDQUFDLENBQWhCO0FBQUEsUUFBa0JYLENBQUMsR0FBQyxDQUFDLENBQXJCO0FBQUEsUUFBdUJvRCxDQUFDLEdBQUMsV0FBekI7QUFBQSxRQUFxQ0ksQ0FBQyxHQUFDLE1BQXZDO0FBQUEsUUFBOENGLENBQUMsR0FBQyxPQUFoRDtBQUFBLFFBQXdEbEIsQ0FBQyxHQUFDLEtBQTFEO0FBQUEsUUFBZ0VFLENBQUMsR0FBQyxLQUFsRTtBQUFBLFFBQXdFTyxDQUFDLEdBQUMsUUFBMUU7QUFBQSxRQUFtRmEsQ0FBQyxHQUFDLE9BQXJGO0FBQUEsUUFBNkZuQixDQUFDLEdBQUMsa0JBQS9GO0FBQWtILGdCQUFVNUMsQ0FBQyxDQUFDcUYsSUFBWixJQUFrQjNELENBQWxCLEdBQW9CdEIsQ0FBQyxFQUFyQixHQUF3QmlCLENBQUMsQ0FBQ3hCLENBQUQsQ0FBRCxDQUFLTCxFQUFMLENBQVFxRSxDQUFDLEdBQUMsR0FBRixHQUFNMUQsQ0FBZCxFQUFnQkMsQ0FBaEIsQ0FBeEI7QUFBMkM7O0FBQUEsV0FBU0osQ0FBVCxDQUFXQSxDQUFYLEVBQWEwQixDQUFiLEVBQWU7QUFBQyxRQUFJeEIsQ0FBQyxHQUFDLElBQU47QUFBQSxRQUFXQyxDQUFDLEdBQUNrQixDQUFDLENBQUNpRSxNQUFGLENBQVMsRUFBVCxFQUFZcEYsQ0FBQyxDQUFDcUYsTUFBZCxFQUFxQjdELENBQXJCLENBQWI7QUFBQSxRQUFxQ3RCLENBQUMsR0FBQyxFQUF2QztBQUFBLFFBQTBDRyxDQUFDLEdBQUNKLENBQUMsQ0FBQ2lDLElBQUYsR0FBTyxHQUFQLEdBQVksRUFBRW5DLENBQTFEO0FBQTRELFdBQU9DLENBQUMsQ0FBQ3FGLE1BQUYsR0FBUyxVQUFTMUYsQ0FBVCxFQUFXRSxDQUFYLEVBQWE7QUFBQyxhQUFPQSxDQUFDLEtBQUdELENBQUosR0FBTUssQ0FBQyxDQUFDTixDQUFELENBQVAsSUFBWU0sQ0FBQyxDQUFDTixDQUFELENBQUQsR0FBS0UsQ0FBTCxFQUFPRyxDQUFuQixDQUFQO0FBQTZCLEtBQXBELEVBQXFEQSxDQUFDLENBQUNzRixRQUFGLEdBQVcsVUFBUzNGLENBQVQsRUFBVztBQUFDLGFBQU9PLENBQUMsQ0FBQ0osQ0FBRixJQUFLSSxDQUFDLENBQUNKLENBQUYsQ0FBSSxhQUFXcUIsQ0FBQyxDQUFDUCxJQUFGLENBQU9qQixDQUFQLENBQVgsR0FBcUJ3QixDQUFDLENBQUN4QixDQUFELENBQXRCLEdBQTBCQSxDQUE5QixDQUFMLEVBQXNDSyxDQUE3QztBQUErQyxLQUEzSCxFQUE0SEEsQ0FBQyxDQUFDdUYsUUFBRixHQUFXLFlBQVU7QUFBQyxhQUFPckYsQ0FBQyxDQUFDZ0IsQ0FBRixHQUFJaEIsQ0FBQyxDQUFDZ0IsQ0FBRixFQUFKLEdBQVUsRUFBakI7QUFBb0IsS0FBdEssRUFBdUtsQixDQUFDLENBQUN3RixNQUFGLEdBQVMsVUFBUzdGLENBQVQsRUFBVztBQUFDLGFBQU9PLENBQUMsQ0FBQ04sQ0FBRixJQUFLTSxDQUFDLENBQUNOLENBQUYsQ0FBSSxFQUFKLEVBQU8sQ0FBQ0QsQ0FBUixDQUFMLEVBQWdCSyxDQUF2QjtBQUF5QixLQUFyTixFQUFzTkEsQ0FBQyxDQUFDeUYsS0FBRixHQUFRLFVBQVM5RixDQUFULEVBQVc7QUFBQyxhQUFPTyxDQUFDLENBQUNBLENBQUYsSUFBS0EsQ0FBQyxDQUFDQSxDQUFGLENBQUksYUFBV2lCLENBQUMsQ0FBQ1AsSUFBRixDQUFPakIsQ0FBUCxDQUFYLEdBQXFCd0IsQ0FBQyxDQUFDeEIsQ0FBRCxDQUF0QixHQUEwQkEsQ0FBOUIsQ0FBTCxFQUFzQ0ssQ0FBN0M7QUFBK0MsS0FBelIsRUFBMFJBLENBQUMsQ0FBQzBGLE9BQUYsR0FBVSxZQUFVO0FBQUMsYUFBT3hGLENBQUMsQ0FBQ04sQ0FBRixJQUFLTSxDQUFDLENBQUNOLENBQUYsQ0FBSTtBQUFDbUIsV0FBRyxFQUFDLENBQUM7QUFBTixPQUFKLEVBQWEsQ0FBQyxDQUFkLENBQUwsRUFBc0JmLENBQTdCO0FBQStCLEtBQTlVLEVBQStVQSxDQUFDLENBQUM0QyxPQUFGLEdBQVUsWUFBVTtBQUFDLGFBQU96QixDQUFDLENBQUNsQixDQUFDLENBQUNxQixZQUFILENBQUQsQ0FBa0JrQyxHQUFsQixDQUFzQixNQUFJbkQsQ0FBMUIsRUFBNEJILENBQUMsQ0FBQ04sQ0FBOUIsR0FBaUN1QixDQUFDLENBQUN4QixDQUFELENBQUQsQ0FBSzZELEdBQUwsQ0FBUyxNQUFJbkQsQ0FBYixDQUFqQyxFQUFpREgsQ0FBQyxHQUFDLEVBQW5ELEVBQXNETixDQUE3RDtBQUErRCxLQUFuYSxFQUFvYUMsQ0FBQyxDQUFDRyxDQUFELEVBQUdDLENBQUgsRUFBS0gsQ0FBTCxFQUFPSSxDQUFQLEVBQVNHLENBQVQsQ0FBcmEsRUFBaWJKLENBQUMsQ0FBQzBGLFNBQUYsR0FBWTdGLENBQVosR0FBY0UsQ0FBdGM7QUFBd2M7O0FBQUEsTUFBSW1CLENBQUMsR0FBQ3hCLENBQUMsQ0FBQ2lHLE1BQUYsSUFBVWpHLENBQUMsQ0FBQ2tHLEtBQWxCO0FBQUEsTUFBd0I5RixDQUFDLEdBQUMsQ0FBMUI7QUFBQSxNQUE0QnlCLENBQUMsR0FBQyxDQUFDLENBQS9CO0FBQWlDTCxHQUFDLENBQUMyRSxFQUFGLENBQUsvSSxJQUFMLEdBQVVvRSxDQUFDLENBQUMyRSxFQUFGLENBQUtDLElBQUwsR0FBVSxVQUFTcEcsQ0FBVCxFQUFXO0FBQUMsV0FBTyxJQUFJRyxDQUFKLENBQU0sSUFBTixFQUFXSCxDQUFYLENBQVA7QUFBcUIsR0FBckQsRUFBc0R3QixDQUFDLENBQUNwRSxJQUFGLEdBQU9vRSxDQUFDLENBQUM0RSxJQUFGLEdBQU8sVUFBU3BHLENBQVQsRUFBV0UsQ0FBWCxFQUFhRSxDQUFiLEVBQWU7QUFBQyxRQUFHb0IsQ0FBQyxDQUFDNkUsVUFBRixDQUFhbkcsQ0FBYixNQUFrQkUsQ0FBQyxHQUFDRixDQUFGLEVBQUlBLENBQUMsR0FBQyxFQUF4QixHQUE0QnNCLENBQUMsQ0FBQzZFLFVBQUYsQ0FBYWpHLENBQWIsQ0FBL0IsRUFBK0M7QUFBQ0osT0FBQyxHQUFDd0IsQ0FBQyxDQUFDOEUsT0FBRixDQUFVdEcsQ0FBVixJQUFhQSxDQUFiLEdBQWUsQ0FBQ0EsQ0FBRCxDQUFqQixFQUFxQkUsQ0FBQyxHQUFDc0IsQ0FBQyxDQUFDOEUsT0FBRixDQUFVcEcsQ0FBVixJQUFhQSxDQUFiLEdBQWUsQ0FBQ0EsQ0FBRCxDQUF0Qzs7QUFBMEMsV0FBSSxJQUFJMkIsQ0FBQyxHQUFDMUIsQ0FBQyxDQUFDb0csU0FBRixDQUFZYixNQUFsQixFQUF5QnJGLENBQUMsR0FBQ3dCLENBQUMsQ0FBQ0ssRUFBRixLQUFPTCxDQUFDLENBQUNLLEVBQUYsR0FBSyxFQUFaLENBQTNCLEVBQTJDNUIsQ0FBQyxHQUFDLENBQTdDLEVBQStDQyxDQUFDLEdBQUNQLENBQUMsQ0FBQ25CLE1BQXZELEVBQThEeUIsQ0FBQyxHQUFDQyxDQUFoRSxFQUFrRUQsQ0FBQyxFQUFuRTtBQUFzRSxTQUFDdUIsQ0FBQyxDQUFDN0IsQ0FBQyxDQUFDTSxDQUFELENBQUYsQ0FBRCxLQUFVTCxDQUFWLElBQWF1QixDQUFDLENBQUM2RSxVQUFGLENBQWF4RSxDQUFDLENBQUM3QixDQUFDLENBQUNNLENBQUQsQ0FBRixDQUFkLENBQWQsTUFBdUN1QixDQUFDLENBQUM3QixDQUFDLENBQUNNLENBQUQsQ0FBRixDQUFELEdBQVFGLENBQS9DO0FBQXRFOztBQUF3SCxXQUFJLElBQUlNLENBQUMsR0FBQyxDQUFOLEVBQVFHLENBQUMsR0FBQ1gsQ0FBQyxDQUFDckIsTUFBaEIsRUFBdUI2QixDQUFDLEdBQUNHLENBQXpCLEVBQTJCSCxDQUFDLEVBQTVCO0FBQStCTCxTQUFDLENBQUNILENBQUMsQ0FBQ1EsQ0FBRCxDQUFGLENBQUQsR0FBUVYsQ0FBQyxDQUFDLENBQUQsQ0FBVDtBQUEvQjtBQUE0QztBQUFDLEdBQW5WLEVBQW9WRyxDQUFDLENBQUNvRyxTQUFGLENBQVliLE1BQVosR0FBbUI7QUFBQ25ELFFBQUksRUFBQyxNQUFOO0FBQWF5RCxhQUFTLEVBQUMsQ0FBQyxDQUF4QjtBQUEwQmhELGVBQVcsRUFBQyxDQUFDLENBQXZDO0FBQXlDd0MsUUFBSSxFQUFDLE1BQTlDO0FBQXFEZixhQUFTLEVBQUMsR0FBL0Q7QUFBbUV0QixlQUFXLEVBQUMsQ0FBQyxDQUFoRjtBQUFrRnhCLGdCQUFZLEVBQUMzQixDQUEvRjtBQUFpR3dFLG1CQUFlLEVBQUMsTUFBakg7QUFBd0h6QyxhQUFTLEVBQUMsSUFBbEk7QUFBdUlILGdCQUFZLEVBQUMsb0ZBQXBKO0FBQXlPRSxlQUFXLEVBQUMsSUFBclA7QUFBMFBuQixTQUFLLEVBQUMsQ0FBQyxDQUFqUTtBQUFtUUcsWUFBUSxFQUFDLENBQUMsQ0FBN1E7QUFBK1F3QixhQUFTLEVBQUMsVUFBelI7QUFBb1NOLG1CQUFlLEVBQUMsYUFBcFQ7QUFBa1V5QixrQkFBYyxFQUFDLFlBQWpWO0FBQThWQyxtQkFBZSxFQUFDLGFBQTlXO0FBQTRYekIsbUJBQWUsRUFBQyxhQUE1WTtBQUEwWlUsc0JBQWtCLEVBQUMsZ0JBQTdhO0FBQThiZ0IsbUJBQWUsRUFBQyxDQUFDLENBQS9jO0FBQWlkdkIsZUFBVyxFQUFDLFNBQTdkO0FBQXVlVixjQUFVLEVBQUMsUUFBbGY7QUFBMmZ5QyxVQUFNLEVBQUMsTUFBbGdCO0FBQXlnQkMsY0FBVSxFQUFDLENBQXBoQjtBQUFzaEJpQixrQkFBYyxFQUFDLENBQUMsQ0FBdGlCO0FBQXdpQnJFLFlBQVEsRUFBQyxHQUFqakI7QUFBcWpCd0YsY0FBVSxFQUFDdkcsQ0FBaGtCO0FBQWtrQndHLGFBQVMsRUFBQ3hHLENBQTVrQjtBQUE4a0J5RyxXQUFPLEVBQUN6RyxDQUF0bEI7QUFBd2xCMEcsaUJBQWEsRUFBQzFHO0FBQXRtQixHQUF2VyxFQUFnOUJ1QixDQUFDLENBQUN4QixDQUFELENBQUQsQ0FBS0wsRUFBTCxDQUFRLE1BQVIsRUFBZSxZQUFVO0FBQUNrQyxLQUFDLEdBQUMsQ0FBQyxDQUFIO0FBQUssR0FBL0IsQ0FBaDlCO0FBQWkvQixDQUFoeEosQ0FBaXhKeEUsTUFBanhKLENBQUQ7QUFFRDs7QUFDQSxDQUFDLFVBQVMyQyxDQUFULEVBQVc7QUFBQ0EsR0FBQyxDQUFDb0csSUFBRixDQUFPLENBQUMsSUFBRCxFQUFNLE9BQU4sRUFBYyxPQUFkLENBQVAsRUFBOEIsQ0FBQyxPQUFELEVBQVMsT0FBVCxDQUE5QixFQUFnRCxVQUFTakcsQ0FBVCxFQUFXRixDQUFYLEVBQWE7QUFBQyxRQUFJQyxDQUFDLEdBQUNDLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBSzJFLE9BQUwsQ0FBYUMsV0FBYixFQUFOOztBQUFpQyxRQUFHLFlBQVU3RSxDQUFWLElBQWEsWUFBVUEsQ0FBMUIsRUFBNEI7QUFBQyxVQUFJMkIsQ0FBQyxHQUFDMUIsQ0FBQyxDQUFDeUcsSUFBRixDQUFPLFVBQVAsQ0FBTjtBQUFBLFVBQXlCeEcsQ0FBQyxHQUFDRCxDQUFDLENBQUN5RyxJQUFGLENBQU8sWUFBUCxDQUEzQjtBQUFBLFVBQWdEcEYsQ0FBQyxHQUFDLENBQWxEO0FBQUEsVUFBb0RkLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQyxVQUFFYyxDQUFGLEtBQU1LLENBQUMsQ0FBQ2hELE1BQVIsSUFBZ0JvQixDQUFDLENBQUMsQ0FBQyxDQUFGLENBQWpCO0FBQXNCLE9BQXZGO0FBQUEsVUFBd0Z1QyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxHQUFVO0FBQUMsWUFBSXJDLENBQUMsR0FBQ0gsQ0FBQyxDQUFDLElBQUQsQ0FBUDtBQUFBLFlBQWNDLENBQUMsR0FBQ0UsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLMkUsT0FBTCxDQUFhQyxXQUFiLEVBQWhCO0FBQUEsWUFBMkM3RSxDQUFDLEdBQUNDLENBQUMsQ0FBQzBHLElBQUYsQ0FBTyxZQUFQLENBQTdDO0FBQUEsWUFBa0VoRixDQUFDLEdBQUM3QixDQUFDLENBQUMsZUFBYUMsQ0FBYixHQUFlLFVBQWYsR0FBMEIsU0FBM0IsQ0FBckU7QUFBMkcsdUJBQWFBLENBQWIsSUFBZ0I0QixDQUFDLENBQUNrQyxHQUFGLENBQU0sT0FBTixFQUFjckQsQ0FBZCxDQUFoQixFQUFpQ1YsQ0FBQyxDQUFDekMsSUFBRixDQUFPMkMsQ0FBUCxFQUFTLFVBQVNGLENBQVQsRUFBV0csQ0FBWCxFQUFhO0FBQUMwQixXQUFDLENBQUNRLElBQUYsQ0FBT2xDLENBQUMsQ0FBQ29DLElBQVQsRUFBY3BDLENBQUMsQ0FBQzJHLEtBQWhCO0FBQXVCLFNBQTlDLENBQWpDLEVBQWlGM0csQ0FBQyxDQUFDNEcsV0FBRixDQUFjbEYsQ0FBZCxDQUFqRjtBQUFrRyxPQUFsVDs7QUFBbVQxQixPQUFDLENBQUM0RCxHQUFGLENBQU0sZ0JBQU4sRUFBdUIsWUFBVTtBQUFDOUQsU0FBQyxDQUFDLENBQUMsQ0FBRixDQUFEO0FBQU0sT0FBeEMsRUFBMEM0RCxHQUExQyxDQUE4QyxZQUE5QyxFQUE0RHhCLElBQTVELENBQWlFLFFBQWpFLEVBQTBFbEMsQ0FBQyxDQUFDa0MsSUFBRixDQUFPLGFBQVAsQ0FBMUUsR0FBaUdSLENBQUMsQ0FBQ2hELE1BQUYsR0FBU2dELENBQUMsQ0FBQ3RFLElBQUYsQ0FBT2lGLENBQVAsQ0FBVCxHQUFtQnJDLENBQUMsQ0FBQ2tDLElBQUYsQ0FBTyxVQUFQLEtBQW9CckMsQ0FBQyxDQUFDekMsSUFBRixDQUFPNEMsQ0FBQyxDQUFDa0MsSUFBRixDQUFPLFVBQVAsRUFBbUIyQyxLQUFuQixDQUF5QixHQUF6QixDQUFQLEVBQXFDLFVBQVMvRSxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFlBQUkyQixDQUFDLEdBQUMzQixDQUFDLENBQUM4RSxLQUFGLENBQVEsR0FBUixDQUFOO0FBQW1CN0UsU0FBQyxDQUFDNkcsTUFBRixDQUFTaEgsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjK0QsR0FBZCxDQUFrQixPQUFsQixFQUEwQnJELENBQTFCLEVBQTZCMkIsSUFBN0IsQ0FBa0M7QUFBQzRFLGFBQUcsRUFBQ3BGLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBS29ELElBQUwsRUFBTDtBQUFpQmhFLGNBQUksRUFBQ1ksQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLb0QsSUFBTDtBQUF0QixTQUFsQyxDQUFUO0FBQWdGLE9BQXRKLEdBQXdKLEtBQUtTLE1BQUwsQ0FBWSxpQkFBWixLQUFnQ3ZGLENBQUMsQ0FBQzNDLFVBQUYsQ0FBYSxVQUFiLENBQTVNLElBQXNPeUMsQ0FBQyxDQUFDLENBQUMsQ0FBRixDQUEzVixFQUFnV0csQ0FBQyxDQUFDdkIsTUFBRixJQUFVdUIsQ0FBQyxDQUFDN0MsSUFBRixDQUFPaUYsQ0FBUCxDQUExVztBQUFvWCxLQUFwc0IsTUFBeXNCdkMsQ0FBQyxDQUFDLENBQUMsQ0FBRixDQUFEO0FBQU0sR0FBOXlCO0FBQWd6QixDQUE1ekIsQ0FBNnpCNUMsTUFBTSxDQUFDNEksTUFBUCxJQUFlNUksTUFBTSxDQUFDNkksS0FBbjFCLENBQUQ7QUFFQTs7QUFDQSxDQUFDLFVBQVNsRyxDQUFULEVBQVc7QUFBQyxXQUFTQyxDQUFULENBQVdBLENBQVgsRUFBYUUsQ0FBYixFQUFlcUIsQ0FBZixFQUFpQjtBQUFDLFFBQUlLLENBQUMsR0FBQzVCLENBQUMsQ0FBQzRHLElBQUYsQ0FBTyxZQUFQLENBQU47QUFBQSxRQUEyQm5HLENBQUMsR0FBQ1YsQ0FBQyxDQUFDLE1BQUlHLENBQUosR0FBTSxHQUFQLENBQTlCO0FBQTBDLFdBQU9ILENBQUMsQ0FBQ3pDLElBQUYsQ0FBT3NFLENBQVAsRUFBUyxVQUFTN0IsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxtQkFBV0EsQ0FBQyxDQUFDc0MsSUFBYixJQUFtQnRDLENBQUMsQ0FBQ3NDLElBQUYsS0FBU25DLENBQTVCLEtBQWdDSCxDQUFDLENBQUM2RyxLQUFGLEdBQVE1RyxDQUFDLENBQUNELENBQUMsQ0FBQzZHLEtBQUgsRUFBU3RGLENBQVQsQ0FBekMsR0FBc0RkLENBQUMsQ0FBQzJCLElBQUYsQ0FBT3BDLENBQUMsQ0FBQ3NDLElBQVQsRUFBY3RDLENBQUMsQ0FBQzZHLEtBQWhCLENBQXREO0FBQTZFLEtBQXBHLEdBQXNHN0csQ0FBQyxDQUFDOEcsV0FBRixDQUFjckcsQ0FBZCxDQUF0RyxFQUF1SEEsQ0FBOUg7QUFBZ0k7O0FBQUEsV0FBU1AsQ0FBVCxDQUFXRixDQUFYLEVBQWFFLENBQWIsRUFBZUQsQ0FBZixFQUFpQjtBQUFDLFFBQUlFLENBQUMsR0FBQ0osQ0FBQyxDQUFDLE9BQUQsQ0FBRCxDQUFXK0QsR0FBWCxDQUFlLE1BQWYsRUFBc0IsWUFBVTtBQUFDN0QsT0FBQyxDQUFDLENBQUMsQ0FBRixDQUFEO0FBQU0sS0FBdkMsRUFBeUM2RCxHQUF6QyxDQUE2QyxPQUE3QyxFQUFxRCxZQUFVO0FBQUM3RCxPQUFDLENBQUMsQ0FBQyxDQUFGLENBQUQ7QUFBTSxLQUF0RSxFQUF3RWdILFFBQXhFLENBQWlGakgsQ0FBakYsRUFBb0ZvQyxJQUFwRixDQUF5RixLQUF6RixFQUErRmxDLENBQS9GLENBQU47QUFBd0dDLEtBQUMsQ0FBQ2tFLFFBQUYsSUFBWWxFLENBQUMsQ0FBQytHLElBQUYsRUFBWjtBQUFxQjs7QUFBQSxXQUFTakgsQ0FBVCxDQUFXRixDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFFBQUdBLENBQUgsRUFBSztBQUFDLFVBQUlFLENBQUMsR0FBQ0gsQ0FBQyxDQUFDZ0YsS0FBRixDQUFRLEdBQVIsQ0FBTjtBQUFtQmhGLE9BQUMsR0FBQyxFQUFGOztBQUFLLFdBQUksSUFBSUUsQ0FBQyxHQUFDLENBQU4sRUFBUUUsQ0FBQyxHQUFDRCxDQUFDLENBQUN0QixNQUFoQixFQUF1QnFCLENBQUMsR0FBQ0UsQ0FBekIsRUFBMkJGLENBQUMsRUFBNUI7QUFBK0JGLFNBQUMsSUFBRUMsQ0FBQyxHQUFDRSxDQUFDLENBQUNELENBQUQsQ0FBRCxDQUFLK0UsSUFBTCxFQUFGLElBQWUvRSxDQUFDLEtBQUdFLENBQUMsR0FBQyxDQUFOLEdBQVEsR0FBUixHQUFZLEVBQTNCLENBQUg7QUFBL0I7QUFBaUU7O0FBQUEsV0FBT0osQ0FBUDtBQUFTOztBQUFBLE1BQUlJLENBQUMsR0FBQyxVQUFOO0FBQWlCSixHQUFDLENBQUNvRyxJQUFGLENBQU8sQ0FBQyxLQUFELEVBQU8sU0FBUCxDQUFQLEVBQXlCLENBQUMsU0FBRCxDQUF6QixFQUFxQyxVQUFTNUUsQ0FBVCxFQUFXSyxDQUFYLEVBQWE7QUFBQyxRQUFHLGNBQVlMLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBS3NELE9BQUwsQ0FBYUMsV0FBYixFQUFmLEVBQTBDO0FBQUMsVUFBSXJFLENBQUMsR0FBQ2MsQ0FBQyxDQUFDb0YsSUFBRixDQUFPeEcsQ0FBUCxDQUFOO0FBQUEsVUFBZ0JTLENBQUMsR0FBQ1csQ0FBQyxDQUFDb0YsSUFBRixDQUFPLFVBQVAsQ0FBbEI7QUFBQSxVQUFxQ3ZHLENBQUMsR0FBQyxLQUFLcUYsTUFBTCxDQUFZLFdBQVosS0FBMEIsRUFBakU7QUFBb0VoRixPQUFDLENBQUM3QixNQUFGLElBQVU2QixDQUFDLENBQUNuRCxJQUFGLENBQU8sWUFBVTtBQUFDMEMsU0FBQyxDQUFDRCxDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVMsUUFBVCxFQUFrQkssQ0FBbEIsQ0FBRDtBQUFzQixPQUF4QyxHQUEwQyxNQUFJUSxDQUFDLENBQUNoQyxNQUFOLElBQWNnQyxDQUFDLEdBQUNaLENBQUMsQ0FBQ1ksQ0FBRCxFQUFHLEtBQUgsRUFBU1IsQ0FBVCxDQUFILEVBQWVRLENBQUMsQ0FBQ2xCLEVBQUYsQ0FBSyxNQUFMLEVBQVksWUFBVTtBQUFDa0MsU0FBQyxDQUFDLENBQUMsQ0FBRixDQUFEO0FBQU0sT0FBN0IsRUFBK0JsQyxFQUEvQixDQUFrQyxPQUFsQyxFQUEwQyxZQUFVO0FBQUNrQyxTQUFDLENBQUMsQ0FBQyxDQUFGLENBQUQ7QUFBTSxPQUEzRCxDQUFmLEVBQTRFaEIsQ0FBQyxDQUFDd0IsSUFBRixDQUFPLEtBQVAsRUFBYXhCLENBQUMsQ0FBQ3dCLElBQUYsQ0FBT2pDLENBQVAsQ0FBYixDQUE1RSxFQUFvRyxLQUFLc0YsTUFBTCxDQUFZLGlCQUFaLEtBQWdDN0UsQ0FBQyxDQUFDckQsVUFBRixDQUFhNEMsQ0FBYixDQUFsSixJQUFtS29CLENBQUMsQ0FBQ2EsSUFBRixDQUFPakMsQ0FBUCxLQUFXRCxDQUFDLENBQUNxQixDQUFELEVBQUduQixDQUFDLEdBQUNtQixDQUFDLENBQUNhLElBQUYsQ0FBT2pDLENBQVAsQ0FBTCxFQUFleUIsQ0FBZixDQUFELEVBQW1CLEtBQUs2RCxNQUFMLENBQVksaUJBQVosS0FBZ0NsRSxDQUFDLENBQUNoRSxVQUFGLENBQWE0QyxDQUFiLENBQTlELElBQStFeUIsQ0FBQyxDQUFDLENBQUMsQ0FBRixDQUF2UyxJQUE2U0wsQ0FBQyxDQUFDYSxJQUFGLENBQU8sYUFBUCxLQUF1QnJDLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY3FDLElBQWQsQ0FBbUI7QUFBQytFLGFBQUssRUFBQzVGLENBQUMsQ0FBQ2EsSUFBRixDQUFPLFlBQVAsQ0FBUDtBQUE0QmdGLGFBQUssRUFBQzdGLENBQUMsQ0FBQ2EsSUFBRixDQUFPLFlBQVAsQ0FBbEM7QUFBdURwQixZQUFJLEVBQUNPLENBQUMsQ0FBQ2EsSUFBRixDQUFPLFdBQVAsQ0FBNUQ7QUFBZ0ZpRixjQUFNLEVBQUNwSCxDQUFDLENBQUNzQixDQUFDLENBQUNhLElBQUYsQ0FBTyxhQUFQLENBQUQsRUFBdUJoQyxDQUF2QjtBQUF4RixPQUFuQixFQUF1STZHLFFBQXZJLENBQWdKMUYsQ0FBaEosR0FBbUpyQixDQUFDLENBQUNxQixDQUFELEVBQUduQixDQUFDLEdBQUNtQixDQUFDLENBQUNhLElBQUYsQ0FBT2pDLENBQVAsQ0FBTCxFQUFleUIsQ0FBZixDQUFwSixFQUFzSyxLQUFLNkQsTUFBTCxDQUFZLGlCQUFaLEtBQWdDbEUsQ0FBQyxDQUFDaEUsVUFBRixDQUFhNEMsQ0FBQyxHQUFDLDhDQUFmLENBQTdOLElBQTZSeUIsQ0FBQyxDQUFDLENBQUMsQ0FBRixDQUEza0I7QUFBZ2xCLEtBQS9yQixNQUFvc0JBLENBQUMsQ0FBQyxDQUFDLENBQUYsQ0FBRDtBQUFNLEdBQTd2QjtBQUErdkIsQ0FBL3RDLENBQWd1Q3hFLE1BQU0sQ0FBQzRJLE1BQVAsSUFBZTVJLE1BQU0sQ0FBQzZJLEtBQXR2QyxDQUFELEMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiXG4vKlxuICogV2VsY29tZSB0byB5b3VyIGFwcCdzIG1haW4gSmF2YVNjcmlwdCBmaWxlIVxuICpcbiAqIFdlIHJlY29tbWVuZCBpbmNsdWRpbmcgdGhlIGJ1aWx0IHZlcnNpb24gb2YgdGhpcyBKYXZhU2NyaXB0IGZpbGVcbiAqIChhbmQgaXRzIENTUyBmaWxlKSBpbiB5b3VyIGJhc2UgbGF5b3V0IChiYXNlLmh0bWwudHdpZykuXG4gKi9cblxuLy8gYW55IENTUyB5b3UgcmVxdWlyZSB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbnJlcXVpcmUoJy4uL2Nzcy9hcHAuc2NzcycpO1xuXG4vLyBOZWVkIGpRdWVyeT8gSW5zdGFsbCBpdCB3aXRoIFwieWFybiBhZGQganF1ZXJ5XCIsIHRoZW4gdW5jb21tZW50IHRvIHJlcXVpcmUgaXQuXG4vL2NvbnN0ICQgPSByZXF1aXJlKCdqcXVlcnknKTtcblxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xuICAgaW5pdFByaWNlVGFibGUgKCk7ICAgLy8gbWF0Y2ggaGVpZ2h0IG9uIHByaWNlIHRhYmxlXG4gICBpbml0RnVxTGlzdCAoKTsgIC8vIHNsaWRlRG93biBmdXEgYmxvY2sgb24gY2xpY2tcbiAgIGluaXRTY3JvbGwgKCk7ICAgLy8gYWRkaW5nIGJvcmRlciBvbiBoZWFkZXIgYnkgc2Nyb2xsXG4gICBpbml0Q2xvc2VQb3B1cCgpOyAgICAvLyBjbG9zaW5nIGNvb2tpZSBwb3B1cFxuICAgaW5pdE1vYmlsZU1lbnUgKCk7IC8vIG1vYmlsZSBtZW51XG4gICBpbml0VGFicyAoKTsgLy8gdGFicyBhbmQgZ2FsbGVyeSBzd3lwZXJcbiAgIGluaXRMYXp5TG9hZCAoKTsgLy8gbGF6eSBsb2FkIHBsdWdpblxuICAgaW5pdEludGVncmF0aW9uU3dpdGNoZXIgKCk7XG5cbiAgIGZ1bmN0aW9uIGluaXRMYXp5TG9hZCAoKSB7XG4gICAgICAgICQoJ2ltZywgdmlkZW8sIHBpY3R1cmUnKS5MYXp5KCk7IFxuICAgfVxuXG4gICBmdW5jdGlvbiBpbml0UHJpY2VUYWJsZSAoKXtcbiAgICAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKCcucHJpY2UtdGFibGUgLmNvbHVtbnMnKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgJCgnLnJvdycsJCh0aGlzKSkucmVtb3ZlQXR0cignc3R5bGUnKTtcbiAgICAgICAgICAgICAgICB2YXIgJHJvd3MgPSBbXTtcbiAgICAgICAgICAgICAgICAkKCcuY29sJywkKHRoaXMpKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICQoJy5yb3cnLCQodGhpcykpLmVhY2goZnVuY3Rpb24oaW5kZXgpe1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQodGhpcykuaGVpZ2h0KCkgPiAkcm93c1tpbmRleF0gfHwgJHJvd3NbaW5kZXhdID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb3dzW2luZGV4XSA9ICQodGhpcykuaGVpZ2h0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICQoJy5jb2wnLCQodGhpcykpLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgJCgnLnJvdycsJCh0aGlzKSkuZWFjaChmdW5jdGlvbihpbmRleCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmNzcygnaGVpZ2h0Jywkcm93c1tpbmRleF0pO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDgwMCkge1xuICAgICAgICAgICAgICAgICQoJy5wcmljZS10YWJsZSAuY29sdW1ucyAuY29sJykucmVtb3ZlQXR0cignc3R5bGUnKTtcbiAgICAgICAgICAgICAgICAkKCcudGFibGUtbmF2IC5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgJCgnLnRhYmxlLW5hdiBsaTpmaXJzdCcpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICB9KS50cmlnZ2VyKCdyZXNpemUnKTtcbiAgICAgICAkKCcudGFibGUtbmF2IHNwYW4nKS5jbGljayhmdW5jdGlvbigpe1xuICAgICAgICAgICAkKCcucHJpY2UtdGFibGUgLmNvbHVtbnMgLmNvbCcpLmhpZGUoKS5lcSgkKHRoaXMpLmNsb3Nlc3QoJ2xpJykuaW5kZXgoKSsxKS5zaG93KCk7XG4gICAgICAgICAgICQoJy5wcmljZS10YWJsZSAuY29sdW1ucy5uby1ib3JkZXJzIC5jb2wnKS5oaWRlKCkuZXEoJCh0aGlzKS5jbG9zZXN0KCdsaScpLmluZGV4KCkrMSkuc2hvdygpO1xuICAgICAgICAgICAkKCcucHJpY2UtdGFibGUgLmNvbHVtbnMubm8tYm9yZGVycyAuY29sOmZpcnN0Jykuc2hvdygpO1xuICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJ2xpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgfSlcbiAgIH1cblxuICAgZnVuY3Rpb24gaW5pdEZ1cUxpc3QgKCl7XG4gICAgICAgJCgnLmZ1cS1saXN0IC5oZWFkJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgICAgJCh0aGlzKS5uZXh0KCcudGV4dCcpLnNsaWRlVG9nZ2xlKDMwMCwgZnVuY3Rpb24oKXskKHRoaXMpLmNsb3Nlc3QoJy5pdGVtJykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpfSk7XG4gICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICB9KVxuICAgfVxuXG4gICBmdW5jdGlvbiBpbml0U2Nyb2xsICgpe1xuICAgICAgICAkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBpZiAoJCh3aW5kb3cpLnNjcm9sbFRvcCgpID4gMCkge1xuICAgICAgICAgICAgICAgICQoJyNoZWFkZXInKS5hZGRDbGFzcygnc2Nyb2xsJyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQoJyNoZWFkZXInKS5yZW1vdmVDbGFzcygnc2Nyb2xsJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoJCgnLnByaWNlLXRhYmxlJykubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCQod2luZG93KS5zY3JvbGxUb3AoKSArIDIwMCA+ICQoJy5wcmljZS10YWJsZScpLm9mZnNldCgpLnRvcCkge1xuICAgICAgICAgICAgICAgICAgICAkKCcucHJpY2UtdGFibGUnKS5hZGRDbGFzcygnc2Nyb2xsJylcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkKCcucHJpY2UtdGFibGUnKS5yZW1vdmVDbGFzcygnc2Nyb2xsJylcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdENsb3NlUG9wdXAgKCl7XG4gICAgICAgICQoJy5jb29raWUtcG9wdXAgLmNsb3NlJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICQodGhpcykuY2xvc2VzdCgnLmNvb2tpZS1wb3B1cCcpLmZhZGVPdXQoMzAwKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbml0TW9iaWxlTWVudSAoKXtcbiAgICAgICAgJCgnI2hlYWRlciAubWItYnVyZ2VyJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICQoJyNoZWFkZXInKS50b2dnbGVDbGFzcygnbWItb3Blbi1uYXYnKTtcbiAgICAgICAgICAgICQoJyNuYXZpZ2F0aW9uIC5tZW51IC5oYXMtZHJvcCcpLmNsb3Nlc3QoJ2xpJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgJCgnI2hlYWRlciAuYmFjaycpLmhpZGUoKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICAgICQoJy5zaWRlYmFyIC5tYi1idXJnZXIsIC5vcGVuLWNhdGVnb3JpZXMtbWVudScpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKCcuc2lkZWJhcicpLnRvZ2dsZUNsYXNzKCdtYi1vcGVuLW5hdicpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgICAgJCgnI25hdmlnYXRpb24gLmhhcy1kcm9wJykuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGlmICgkKHdpbmRvdykuaW5uZXJXaWR0aCgpIDw9IDc2OCkge1xuICAgICAgICAgICAgICAgICQoJyNoZWFkZXIgLmJhY2snKVxuICAgICAgICAgICAgICAgICAgICAudGV4dCgkKHRoaXMpXG4gICAgICAgICAgICAgICAgICAgIC50ZXh0KCkpLnNob3coKTtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJ2xpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgJCgnI2hlYWRlcicpLnByZXBlbmQoJzxzcGFuIGNsYXNzPVwiYmFja1wiPjwvc3Bhbj4nKTtcbiAgICAgICAgJCgnI2hlYWRlciAuYmFjaycpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKCcjbmF2aWdhdGlvbiAubWVudSAuaGFzLWRyb3AnKS5jbG9zZXN0KCdsaScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICQodGhpcykuaGlkZSgpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KVxuICAgICAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBpZiAoJCh3aW5kb3cpLmlubmVyV2lkdGgoKSA+IDc2OCkge1xuICAgICAgICAgICAgICAgICQoJyNoZWFkZXInKS5yZW1vdmVDbGFzcygnbWItb3Blbi1uYXYnKTtcbiAgICAgICAgICAgICAgICAkKCcjbmF2aWdhdGlvbiAubWVudSBsaS5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluaXRUYWJzICgpe1xuICAgICAgICB2YXIgc3dpcGVyID0gbmV3IFN3aXBlcignLnRhYmNvbnRyb2wnLCB7XG4gICAgICAgICAgICBkaXJlY3Rpb246ICdob3Jpem9udGFsJyxcbiAgICAgICAgICAgIHNsaWRlc1BlclZpZXc6ICdhdXRvJyxcbiAgICAgICAgICAgIGNlbnRlcmVkU2xpZGVzOiB0cnVlLFxuICAgICAgICAgICAgZnJlZU1vZGU6IHRydWUsXG4gICAgICAgICAgICBtb3VzZXdoZWVsOiB0cnVlLFxuICAgICAgICAgICAgYnJlYWtwb2ludHM6IHtcbiAgICAgICAgICAgICAgICA3NjggOiB7XG4gICAgICAgICAgICAgICAgICAgIGNlbnRlcmVkU2xpZGVzOiBmYWxzZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIHNsaWRlQ2hhbmdlOiBmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAkKCcudGFic2V0IC50YWJzIC50YWInKS5oaWRlKCkuZXEoJCgnLnN3aXBlci1zbGlkZS1hY3RpdmUnKS5pbmRleCgpKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgJCgnLnRhYmNvbnRyb2wgLml0ZW0nKS5jbGljayhmdW5jdGlvbigpe1xuICAgICAgICAgICAgc3dpcGVyLnNsaWRlVG8oJCh0aGlzKS5pbmRleCgpKTtcbiAgICAgICAgICAgICQoJy50YWJzZXQgLnRhYnMgLnRhYicpLmhpZGUoKS5lcSgkKHRoaXMpLmluZGV4KCkpLnNob3coKTtcbiAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ3N3aXBlci1zbGlkZS1hY3RpdmUnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdzd2lwZXItc2xpZGUtYWN0aXZlJylcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdEludGVncmF0aW9uU3dpdGNoZXIgKCl7XG4gICAgICAgICQoJy5jYXRlZ29yaWVzLXN3aXRjaGVyIGEnKS5jbGljayhmdW5jdGlvbigpe1xuICAgICAgICAgICAgJCh0aGlzKS5jbG9zZXN0KCdsaScpLmFkZENsYXNzKCdhY3RpdmUnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICQoJy5zaWRlYmFyJykucmVtb3ZlQ2xhc3MoJ21iLW9wZW4tbmF2Jyk7XG4gICAgICAgICAgICAkKCcuaW50ZWdyYXRpb24tbGlzdCBsaScpLmhpZGUoKVxuICAgICAgICAgICAgJCgnLmludGVncmF0aW9uLWxpc3QgLicrJCh0aGlzKS5kYXRhKCdjYXRlZ29yeScpKS5mYWRlSW4oMzAwKTtcbiAgICAgICAgICAgICQod2luZG93KS50cmlnZ2VyKCdzY3JvbGwnKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICAgICQoJy5zaWRlYmFyIC5saW5rLWhvbGRlciAubW9yZScpLmNsaWNrKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKCcuY2F0ZWdvcmllcy1zd2l0Y2hlciBsaScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICQoJy5pbnRlZ3JhdGlvbi1saXN0IGxpJykuZmFkZUluKDMwMCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pXG4gICAgfVxuICAgICBcbn0pO1xuXG4gLyohIGpRdWVyeSAmIFplcHRvIExhenkgdjEuNy4xMCAtIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5IC0gTUlUJkdQTC0yLjAgbGljZW5zZSAtIENvcHlyaWdodCAyMDEyLTIwMTggRGFuaWVsICdFaXNiZWhyJyBLZXJuICovXG4gIWZ1bmN0aW9uKHQsZSl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gcihyLGEsaSx1LGwpe2Z1bmN0aW9uIGYoKXtMPXQuZGV2aWNlUGl4ZWxSYXRpbz4xLGk9YyhpKSxhLmRlbGF5Pj0wJiZzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7cyghMCl9LGEuZGVsYXkpLChhLmRlbGF5PDB8fGEuY29tYmluZWQpJiYodS5lPXYoYS50aHJvdHRsZSxmdW5jdGlvbih0KXtcInJlc2l6ZVwiPT09dC50eXBlJiYodz1CPS0xKSxzKHQuYWxsKX0pLHUuYT1mdW5jdGlvbih0KXt0PWModCksaS5wdXNoLmFwcGx5KGksdCl9LHUuZz1mdW5jdGlvbigpe3JldHVybiBpPW4oaSkuZmlsdGVyKGZ1bmN0aW9uKCl7cmV0dXJuIW4odGhpcykuZGF0YShhLmxvYWRlZE5hbWUpfSl9LHUuZj1mdW5jdGlvbih0KXtmb3IodmFyIGU9MDtlPHQubGVuZ3RoO2UrKyl7dmFyIHI9aS5maWx0ZXIoZnVuY3Rpb24oKXtyZXR1cm4gdGhpcz09PXRbZV19KTtyLmxlbmd0aCYmcyghMSxyKX19LHMoKSxuKGEuYXBwZW5kU2Nyb2xsKS5vbihcInNjcm9sbC5cIitsK1wiIHJlc2l6ZS5cIitsLHUuZSkpfWZ1bmN0aW9uIGModCl7dmFyIGk9YS5kZWZhdWx0SW1hZ2Usbz1hLnBsYWNlaG9sZGVyLHU9YS5pbWFnZUJhc2UsbD1hLnNyY3NldEF0dHJpYnV0ZSxmPWEubG9hZGVyQXR0cmlidXRlLGM9YS5fZnx8e307dD1uKHQpLmZpbHRlcihmdW5jdGlvbigpe3ZhciB0PW4odGhpcykscj1tKHRoaXMpO3JldHVybiF0LmRhdGEoYS5oYW5kbGVkTmFtZSkmJih0LmF0dHIoYS5hdHRyaWJ1dGUpfHx0LmF0dHIobCl8fHQuYXR0cihmKXx8Y1tyXSE9PWUpfSkuZGF0YShcInBsdWdpbl9cIithLm5hbWUscik7Zm9yKHZhciBzPTAsZD10Lmxlbmd0aDtzPGQ7cysrKXt2YXIgQT1uKHRbc10pLGc9bSh0W3NdKSxoPUEuYXR0cihhLmltYWdlQmFzZUF0dHJpYnV0ZSl8fHU7Zz09PU4mJmgmJkEuYXR0cihsKSYmQS5hdHRyKGwsYihBLmF0dHIobCksaCkpLGNbZ109PT1lfHxBLmF0dHIoZil8fEEuYXR0cihmLGNbZ10pLGc9PT1OJiZpJiYhQS5hdHRyKEUpP0EuYXR0cihFLGkpOmc9PT1OfHwhb3x8QS5jc3MoTykmJlwibm9uZVwiIT09QS5jc3MoTyl8fEEuY3NzKE8sXCJ1cmwoJ1wiK28rXCInKVwiKX1yZXR1cm4gdH1mdW5jdGlvbiBzKHQsZSl7aWYoIWkubGVuZ3RoKXJldHVybiB2b2lkKGEuYXV0b0Rlc3Ryb3kmJnIuZGVzdHJveSgpKTtmb3IodmFyIG89ZXx8aSx1PSExLGw9YS5pbWFnZUJhc2V8fFwiXCIsZj1hLnNyY3NldEF0dHJpYnV0ZSxjPWEuaGFuZGxlZE5hbWUscz0wO3M8by5sZW5ndGg7cysrKWlmKHR8fGV8fEEob1tzXSkpe3ZhciBnPW4ob1tzXSksaD1tKG9bc10pLGI9Zy5hdHRyKGEuYXR0cmlidXRlKSx2PWcuYXR0cihhLmltYWdlQmFzZUF0dHJpYnV0ZSl8fGwscD1nLmF0dHIoYS5sb2FkZXJBdHRyaWJ1dGUpO2cuZGF0YShjKXx8YS52aXNpYmxlT25seSYmIWcuaXMoXCI6dmlzaWJsZVwiKXx8ISgoYnx8Zy5hdHRyKGYpKSYmKGg9PT1OJiYoditiIT09Zy5hdHRyKEUpfHxnLmF0dHIoZikhPT1nLmF0dHIoRikpfHxoIT09TiYmditiIT09Zy5jc3MoTykpfHxwKXx8KHU9ITAsZy5kYXRhKGMsITApLGQoZyxoLHYscCkpfXUmJihpPW4oaSkuZmlsdGVyKGZ1bmN0aW9uKCl7cmV0dXJuIW4odGhpcykuZGF0YShjKX0pKX1mdW5jdGlvbiBkKHQsZSxyLGkpeysrejt2YXIgbz1mdW5jdGlvbigpe3koXCJvbkVycm9yXCIsdCkscCgpLG89bi5ub29wfTt5KFwiYmVmb3JlTG9hZFwiLHQpO3ZhciB1PWEuYXR0cmlidXRlLGw9YS5zcmNzZXRBdHRyaWJ1dGUsZj1hLnNpemVzQXR0cmlidXRlLGM9YS5yZXRpbmFBdHRyaWJ1dGUscz1hLnJlbW92ZUF0dHJpYnV0ZSxkPWEubG9hZGVkTmFtZSxBPXQuYXR0cihjKTtpZihpKXt2YXIgZz1mdW5jdGlvbigpe3MmJnQucmVtb3ZlQXR0cihhLmxvYWRlckF0dHJpYnV0ZSksdC5kYXRhKGQsITApLHkoVCx0KSxzZXRUaW1lb3V0KHAsMSksZz1uLm5vb3B9O3Qub2ZmKEkpLm9uZShJLG8pLm9uZShELGcpLHkoaSx0LGZ1bmN0aW9uKGUpe2U/KHQub2ZmKEQpLGcoKSk6KHQub2ZmKEkpLG8oKSl9KXx8dC50cmlnZ2VyKEkpfWVsc2V7dmFyIGg9bihuZXcgSW1hZ2UpO2gub25lKEksbykub25lKEQsZnVuY3Rpb24oKXt0LmhpZGUoKSxlPT09Tj90LmF0dHIoQyxoLmF0dHIoQykpLmF0dHIoRixoLmF0dHIoRikpLmF0dHIoRSxoLmF0dHIoRSkpOnQuY3NzKE8sXCJ1cmwoJ1wiK2guYXR0cihFKStcIicpXCIpLHRbYS5lZmZlY3RdKGEuZWZmZWN0VGltZSkscyYmKHQucmVtb3ZlQXR0cih1K1wiIFwiK2wrXCIgXCIrYytcIiBcIithLmltYWdlQmFzZUF0dHJpYnV0ZSksZiE9PUMmJnQucmVtb3ZlQXR0cihmKSksdC5kYXRhKGQsITApLHkoVCx0KSxoLnJlbW92ZSgpLHAoKX0pO3ZhciBtPShMJiZBP0E6dC5hdHRyKHUpKXx8XCJcIjtoLmF0dHIoQyx0LmF0dHIoZikpLmF0dHIoRix0LmF0dHIobCkpLmF0dHIoRSxtP3IrbTpudWxsKSxoLmNvbXBsZXRlJiZoLnRyaWdnZXIoRCl9fWZ1bmN0aW9uIEEodCl7dmFyIGU9dC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSxyPWEuc2Nyb2xsRGlyZWN0aW9uLG49YS50aHJlc2hvbGQsaT1oKCkrbj5lLnRvcCYmLW48ZS5ib3R0b20sbz1nKCkrbj5lLmxlZnQmJi1uPGUucmlnaHQ7cmV0dXJuXCJ2ZXJ0aWNhbFwiPT09cj9pOlwiaG9yaXpvbnRhbFwiPT09cj9vOmkmJm99ZnVuY3Rpb24gZygpe3JldHVybiB3Pj0wP3c6dz1uKHQpLndpZHRoKCl9ZnVuY3Rpb24gaCgpe3JldHVybiBCPj0wP0I6Qj1uKHQpLmhlaWdodCgpfWZ1bmN0aW9uIG0odCl7cmV0dXJuIHQudGFnTmFtZS50b0xvd2VyQ2FzZSgpfWZ1bmN0aW9uIGIodCxlKXtpZihlKXt2YXIgcj10LnNwbGl0KFwiLFwiKTt0PVwiXCI7Zm9yKHZhciBhPTAsbj1yLmxlbmd0aDthPG47YSsrKXQrPWUrclthXS50cmltKCkrKGEhPT1uLTE/XCIsXCI6XCJcIil9cmV0dXJuIHR9ZnVuY3Rpb24gdih0LGUpe3ZhciBuLGk9MDtyZXR1cm4gZnVuY3Rpb24obyx1KXtmdW5jdGlvbiBsKCl7aT0rbmV3IERhdGUsZS5jYWxsKHIsbyl9dmFyIGY9K25ldyBEYXRlLWk7biYmY2xlYXJUaW1lb3V0KG4pLGY+dHx8IWEuZW5hYmxlVGhyb3R0bGV8fHU/bCgpOm49c2V0VGltZW91dChsLHQtZil9fWZ1bmN0aW9uIHAoKXstLXosaS5sZW5ndGh8fHp8fHkoXCJvbkZpbmlzaGVkQWxsXCIpfWZ1bmN0aW9uIHkodCxlLG4pe3JldHVybiEhKHQ9YVt0XSkmJih0LmFwcGx5KHIsW10uc2xpY2UuY2FsbChhcmd1bWVudHMsMSkpLCEwKX12YXIgej0wLHc9LTEsQj0tMSxMPSExLFQ9XCJhZnRlckxvYWRcIixEPVwibG9hZFwiLEk9XCJlcnJvclwiLE49XCJpbWdcIixFPVwic3JjXCIsRj1cInNyY3NldFwiLEM9XCJzaXplc1wiLE89XCJiYWNrZ3JvdW5kLWltYWdlXCI7XCJldmVudFwiPT09YS5iaW5kfHxvP2YoKTpuKHQpLm9uKEQrXCIuXCIrbCxmKX1mdW5jdGlvbiBhKGEsbyl7dmFyIHU9dGhpcyxsPW4uZXh0ZW5kKHt9LHUuY29uZmlnLG8pLGY9e30sYz1sLm5hbWUrXCItXCIrICsraTtyZXR1cm4gdS5jb25maWc9ZnVuY3Rpb24odCxyKXtyZXR1cm4gcj09PWU/bFt0XToobFt0XT1yLHUpfSx1LmFkZEl0ZW1zPWZ1bmN0aW9uKHQpe3JldHVybiBmLmEmJmYuYShcInN0cmluZ1wiPT09bi50eXBlKHQpP24odCk6dCksdX0sdS5nZXRJdGVtcz1mdW5jdGlvbigpe3JldHVybiBmLmc/Zi5nKCk6e319LHUudXBkYXRlPWZ1bmN0aW9uKHQpe3JldHVybiBmLmUmJmYuZSh7fSwhdCksdX0sdS5mb3JjZT1mdW5jdGlvbih0KXtyZXR1cm4gZi5mJiZmLmYoXCJzdHJpbmdcIj09PW4udHlwZSh0KT9uKHQpOnQpLHV9LHUubG9hZEFsbD1mdW5jdGlvbigpe3JldHVybiBmLmUmJmYuZSh7YWxsOiEwfSwhMCksdX0sdS5kZXN0cm95PWZ1bmN0aW9uKCl7cmV0dXJuIG4obC5hcHBlbmRTY3JvbGwpLm9mZihcIi5cIitjLGYuZSksbih0KS5vZmYoXCIuXCIrYyksZj17fSxlfSxyKHUsbCxhLGYsYyksbC5jaGFpbmFibGU/YTp1fXZhciBuPXQualF1ZXJ5fHx0LlplcHRvLGk9MCxvPSExO24uZm4uTGF6eT1uLmZuLmxhenk9ZnVuY3Rpb24odCl7cmV0dXJuIG5ldyBhKHRoaXMsdCl9LG4uTGF6eT1uLmxhenk9ZnVuY3Rpb24odCxyLGkpe2lmKG4uaXNGdW5jdGlvbihyKSYmKGk9cixyPVtdKSxuLmlzRnVuY3Rpb24oaSkpe3Q9bi5pc0FycmF5KHQpP3Q6W3RdLHI9bi5pc0FycmF5KHIpP3I6W3JdO2Zvcih2YXIgbz1hLnByb3RvdHlwZS5jb25maWcsdT1vLl9mfHwoby5fZj17fSksbD0wLGY9dC5sZW5ndGg7bDxmO2wrKykob1t0W2xdXT09PWV8fG4uaXNGdW5jdGlvbihvW3RbbF1dKSkmJihvW3RbbF1dPWkpO2Zvcih2YXIgYz0wLHM9ci5sZW5ndGg7YzxzO2MrKyl1W3JbY11dPXRbMF19fSxhLnByb3RvdHlwZS5jb25maWc9e25hbWU6XCJsYXp5XCIsY2hhaW5hYmxlOiEwLGF1dG9EZXN0cm95OiEwLGJpbmQ6XCJsb2FkXCIsdGhyZXNob2xkOjUwMCx2aXNpYmxlT25seTohMSxhcHBlbmRTY3JvbGw6dCxzY3JvbGxEaXJlY3Rpb246XCJib3RoXCIsaW1hZ2VCYXNlOm51bGwsZGVmYXVsdEltYWdlOlwiZGF0YTppbWFnZS9naWY7YmFzZTY0LFIwbEdPRGxoQVFBQkFJQUFBUC8vL3dBQUFDSDVCQUVBQUFBQUxBQUFBQUFCQUFFQUFBSUNSQUVBT3c9PVwiLHBsYWNlaG9sZGVyOm51bGwsZGVsYXk6LTEsY29tYmluZWQ6ITEsYXR0cmlidXRlOlwiZGF0YS1zcmNcIixzcmNzZXRBdHRyaWJ1dGU6XCJkYXRhLXNyY3NldFwiLHNpemVzQXR0cmlidXRlOlwiZGF0YS1zaXplc1wiLHJldGluYUF0dHJpYnV0ZTpcImRhdGEtcmV0aW5hXCIsbG9hZGVyQXR0cmlidXRlOlwiZGF0YS1sb2FkZXJcIixpbWFnZUJhc2VBdHRyaWJ1dGU6XCJkYXRhLWltYWdlYmFzZVwiLHJlbW92ZUF0dHJpYnV0ZTohMCxoYW5kbGVkTmFtZTpcImhhbmRsZWRcIixsb2FkZWROYW1lOlwibG9hZGVkXCIsZWZmZWN0Olwic2hvd1wiLGVmZmVjdFRpbWU6MCxlbmFibGVUaHJvdHRsZTohMCx0aHJvdHRsZToyNTAsYmVmb3JlTG9hZDplLGFmdGVyTG9hZDplLG9uRXJyb3I6ZSxvbkZpbmlzaGVkQWxsOmV9LG4odCkub24oXCJsb2FkXCIsZnVuY3Rpb24oKXtvPSEwfSl9KHdpbmRvdyk7XG5cbi8qISBqUXVlcnkgJiBaZXB0byBMYXp5IC0gQVYgUGx1Z2luIHYxLjQgLSBodHRwOi8vanF1ZXJ5LmVpc2JlaHIuZGUvbGF6eSAtIE1JVCZHUEwtMi4wIGxpY2Vuc2UgLSBDb3B5cmlnaHQgMjAxMi0yMDE4IERhbmllbCAnRWlzYmVocicgS2VybiAqL1xuIWZ1bmN0aW9uKHQpe3QubGF6eShbXCJhdlwiLFwiYXVkaW9cIixcInZpZGVvXCJdLFtcImF1ZGlvXCIsXCJ2aWRlb1wiXSxmdW5jdGlvbihhLGUpe3ZhciByPWFbMF0udGFnTmFtZS50b0xvd2VyQ2FzZSgpO2lmKFwiYXVkaW9cIj09PXJ8fFwidmlkZW9cIj09PXIpe3ZhciBvPWEuZmluZChcImRhdGEtc3JjXCIpLGk9YS5maW5kKFwiZGF0YS10cmFja1wiKSxuPTAsYz1mdW5jdGlvbigpeysrbj09PW8ubGVuZ3RoJiZlKCExKX0sZD1mdW5jdGlvbigpe3ZhciBhPXQodGhpcyksZT1hWzBdLnRhZ05hbWUudG9Mb3dlckNhc2UoKSxyPWEucHJvcChcImF0dHJpYnV0ZXNcIiksbz10KFwiZGF0YS1zcmNcIj09PWU/XCI8c291cmNlPlwiOlwiPHRyYWNrPlwiKTtcImRhdGEtc3JjXCI9PT1lJiZvLm9uZShcImVycm9yXCIsYyksdC5lYWNoKHIsZnVuY3Rpb24odCxhKXtvLmF0dHIoYS5uYW1lLGEudmFsdWUpfSksYS5yZXBsYWNlV2l0aChvKX07YS5vbmUoXCJsb2FkZWRtZXRhZGF0YVwiLGZ1bmN0aW9uKCl7ZSghMCl9KS5vZmYoXCJsb2FkIGVycm9yXCIpLmF0dHIoXCJwb3N0ZXJcIixhLmF0dHIoXCJkYXRhLXBvc3RlclwiKSksby5sZW5ndGg/by5lYWNoKGQpOmEuYXR0cihcImRhdGEtc3JjXCIpPyh0LmVhY2goYS5hdHRyKFwiZGF0YS1zcmNcIikuc3BsaXQoXCIsXCIpLGZ1bmN0aW9uKGUscil7dmFyIG89ci5zcGxpdChcInxcIik7YS5hcHBlbmQodChcIjxzb3VyY2U+XCIpLm9uZShcImVycm9yXCIsYykuYXR0cih7c3JjOm9bMF0udHJpbSgpLHR5cGU6b1sxXS50cmltKCl9KSl9KSx0aGlzLmNvbmZpZyhcInJlbW92ZUF0dHJpYnV0ZVwiKSYmYS5yZW1vdmVBdHRyKFwiZGF0YS1zcmNcIikpOmUoITEpLGkubGVuZ3RoJiZpLmVhY2goZCl9ZWxzZSBlKCExKX0pfSh3aW5kb3cualF1ZXJ5fHx3aW5kb3cuWmVwdG8pO1xuXG4vKiEgalF1ZXJ5ICYgWmVwdG8gTGF6eSAtIFBpY3R1cmUgUGx1Z2luIHYxLjMgLSBodHRwOi8vanF1ZXJ5LmVpc2JlaHIuZGUvbGF6eSAtIE1JVCZHUEwtMi4wIGxpY2Vuc2UgLSBDb3B5cmlnaHQgMjAxMi0yMDE4IERhbmllbCAnRWlzYmVocicgS2VybiAqL1xuIWZ1bmN0aW9uKHQpe2Z1bmN0aW9uIGUoZSxhLG4pe3ZhciBvPWUucHJvcChcImF0dHJpYnV0ZXNcIiksYz10KFwiPFwiK2ErXCI+XCIpO3JldHVybiB0LmVhY2gobyxmdW5jdGlvbih0LGUpe1wic3Jjc2V0XCIhPT1lLm5hbWUmJmUubmFtZSE9PWl8fChlLnZhbHVlPXIoZS52YWx1ZSxuKSksYy5hdHRyKGUubmFtZSxlLnZhbHVlKX0pLGUucmVwbGFjZVdpdGgoYyksY31mdW5jdGlvbiBhKGUsYSxyKXt2YXIgaT10KFwiPGltZz5cIikub25lKFwibG9hZFwiLGZ1bmN0aW9uKCl7cighMCl9KS5vbmUoXCJlcnJvclwiLGZ1bmN0aW9uKCl7cighMSl9KS5hcHBlbmRUbyhlKS5hdHRyKFwic3JjXCIsYSk7aS5jb21wbGV0ZSYmaS5sb2FkKCl9ZnVuY3Rpb24gcih0LGUpe2lmKGUpe3ZhciBhPXQuc3BsaXQoXCIsXCIpO3Q9XCJcIjtmb3IodmFyIHI9MCxpPWEubGVuZ3RoO3I8aTtyKyspdCs9ZSthW3JdLnRyaW0oKSsociE9PWktMT9cIixcIjpcIlwiKX1yZXR1cm4gdH12YXIgaT1cImRhdGEtc3JjXCI7dC5sYXp5KFtcInBpY1wiLFwicGljdHVyZVwiXSxbXCJwaWN0dXJlXCJdLGZ1bmN0aW9uKG4sbyl7aWYoXCJwaWN0dXJlXCI9PT1uWzBdLnRhZ05hbWUudG9Mb3dlckNhc2UoKSl7dmFyIGM9bi5maW5kKGkpLHM9bi5maW5kKFwiZGF0YS1pbWdcIiksdT10aGlzLmNvbmZpZyhcImltYWdlQmFzZVwiKXx8XCJcIjtjLmxlbmd0aD8oYy5lYWNoKGZ1bmN0aW9uKCl7ZSh0KHRoaXMpLFwic291cmNlXCIsdSl9KSwxPT09cy5sZW5ndGg/KHM9ZShzLFwiaW1nXCIsdSkscy5vbihcImxvYWRcIixmdW5jdGlvbigpe28oITApfSkub24oXCJlcnJvclwiLGZ1bmN0aW9uKCl7byghMSl9KSxzLmF0dHIoXCJzcmNcIixzLmF0dHIoaSkpLHRoaXMuY29uZmlnKFwicmVtb3ZlQXR0cmlidXRlXCIpJiZzLnJlbW92ZUF0dHIoaSkpOm4uYXR0cihpKT8oYShuLHUrbi5hdHRyKGkpLG8pLHRoaXMuY29uZmlnKFwicmVtb3ZlQXR0cmlidXRlXCIpJiZuLnJlbW92ZUF0dHIoaSkpOm8oITEpKTpuLmF0dHIoXCJkYXRhLXNyY3NldFwiKT8odChcIjxzb3VyY2U+XCIpLmF0dHIoe21lZGlhOm4uYXR0cihcImRhdGEtbWVkaWFcIiksc2l6ZXM6bi5hdHRyKFwiZGF0YS1zaXplc1wiKSx0eXBlOm4uYXR0cihcImRhdGEtdHlwZVwiKSxzcmNzZXQ6cihuLmF0dHIoXCJkYXRhLXNyY3NldFwiKSx1KX0pLmFwcGVuZFRvKG4pLGEobix1K24uYXR0cihpKSxvKSx0aGlzLmNvbmZpZyhcInJlbW92ZUF0dHJpYnV0ZVwiKSYmbi5yZW1vdmVBdHRyKGkrXCIgZGF0YS1zcmNzZXQgZGF0YS1tZWRpYSBkYXRhLXNpemVzIGRhdGEtdHlwZVwiKSk6byghMSl9ZWxzZSBvKCExKX0pfSh3aW5kb3cualF1ZXJ5fHx3aW5kb3cuWmVwdG8pOyJdLCJzb3VyY2VSb290IjoiIn0=