<?php
// src/Controller/SlackIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SlackIntegrationController extends AbstractController
{
    public function slack_integration()
    {
        return $this->render('slack_integration.html.twig', [
        ]);
    }
}