<?php
// src/Controller/ZendeskSupportIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ZendeskSupportIntegrationController extends AbstractController
{
    public function zendesk_support_integration()
    {
        return $this->render('zendesk_support_integration.html.twig', [
        ]);
    }
}