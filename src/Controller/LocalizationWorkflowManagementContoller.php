<?php
// src/Controller/LocalizationWorkflowManagementContoller.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LocalizationWorkflowManagementContoller extends AbstractController
{
    public function localization_workflow_management()
    {
        return $this->render('localization_workflow_management.html.twig', [
        ]);
    }
}