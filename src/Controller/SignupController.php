<?php
// src/Controller/SignupController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SignupController extends AbstractController
{
    public function signup()
    {
        return $this->render('sign_up.html.twig', [
        ]);
    }
}