<?php
// src/Controller/IntegrationsController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IntegrationsController extends AbstractController
{
    public function integrations()
    {
        return $this->render('integrations.html.twig', [
        ]);
    }
}