<?php
// src/Controller/PricingController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PricingController extends AbstractController
{
    public function pricing()
    {
        return $this->render('pricing.html.twig', [
        ]);
    }
}