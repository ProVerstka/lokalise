<?php
// src/Controller/ProductManController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductManController extends AbstractController
{
    public function product_for_managers()
    {
        return $this->render('product_for_managers.html.twig', [
        ]);
    }
}