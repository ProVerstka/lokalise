<?php
// src/Controller/BitbucketIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BitbucketIntegrationController extends AbstractController
{
    public function bitbucket_integration()
    {
        return $this->render('bitbucket_integration.html.twig', [
        ]);
    }
}