<?php
// src/Controller/TeamController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TeamController extends AbstractController
{
    public function team()
    {
        return $this->render('team.html.twig', [
        ]);
    }
}