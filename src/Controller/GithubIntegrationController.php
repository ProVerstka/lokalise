<?php
// src/Controller/GithubIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GithubIntegrationController extends AbstractController
{
    public function github_integration()
    {
        return $this->render('github_integration.html.twig', [
        ]);
    }
}