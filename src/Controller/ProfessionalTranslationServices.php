<?php
// src/Controller/ProfessionalTranslationServices.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfessionalTranslationServices extends AbstractController
{
    public function professional_translation_services()
    {
        return $this->render('professional_translation_services.html.twig', [
        ]);
    }
}