<?php
// src/Controller/AmazonIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AmazonIntegrationController extends AbstractController
{
    public function amazon_integration()
    {
        return $this->render('amazon_integration.html.twig', [
        ]);
    }
}