<?php
// src/Controller/GoogleIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GoogleIntegrationController extends AbstractController
{
    public function google_integration()
    {
        return $this->render('google_integration.html.twig', [
        ]);
    }
}