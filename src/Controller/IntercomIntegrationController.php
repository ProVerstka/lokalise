<?php
// src/Controller/IntercomIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IntercomIntegrationController extends AbstractController
{
    public function intercom_integration()
    {
        return $this->render('intercom_integration.html.twig', [
        ]);
    }
}