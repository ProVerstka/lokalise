<?php
// src/Controller/LegalController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LegalController extends AbstractController
{
    public function legal()
    {
        return $this->render('legal.html.twig', [
        ]);
    }
}