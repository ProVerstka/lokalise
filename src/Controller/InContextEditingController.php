<?php
// src/Controller/InContextEditingController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class InContextEditingController extends AbstractController
{
    public function in_context_editing()
    {
        return $this->render('in_context_editing.html.twig', [
        ]);
    }
}