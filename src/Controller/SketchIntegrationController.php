<?php
// src/Controller/SketchIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SketchIntegrationController extends AbstractController
{
    public function sketch_integration()
    {
        return $this->render('sketch_integration.html.twig', [
        ]);
    }
}