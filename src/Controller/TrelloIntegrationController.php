<?php
// src/Controller/TrelloIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrelloIntegrationController extends AbstractController
{
    public function trello_integration()
    {
        return $this->render('trello_integration.html.twig', [
        ]);
    }
}