<?php
// src/Controller/FeaturesController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FeaturesController extends AbstractController
{
    public function features()
    {
        return $this->render('features.html.twig', [
        ]);
    }
}