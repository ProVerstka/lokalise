<?php
// src/Controller/DeliverToEndUsersController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeliverToEndUsersController extends AbstractController
{
    public function deliver_to_end_users()
    {
        return $this->render('deliver_to_end_users.html.twig', [
        ]);
    }
}