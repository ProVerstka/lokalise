<?php
// src/Controller/CollaborativeTranslationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CollaborativeTranslationController extends AbstractController
{
    public function collaborative_translation()
    {
        return $this->render('collaborative_translation.html.twig', [
        ]);
    }
}