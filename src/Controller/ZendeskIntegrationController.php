<?php
// src/Controller/ZendeskIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ZendeskIntegrationController extends AbstractController
{
    public function zendesk_integration()
    {
        return $this->render('zendesk_integration.html.twig', [
        ]);
    }
}