<?php
// src/Controller/ProcessAutomationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProcessAutomationController extends AbstractController
{
    public function process_automation()
    {
        return $this->render('process_automation.html.twig', [
        ]);
    }
}