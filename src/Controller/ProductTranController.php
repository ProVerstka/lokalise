<?php
// src/Controller/ProductTranController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductTranController extends AbstractController
{
    public function product_for_translators()
    {
        return $this->render('product_for_translators.html.twig', [
        ]);
    }
}