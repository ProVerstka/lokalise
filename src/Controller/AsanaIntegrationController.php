<?php
// src/Controller/AsanaIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AsanaIntegrationController extends AbstractController
{
    public function asana_integration()
    {
        return $this->render('asana_integration.html.twig', [
        ]);
    }
}