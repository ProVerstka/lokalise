<?php
// src/Controller/CareersController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CareersController extends AbstractController
{
    public function careers()
    {
        return $this->render('careers.html.twig', [
        ]);
    }
}