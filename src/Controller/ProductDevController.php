<?php
// src/Controller/ProductDevController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductDevController extends AbstractController
{
    public function product_for_developers()
    {
        return $this->render('product_for_developers.html.twig', [
        ]);
    }
}