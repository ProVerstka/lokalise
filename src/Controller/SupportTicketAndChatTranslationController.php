<?php
// src/Controller/SupportTicketAndChatTranslationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SupportTicketAndChatTranslationController extends AbstractController
{
    public function support_ticket_and_chat_translation()
    {
        return $this->render('support_ticket_and_chat_translation.html.twig', [
        ]);
    }
}