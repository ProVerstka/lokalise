<?php
// src/Controller/GitlabIntegrationController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GitlabIntegrationController extends AbstractController
{
    public function gitlab_integration()
    {
        return $this->render('gitlab_integration.html.twig', [
        ]);
    }
}