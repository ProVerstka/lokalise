<?php
// src/Controller/CaseStudyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CaseStudyController extends AbstractController
{
    public function case_study()
    {
        return $this->render('case_study.html.twig', [
        ]);
    }
}