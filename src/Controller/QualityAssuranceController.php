<?php
// src/Controller/QualityAssuranceController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QualityAssuranceController extends AbstractController
{
    public function quality_assurance()
    {
        return $this->render('quality_assurance.html.twig', [
        ]);
    }
}