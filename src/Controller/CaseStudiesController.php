<?php
// src/Controller/CaseStudiesController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CaseStudiesController extends AbstractController
{
    public function case_studies()
    {
        return $this->render('case_studies.html.twig', [
        ]);
    }
}